#include "Light.fx"

//------------- 상수 버퍼 ----------------//
//VS(slot0)
cbuffer cbViewProjectionMatrix : register(b0)
{
	matrix gmtxView : packoffset(c0);
	matrix gmtxProjection : packoffset(c4);
};

//VS(slot1)
cbuffer cbWorldMatrix : register(b1)
{
	matrix gmtxWorld : packoffset(c0);
};

//PS(slot0)
cbuffer cbColor : register(b0)
{
	float4	gcColor : packoffset(c0);
};
//------------- 상수 버퍼 ----------------//


//----------------------------------------//
Texture2D gtxtTexture : register(t0);
SamplerState gSamplerState : register(s0);

Texture2D gtxtDetailTexture : register(t1);
SamplerState gDetailSamplerState : register(s1);
//----------------------------------------//


//-----------------------------------------------//
//instance
struct VS_FBX_INSTANCED_INPUT
{
	float3 position : POSITION;
	float3 normal : NORMAL;
	float2 texCoord : TEXCOORD0;
	column_major float4x4 mtxTransform : INSTANCEPOS;
};

struct VS_FBX_INSTANCED_OUTPUT
{
	float4 position : SV_POSITION;
	float3 positionW : POSITION;
	float3 normalW : NORMAL;
	float2 texCoord : TEXCOORD0;
	float4 instanceID : INDEX;
};

VS_FBX_INSTANCED_OUTPUT VSFBXInstance(VS_FBX_INSTANCED_INPUT input, uint instanceID : SV_InstanceID)
{
	VS_FBX_INSTANCED_OUTPUT output = (VS_FBX_INSTANCED_OUTPUT)0;
	output.normalW = mul(float4(input.normal, 1.0f), input.mtxTransform);
	output.positionW = mul(float4(input.position, 1.0f), input.mtxTransform).xyz; //OK
	output.position = mul(mul(float4(output.positionW, 1.0f), gmtxView), gmtxProjection);
	output.texCoord = input.texCoord;
	output.instanceID = instanceID;

	return output;
}
//------------------------------------------------//
