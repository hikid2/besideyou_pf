#pragma once
#include"NetworkLib.h"
#include"CNetwork.h"
class ClientSession;


class ClientNetwork : protected CNetwork
{
public:
	ClientSession * _session;
	ClientSession * _Roomsession;
	AfterTreatment * _aTreatment;
	ClientNetwork();
	virtual ~ClientNetwork() {}
	bool setAfterTreatment(AfterTreatment * treatment);
	bool SetupWSAAsyncSelect(HWND & hwnd);											// Create Session, WSAAsyncSelect(), Connect()
	bool SetupWSAAsyncSelect(HWND & hwnd, ClientSession * _roomSession, char * ip, int port);			// Create Session, WSAAsyncSelect(), Connect()
	void ProcessSocketMessage(HWND hwnd, SOCKET sock, WORD msg, WORD lparam);		// Call Path Where WM_USER :
	void IngameEnd(HWND ghwnd);
};