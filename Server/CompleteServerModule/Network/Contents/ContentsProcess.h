#pragma once
#include"stdafx.h"

// Thread count 변환 -> 상황에따라 수정가능.
#define MAX_PACKET_THREAD		SIZE_64

class ContentsProcess
{
public:
	ContentsProcess();
	virtual ~ContentsProcess();

	void PutPackage(Package *package);

	virtual void RegistSubContents() = 0; // 하위 컨텐츠 상속용 함수
	static void PacketNotifyTerminal(Session * session, Packet * rowPacket);


protected:
	typedef void(*ContentsMethod)(Session * session, Packet * rowPacket);
	typedef void(*Runwork)(Work * rowWork);
	std::unordered_map<PacketType, ContentsMethod> mContentsMethodTable;

private:
	void Initalize(jsonValue_t * config);
	void RegistDefaultPacketFunc();
	void Run(Package * package);
	void Execute();

	void Process();

private:
	array<Thread *, MAX_PACKET_THREAD> mThreadPool;
	ThreadJobQueue<Package *> * mPackageQueue;

};