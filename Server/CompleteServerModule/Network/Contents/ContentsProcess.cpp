#include"stdafx.h"
#include"ContentsProcess.h"

void ContentsProcess::Initalize(jsonValue_t * config)
{
	this->RegistDefaultPacketFunc();
}

void ContentsProcess::RegistDefaultPacketFunc()
{
	mContentsMethodTable.insert(make_pair(PE_T_NTY_TERMINAL, &ContentsProcess::PacketNotifyTerminal));
}

void ContentsProcess::Run(Package * package)
{
	PacketType tempType = package->mPacketPtr->type();
	ContentsMethod tempContentsMethod = mContentsMethodTable.at(tempType);
	if (tempContentsMethod == nullptr) {
		SLog(L"!! invalid Packet inputted... type[%d]", tempType);
		package->mSessionPtr->OnClose();
		return;
		// 1. 업데이트 되지 않은 타입이거나, 패킷의 조작의심
	}
	//SLog(L"*** [%d] packet run ***", type);
	tempContentsMethod(package->mSessionPtr, package->mPacketPtr);
}

void ContentsProcess::Execute()
{
	Package * tempPackage = nullptr;
	if (mPackageQueue->Pop(tempPackage) != false) {
		this->Run(tempPackage);
		SAFE_DELETE(tempPackage);
	}
}

void ContentsProcess::Process()
{
#if NAMU
	while (_shutdown == false) {
		this->execute();
		CONTEXT_SWITCH;
	}
#endif
}

ContentsProcess::ContentsProcess()
{
	jsonValue_t config;
	if (!loadConfig(&config)) {
		return;
	}
	this->Initalize(&config);
}

ContentsProcess::~ContentsProcess()
{
	SAFE_DELETE(mPackageQueue);
	for(auto & thread : mThreadPool) {
		SAFE_DELETE(thread);
	}
	mContentsMethodTable.clear();
}

void ContentsProcess::PutPackage(Package * package)
{
	this->Run(package);
	SAFE_DELETE(package);
}

void ContentsProcess::PacketNotifyTerminal(Session * session, Packet * rowPacket)
{
	session->Settype(SESSION_TYPE_TERMINAL);
	SLog(L"* [%S] Terminal accepted.", session->ClientAddress().c_str());
}
