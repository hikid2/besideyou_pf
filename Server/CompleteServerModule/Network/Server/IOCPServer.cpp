#include"stdafx.h"
#include "IOCPServer.h"
#include"..\Session\IOCPSession.h"

bool IOCPServer::CreateListenSocket()
{
	mListenSocket = WSASocket(AF_INET, SOCK_STREAM, NULL, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (mListenSocket == INVALID_SOCKET) {
		SErrLog(L" !! ListenSocket(WSASocket) failed");
		return false;
	}

	SOCKADDR_IN serverAdder;
	serverAdder.sin_family = AF_INET;
	serverAdder.sin_port = htons((u_short)mPort);
	inet_pton(AF_INET, mIp, &(serverAdder.sin_addr));

	int bf = 1;
	setsockopt(mListenSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&bf, (int)sizeof(bf));

	int ret = ::bind(mListenSocket, (SOCKADDR *)&serverAdder, sizeof(serverAdder));

	if (ret == SOCKET_ERROR) {
		SLog(L"!! socket error : %d ", WSAGetLastError());
		SErrLog(L" !! Bind Fail");
		return false;
	}
	const int BACK_LOG_SIZE = 5;
	ret = ::listen(mListenSocket, BACK_LOG_SIZE);
	if (ret == SOCKET_ERROR) {
		SErrLog(L" !! Listen Fail");
		return false;
	}
	array<char, SIZE_64> ip;
	inet_ntop(AF_INET, &(serverAdder.sin_addr), ip.data(), ip.size());

	SLog(L"** Server Listen socket created . IP : %S , Prot : %d ", ip.data(), mPort);

	return true;
}

DWORD WINAPI IOCPServer::AcceptThread(LPVOID serverPtr)
{
	IOCPServer * server = (IOCPServer *)serverPtr;

	while (!_shutdown) {
		SOCKET acceptSocket = INVALID_SOCKET;
		SOCKADDR_IN recvAdder;
		static int adderLen = sizeof(recvAdder);
		acceptSocket = WSAAccept(server->GetListenSocekt(), (struct sockaddr *)&recvAdder, &adderLen, NULL, 0);
		if (acceptSocket == SOCKET_ERROR) {
			if (!server->GetStates() == SERVER_STOP) {
				SLog(L" !! Accpet fail");
				// ERR : DO NOT Initialize check to this function!
				break;
			}
		}
		server->OnAceept(acceptSocket, recvAdder);

		if (server->GetStates() != SERVER_READY) {
			SLog(L" !! Logic Error\n");
			// ERR : 거의 이 부분에 올 일이 없으나 서버의 초기화 작업이전에 
			// Accept가 이루어지면 가능성이 없지도 않음. 거의 불가능
			break;
		}
	}
	return 0;
}

DWORD WINAPI IOCPServer::WorkerThread(LPVOID serverPtr)
{
	IOCPServer * server = (IOCPServer *)serverPtr;

	while (!_shutdown) {
		IoData * iodata = nullptr;
		IOCPSession * session = nullptr;
		DWORD transfersize;

		BOOL retval = GetQueuedCompletionStatus(server->GetIocp(), &transfersize, (PULONG_PTR)&session, (LPOVERLAPPED*)&iodata, INFINITE);

		if (!retval) {
			SLog(L" ## queue data getting fail\n");
			if (transfersize == 0) SLog(L"## Abnormally terminated \n##");
			// keepAlive 에서 소켓처리를 넘기는 방안 채택
			// Note : retval = false , transfersize = 0  : 비 정상적 종료
			//		  retval = true , transfersize = 0 : 정상적 종료
			//		  *Overlapped = null, retval = false, completionkey = null
			//		  : WAIT_TIMEOUT
			SessionManager::getInstance().CloseSession(session);
			continue;
		}
		if (retval && transfersize == 0)
		{
			// 정상적 종료 임으로 소켓을 닫는다.
			SessionManager::getInstance().CloseSession(session);
			continue;
		}
		if (session == nullptr) {
			SLog(L"! socket data broken");
			return 0;
		}

		switch (iodata->GetType()) {
		case IO_WRITE:

			session->OnSend((size_t)transfersize, iodata);
			continue;
		case IO_READ: 
		{
			INT64 remained = transfersize;
			while (remained > 0)
			{
				Package *package = session->OnRecv(remained);
				if (package != nullptr) {
					server->PutPackage(package);
				}
			}
			if(remained == 0) session->RecvStandBy();
		}
			continue;
		case IO_ERROR:

			SessionManager::getInstance().CloseSession(session);
			continue;
#if BESIDEYOU
		case IO_FUNC:
			session->mWork->Tick();
			iodata = nullptr;
			SAFE_DELETE(session);
			continue;
#endif

		}
	}
	return 0;
}

IOCPServer::IOCPServer(ContentsProcess * contentsProcess) 
	: Server(contentsProcess)
{
}

IOCPServer::~IOCPServer()
{
	::closesocket(mListenSocket);
}

bool IOCPServer::Run()
{
	if (MAXIMUM_IOCP_THREAD < mWorkerThreadCount) {
		SErrLog(L"! workerThread Limited[%d], this config setting Err[%d] !", MAXIMUM_IOCP_THREAD, mWorkerThreadCount);
		return false;
	}

	gIocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, mWorkerThreadCount);
	if (gIocp == nullptr) {
		SLog(L" !! _giocp err !!");
		return false;
	}
	this->CreateListenSocket();

	mAcceptThread = MAKE_THREAD(IOCPServer, AcceptThread);
	for (auto i = 0; i < mWorkerThreadCount; ++i) {
		mWorkerThread[i] = MAKE_THREAD(IOCPServer, WorkerThread);
	}
#if BESIDEYOU
	TaskManager::getInstance().AddiocpHandle(&gIocp);
#endif
	this->mState = SERVER_READY;
	while (!_shutdown)
	{
		if (!strcmp(this->GetName(), "RoomServer"))
		{
			Clock::getInstance().Update();
			Frame();
		}
		else
			CONTEXT_SWITCH;
	}
	/*
		TODO : SessionManager 를 이용하여 람다함수 제작!
				1. All Session count 
				2. Kick Function
				3. Notify All User's
				4. Notify other's User
				5. Nomal Ping Testing
	*/
	return true;
}

SOCKET IOCPServer::GetListenSocekt()
{
	return mListenSocket;
}

HANDLE IOCPServer::GetIocp()
{
	return gIocp;
}

void IOCPServer::OnAceept(SOCKET accepter, SOCKADDR_IN addressinfo)
{
	IOCPSession * session = new IOCPSession();
	if (session == nullptr) {
		SLog(L"! accpet Sesion created fail");
		return;
	}
	if (!session->OnAccept(accepter, addressinfo)) {
		SAFE_DELETE(session);
		return;
	}
	if (!SessionManager::getInstance().AddSession(session)) {
		SAFE_DELETE(session);
		return;
	}
	session->mIoData[IO_READ].Clear();

	HANDLE handle = CreateIoCompletionPort((HANDLE)accepter, this->GetIocp(), (ULONG_PTR)&(*session), NULL);
	if (!handle) {
		SAFE_DELETE(session);
		return;
	}

	SLog(L" ** Client accept who's [%S]", session->ClientAddress().c_str());
	session->RecvStandBy();
}

void IOCPServer::Frame()
{
	World::getInstance().Update();
}
