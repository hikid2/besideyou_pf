#pragma once
#include"stdafx.h"

typedef enum SERVERSTATE {
	SERVER_STOP,
	SERVER_READY,
	SERVER_INIT,
	COUNT
}ServerState;

class Server{
public:
	Server(ContentsProcess * contentsprocess);
	virtual ~Server();

	virtual void Initialize(jsonValue_t * config);
	virtual bool Run() = 0; // 순수 가상함수
	SERVERSTATE & GetStates(); 
	void PutPackage(Package * package);
	const char * GetName() const { return mName.c_str(); }
protected:
	char mIp[16];
	int mPort;
	int mWorkerThreadCount;
	string mName;

	SERVERSTATE mState;
	ContentsProcess * mContentsprocess;
};