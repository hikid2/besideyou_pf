#include"stdafx.h"
#include"Server.h"

Server::Server(ContentsProcess * contentsprocess)
{
	SLog(L" ## init network ");
	mContentsprocess = contentsprocess;
	mState = SERVER_STOP;
	jsonValue_t config;
	if (!loadConfig(&config)) {
		return;
	}
	this->Initialize(&config);
}

Server::~Server()
{
	shutdownServer();
	mState = SERVER_STOP;
	SAFE_DELETE(mContentsprocess);
	SLog(L"$$ End network Base");
}

void Server::Initialize(jsonValue_t * config)
{
	// 테스크 설정
	TerminalManager::getInstance().run(this);

	TaskManager::getInstance();
	
	// 서버 설정
	
	jsonValue_t Vserver = (*config)["Server"];
	strcpy_s(mIp, Vserver.get("IP", "").asString().c_str());
	mPort = Vserver.get("PORT", 0).asInt();
	mWorkerThreadCount = Vserver.get("ThreadCount", 0).asInt();
	mState = SERVER_INIT;
	SLog(L"** worker thread count : %d", mWorkerThreadCount);
	mName = (*config).get("Name", "").asString();
	SLog(L"### %S Server Start~! ###", mName.data());
}

SERVERSTATE & Server::GetStates()
{
	return mState;
}

void Server::PutPackage(Package * package)
{
	mContentsprocess->PutPackage(package);
}
