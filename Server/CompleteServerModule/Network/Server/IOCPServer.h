#pragma once
#include"stdafx.h"
#include"Server.h"
//Iocp 서버 클래스.

#define MAXIMUM_IOCP_THREAD		SIZE_32

class IOCPServer : public Server, public Singleton<IOCPServer>
{
public:
	IOCPServer() : Server(nullptr){}
	IOCPServer(ContentsProcess * contentsProcess);
	virtual ~IOCPServer();
	bool Run();

	SOCKET GetListenSocekt();
	HANDLE GetIocp();

	void OnAceept(SOCKET accepter, SOCKADDR_IN addressinfo);

	void Frame();


private:

	SOCKET mListenSocket;
	HANDLE gIocp;
	Thread * mAcceptThread;
	array<Thread *, SIZE_32> mWorkerThread;

	bool CreateListenSocket();

	static DWORD WINAPI AcceptThread(LPVOID serverPtr);

	static DWORD WINAPI WorkerThread(LPVOID serverPtr);
};
