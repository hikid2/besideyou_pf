#pragma once
#include"stdafx.h"

class Session;
class SessionManager;
class Package;

typedef enum {
	IO_READ,
	IO_WRITE,
	IO_ERROR,
	IO_FUNC,
}IO_OPERATION;

class IoData {
	OVERLAPPED mOverlapped; // overlapped
	IO_OPERATION mIoType; // type  0 : Read , 1 : Write , 2 : Error
	size_t mTotalBytes; // Read Total byte
	size_t mCurrentBytes; // current Total byte
	array<char, SOCKET_BUF_SIZE> mBuffer; // buf
public:
	IoData();
	void Clear();

	bool LackIOBuf(size_t size);
	int32_t SetupTotalBytes();
	size_t GetTotalByte();

	IO_OPERATION &GetType();
	void SetType(IO_OPERATION type);

	WSABUF wsaBuf();
	char * GetData();
	bool SetData(Stream & stream);// TODO :: buf
	LPWSAOVERLAPPED GetOverlapped();
};


#define IO_DATA_COUNT 1


class IOCPSession : public Session
{
public:
	array<IoData, IO_DATA_COUNT> mIoData;
	Work * mWork;

public:
	IOCPSession();

	virtual void OnSend(size_t size, IoData * data);
	virtual void SendPacket(Packet * packet);
	virtual Package * OnRecv(INT64 & size);
	void RecvStandBy();
private:
	void Initialize();

	void checkErrorIo(DWORD ret);
	DWORD Recv(WSABUF wsaBuf);
	bool IsRecving(size_t size);

	void Send(WSABUF wsaBuf, IoData * data);
};