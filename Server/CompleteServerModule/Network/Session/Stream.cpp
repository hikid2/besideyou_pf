#include"stdafx.h"
#include "Stream.h"

Stream::Stream()
{
	this->Initialize();
}

Stream::Stream(UCHAR * stream, size_t size)
{
	this->Initialize();
	this->Set(stream, size);
}

void Stream::Initialize()
{
	mReadPtr = 0;
	mOffset = 0;
	ZeroMemory(&mStream, sizeof(mStream));
}

UCHAR * Stream::GetData()
{
	return mStream.data();
}

size_t Stream::GetSize()
{
	return mOffset;
}

void Stream::operator=(Stream & stream)
{
	this->Set(stream.GetData(), stream.GetSize());
}

void Stream::Set(UCHAR * data, size_t size)
{
	this->mOffset = size;
	memcpy_s(this->mStream.data(), mStream.size(), (void *)data, size);
}

bool Stream::CheckWriteBound(size_t len)
{
	if (mOffset + len > sizeof(mStream)) {
		SLog(L"! socket stream over.");
		ASSERT(FALSE);
		return false;
	}
	return true;
}
#define STREAM_WRITE(value)						\
	INT32 size = sizeof(value);					\
	if (this->CheckWriteBound(size) == false) {	\
		return;									\
	}											\
	memcpy_s((void *)(mStream.data() + mOffset), mStream.size() - mOffset, (const void *)&value, size);\
	mOffset += size;


template<class T>
void Stream::operator<<(const T & value)
{
	STREAM_WRITE(value);
}


void Stream::operator<<(const bool & value)
{
	STREAM_WRITE(value);
}

void Stream::operator<<(const INT8 & value)
{
	STREAM_WRITE(value);
}

void Stream::operator<<(const UINT8 & value)
{
	STREAM_WRITE(value);
}

void Stream::operator<<(const INT16 & value)
{
	STREAM_WRITE(value);
}

void Stream::operator<<(const UINT16 & value)
{
	STREAM_WRITE(value);
}

void Stream::operator<<(const INT32 & value)
{
	STREAM_WRITE(value);
}

void Stream::operator<<(const UINT32 & value)
{
	STREAM_WRITE(value);
}

void Stream::operator<<(const INT64 & value)
{
	STREAM_WRITE(value);
}

void Stream::operator<<(const UINT64 & value)
{
	STREAM_WRITE(value);
}
void Stream::operator<<(const float & value)
{
	STREAM_WRITE(value);
}
void Stream::operator<<(const double & value)
{
	STREAM_WRITE(value);
}
//template<class T>
//void Stream::operator<<(const std::vector<T>& value)
//{
//	*this << value.size();
//	for (auto i : value) {
//		*this << i;
//	}
//}



void Stream::operator<<(const str_t & value)
{
	*this << (INT32)value.length();
	for (auto i : value) {
		*this << i;
	}
}

void Stream::operator<<(const wstr_t & value)
{
	*this << (INT32)value.length();
	for (auto i : value) {
		*this << i;
	}
}

bool Stream::CheckReadBound(size_t len)
{
	if (mReadPtr + len > mOffset) {
		SLog(L"! readOffset : %d, size : %d, totalOffset = %d", mReadPtr, len, mOffset);
		SLog(L"! socket stream has not memory.");
		ASSERT(FALSE);
		return false;
	}
	return true;
}

void Stream::Read(void * retval, size_t len)
{
	memcpy_s(retval, len, (void *)(mStream.data() + mReadPtr), len);
	mReadPtr += len;
}

#define STREAM_READ(type, retval)			\
	size_t size = sizeof(type);				\
	if(this->CheckReadBound(size) == false){\
		return;								\
	}										\
	this->Read((void *)(retval), size);


template<class T>
void Stream::operator >> (T * retval)
{
	STREAM_READ(T, retval);
}


void Stream::operator >> (bool * retval)
{
	STREAM_READ(bool, retval);
}

void Stream::operator >> (INT8 * retval)
{
	STREAM_READ(INT8, retval);
}

void Stream::operator >> (UINT8 * retval)
{
	STREAM_READ(UINT8, retval);
}

void Stream::operator >> (INT16 * retval)
{
	STREAM_READ(INT16, retval);
}

void Stream::operator >> (UINT16 * retval)
{
	STREAM_READ(UINT16, retval);
}

void Stream::operator >> (INT32 * retval)
{
	STREAM_READ(INT32, retval);
}

void Stream::operator >> (UINT32 * retval)
{
	STREAM_READ(UINT32, retval);
}

void Stream::operator >> (INT64 * retval)
{
	STREAM_READ(INT64, retval);
}

void Stream::operator >> (UINT64 * retval)
{
	STREAM_READ(UINT64, retval);
}

void Stream::operator >> (float * retval)
{
	STREAM_READ(FLOAT, retval);
}

void Stream::operator >> (double * retval)
{
	STREAM_READ(double, retval);
}

//template<class T>
//void Stream::operator >> (std::vector<T>* retval)
//{
//	size_t size;
//	*this >> &size;
//
//	for (size_t i = 0; i < size; ++i) {
//		T tmp;
//		*this >> &tmp;
//		retval->push_back(tmp);
//	}
//}

void Stream::operator >> (str_t * retval)
{
	INT32 size;
	*this >> &size;
	if (this->CheckReadBound(size) == false) {
		return;
	}

	char *buf = new char[size + 1];
	this->Read((void *)(buf), size * sizeof(CHAR));
	buf[size] = '\0';

	retval->clear();
	*retval = buf;

	delete buf;
}

void Stream::operator >> (wstr_t * retval)
{
	INT32 size;
	*this >> &size;
	if (this->CheckReadBound(size) == false) {
		return;
	}

	WCHAR *buf = new WCHAR[size + 1];
	this->Read((void *)(buf), size * sizeof(WCHAR));
	buf[size] = '\0';

	retval->clear();
	*retval = buf;

	delete buf;
}
