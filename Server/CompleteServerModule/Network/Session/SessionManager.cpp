#include"stdafx.h"
#include"SessionManager.h"

SessionManager::SessionManager() : mLock(L"SessionManager")
{
	mIdSeed = 1;
	mMaxConnection = MAXCONNECTIONNUMBER;
	
}

SessionManager::SessionManager(int maxConnection) : mLock(L"SessionManager")
{
	mIdSeed = 1;
	mMaxConnection = maxConnection;
}

SessionManager::~SessionManager()
{
	vector<Session *>removeSessionVec;
	removeSessionVec.resize(mSessionList.size());
	std::copy(mSessionList.begin(), mSessionList.end(), removeSessionVec.begin());
	for (auto session : removeSessionVec) {
		session->OnClose();
	}
	mSessionList.clear();
}

oid_t SessionManager::CreateSessionId()
{
	return mIdSeed++;
}

bool SessionManager::AddSession(Session * session)
{
	SAFE_LOCK(mLock);
	auto findSession = find(mSessionList.begin(), mSessionList.end(), session);
	if (findSession != mSessionList.end()) return false;
	if (mSessionCount > mMaxConnection) {
		SLog(L"!! SessionCount over !!");
		// TODO : 적정 사용자 인원 초과 추후 대응 필요.
		// (room Server의 경우)다른 서버로 연결을 우회하게끔 패킷을 보낸다던가...
		return false;
	}
	session->SetId(this->CreateSessionId());
	mSessionList.emplace_back(session);
	++mSessionCount;
	return true;
}

list<Session*>& SessionManager::GetSessionList()
{
	return mSessionList;
}

bool SessionManager::CloseSession(Session * session)
{
	// TODO : Session을 닫을 때 수상한 Session과 정상인 Session에 대한 처리를 다르게 해야한다.
	// 
	if (session == nullptr) {
		// TODO : Log에 빈 Session을 보낸 상황임으로 
		// 로직상 문제 있을 가능성 유추 가능
		return false;
	}
	SAFE_LOCK(mLock);
	auto findSession = find(mSessionList.begin(), mSessionList.end(), session);
	if (findSession != mSessionList.end()) {
		// 1.소켓을 먼저 끄고
		// 2. 카운트를 바꿔준뒤,
		// 3. 객체를 삭제.
		// TODO : 객체를 지우기전에 유저 정보 최신 업데이트가 필요.
		Session * deleteSession = *findSession;
		SLog(L"** close by client session [%S]", deleteSession->ClientAddress().c_str())
		::closesocket(deleteSession->GetSocket());


		mSessionList.remove(deleteSession);
		--mSessionCount;
		SAFE_DELETE(deleteSession);
		return true;
	}
	return false;
}

Session * SessionManager::GetSession(oid_t id)
{
	SAFE_LOCK(mLock);
	Session * findSession = nullptr;
	for (const auto session : mSessionList) {
		if (session->GetId() == id) {
			findSession = session;
			break;
		}

	}
	return findSession;
}

Session * SessionManager::GetUseSsion(INT64 uid)
{
	SAFE_LOCK(mLock);
	Session * findSession = nullptr;
	for (const auto session : mSessionList) {
		if (session->Uid() == uid) {
			findSession = session;
			break;
		}

	}
	return findSession;
}
