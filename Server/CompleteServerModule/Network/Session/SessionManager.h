#pragma once
#include"stdafx.h"
#include"Session.h"

#define MAXCONNECTIONNUMBER 1000

class SessionManager : public Singleton<SessionManager>
{
	typedef list<Session *> SessionList;
public:
	SessionManager();

	SessionManager(int maxConnection);

	~SessionManager();

	oid_t CreateSessionId();

	bool AddSession(Session * session);
	
	list<Session*> & GetSessionList();

	bool CloseSession(Session * session);

	Session *GetSession(oid_t id);

	Session *GetUseSsion(INT64 uid);
private:
	SessionList mSessionList;
	int mSessionCount;
	int mMaxConnection;
	Lock mLock;
	oid_t mIdSeed;

	typedef std::function<void(SessionList * sessionList, wstr_t *arg)> cmdFunc;
	unordered_map<wstr_t, cmdFunc> mServerCommend;

};