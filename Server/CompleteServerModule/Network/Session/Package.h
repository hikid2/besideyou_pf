#pragma once
#include"stdafx.h"

class Session;

// 소포 클래스 뜯어서 사용하면 바로 지워버리자.
// 주의! session은 함부로 지우면 안된다.
class Package
{
public:
	Session * mSessionPtr;
	Packet * mPacketPtr;

	Package(Session * inSessionPtr, Packet * inPacketPtr)
	{
		mSessionPtr = inSessionPtr;
		mPacketPtr = inPacketPtr;
	}

	
	~Package()
	{
		mSessionPtr = nullptr;
		SAFE_DELETE(mPacketPtr);
	}
};