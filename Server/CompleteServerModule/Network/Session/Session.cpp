#include"stdafx.h"
#include"Session.h"

bool Session::SetsocketOption()
{
	// 좀비 세션에 대한 대처 방안
	// SIO_KEEPALIVE_VALS 사용 권유, SIO_KEEPALIVE는 비추천
	// 이유 : SO_KEEPALIVE 옵션을 적용하면 시스템 레지스트리 값을 변경하므로 그 시스템의 모든 소켓의 KEEPALIVE 속성이 변경하게 된다
	tcp_keepalive keepAliveSet = { 0 }, returned = { 0 };
	keepAliveSet.onoff = 1;
	keepAliveSet.keepalivetime = 10000;				// 10초마다 한번씩 연결되있는지 확인
	keepAliveSet.keepaliveinterval = 10000;			// resend if no-reply

	DWORD dwBytes;
	if (WSAIoctl(mSocketData.mSocket, SIO_KEEPALIVE_VALS, &keepAliveSet, sizeof(keepAliveSet), &returned, sizeof(returned), &dwBytes, NULL, NULL) != 0) {
		return false;
	}
	return true;
}

Session::Session()
{
	mUid = -1;
	mType = SESSION_TYPE_CLIENT;
	this->UpdateHeartBeat();
}

Session::~Session()
{
	this->OnClose();
}

bool Session::OnAccept(SOCKET socket, SOCKADDR_IN addrinfo)
{
	mSocketData.mSocket = socket;
	int addrLen;
	//getpeername : 연결된 상대측 소켓 주소 정보를 가져온다.
	getpeername(mSocketData.mSocket, (struct sockaddr *)&mSocketData.mAddrInfo, &addrLen);
	mSocketData.mAddrInfo = addrinfo;
	if (!this->SetsocketOption()) {
		return false;
	}
	return true;
}

void Session::OnClose()
{
	SessionManager::getInstance().CloseSession(this);
}

SOCKET & Session::GetSocket()
{
	return mSocketData.mSocket;
}

str_t Session::ClientAddress()
{
	array<char, SIZE_64> ip;
	inet_ntop(AF_INET, &(mSocketData.mAddrInfo.sin_addr), ip.data(), ip.size());
	return ip.data();
}

oid_t Session::GetId()
{
	return mId;
}

void Session::SetId(oid_t id)
{
	mId = id;
}

int8_t Session::GetType()
{
	return this->mType;
}

void Session::Settype(int8_t type)
{
	mType = type;
}

tick_t Session::HeartBeat()
{
	return mLastHeartBeat;
}

void Session::UpdateHeartBeat()
{
	mLastHeartBeat = CLOCK.SystemTick();
}
