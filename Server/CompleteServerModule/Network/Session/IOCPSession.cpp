#include"stdafx.h"
#include"Session.h"
#include"IOCPSession.h"
#include"SessionManager.h"
#include"PacketAnalyzer.h"


IoData::IoData()
{
	ZeroMemory(&mOverlapped, sizeof(mOverlapped));
	mIoType = IO_ERROR;
	this->Clear();
}

void IoData::Clear()
{
	mBuffer.fill(0);
	mTotalBytes = 0;
	mCurrentBytes = 0;
}

int32_t IoData::SetupTotalBytes()
{
	packet_size_t offset = 0;
	packet_size_t packetLen = 0;
	if (mTotalBytes == 0) {
		memcpy_s((void*)&packetLen, sizeof(packet_size_t), (void *)mBuffer.data(), sizeof(packet_size_t));
		mTotalBytes = (size_t)packetLen;
	}
	if (mTotalBytes == 0) {
		SLog(L"!!_totalBytes is zero !!");
	}
	offset += sizeof(packetLen);
	return offset;
}

size_t IoData::GetTotalByte()
{
	return mTotalBytes;
}

IO_OPERATION & IoData::GetType()
{
	return mIoType;
}

void IoData::SetType(IO_OPERATION type)
{
	mIoType = type;
}

WSABUF IoData::wsaBuf()
{
	WSABUF wsaBuf;
	wsaBuf.buf = mBuffer.data() + mCurrentBytes;
	wsaBuf.len = (ULONG)(mTotalBytes - mCurrentBytes);
	return wsaBuf;
}

char * IoData::GetData()
{
	return mBuffer.data();
}

bool IoData::SetData(Stream & stream)
{
	this->Clear();

	if (mBuffer.max_size() <= stream.GetSize()) {
		SLog(L"!! Packet size very big. this size [%d] byte !!", stream.GetSize());
		return false;
	}
	packet_size_t offset = 0;
	char *buf = mBuffer.data();

	packet_size_t packetLen = sizeof(packet_size_t) + (packet_size_t)stream.GetSize();

	// 데이텅 앞부분에 데이터의 총 크기를 작성
	memcpy_s(buf + offset, mBuffer.max_size(), (void *)&packetLen, sizeof(packetLen));

	// 앞 부분의 4바이트를 사용하였음 -> offset move
	offset += sizeof(packetLen);

	memcpy_s(buf + offset, mBuffer.max_size(), (void *)stream.GetData(), (int32_t)stream.GetSize());
	
	offset += (packet_size_t)stream.GetSize();

	mTotalBytes = offset;
	return true;
}

LPWSAOVERLAPPED IoData::GetOverlapped()
{
	return &mOverlapped;
}

bool IoData::LackIOBuf(size_t size)
{
	mCurrentBytes += size;
	if (mCurrentBytes < mTotalBytes) {
		SLog(L"######## Total byte [%d] leak of Buf size [%d] ######## ", mTotalBytes, mTotalBytes - mCurrentBytes);
		return true;
	}
	return false;
}

void IOCPSession::Initialize()
{
	ZeroMemory(&mSocketData, sizeof(SOCKET_DATA));
	mWork = nullptr;
	mIoData[IO_READ].SetType(IO_READ);
}

void IOCPSession::checkErrorIo(DWORD ret)
{
	if (ret == SOCKET_ERROR && (WSAGetLastError() != ERROR_IO_PENDING)) {
		SLog(L"!! socket error : %d ", WSAGetLastError());
		SessionManager::getInstance().CloseSession(this);
	}
}

DWORD IOCPSession::Recv(WSABUF wsaBuf)
{
	DWORD flag = 0;
	DWORD recvBytes = 0;
	DWORD ret = WSARecv(mSocketData.mSocket, &wsaBuf, 1, &recvBytes, &flag, mIoData[IO_READ].GetOverlapped(), NULL);
	this->checkErrorIo(ret);
	return recvBytes;

}

bool IOCPSession::IsRecving(size_t size)
{
	// recv buf 가 아직 다 안받아 졌는지 확인.
	if (mIoData[IO_READ].LackIOBuf(size)) {
		SLog(L"[uid :%d this Packet recv is leak ]", this->mUid);
		return true;
		/*if (this->recv(_ioData[IO_READ].wsaBuf())) 
			return false;
		else
			return true;*/
	}
	return false;
}

void IOCPSession::Send(WSABUF wsaBuf, IoData * data)
{
	DWORD flag = 0;
	DWORD sendBytes = 0;
	DWORD ret = WSASend(mSocketData.mSocket, &wsaBuf, 1, &sendBytes, flag, data->GetOverlapped(), NULL);
	this->checkErrorIo(ret);
}

IOCPSession::IOCPSession()
{
	this->Initialize();
}

void IOCPSession::OnSend(size_t size, IoData * data)
{
	if (data->LackIOBuf(size)) {
		this->Send(data->wsaBuf(), data);
		SLog(L"To many Sending, rework to Send packet");
		return;
	}
	SAFE_DELETE(data);
}

void IOCPSession::SendPacket(Packet * packet)
{
	Stream stream;
	packet->encode(stream);
	IoData * data = new IoData();
	if (!data->SetData(stream)) {
		return;
	}
	data->SetType(IO_WRITE);
	/*if (!_ioData[IO_WRITE].setData(stream)) {
		return;
	}*/
	WSABUF wsabuf;
	wsabuf.buf = data->GetData();
	wsabuf.len = (ULONG)data->GetTotalByte();//size();

	this->Send(wsabuf, data);
}

Package * IOCPSession::OnRecv(INT64 & size)
{
	packet_size_t offset = 0;
	offset += mIoData[IO_READ].SetupTotalBytes();
	if (this->IsRecving(size)) {
		// 부족하게 받아 더 리시브를 함
		DWORD recvSize = this->Recv(mIoData[IO_READ].wsaBuf());
		// 받은 패킷이 바로 완료가 되었으며 그 크기가 이전에 못받은것보다 같거나 크다면?
		if(recvSize >= mIoData[IO_READ].wsaBuf().len)
		{
			size += recvSize;
		}
		else if (recvSize == 0)
		{
			size = -1;
			return nullptr;
		}
	}
	packet_size_t packetdataSize = (packet_size_t)mIoData[IO_READ].GetTotalByte() - sizeof(packet_size_t);
	byte * packetData = (byte*)mIoData[IO_READ].GetData() + offset;

	Packet *packet = PacketAnalyzer::getInstance().analyzer((const char*)packetData, packetdataSize);
	if (packet == nullptr) {
		ASSERT(false);
		SLog(L"!! Not regist packet Type!!");
		this->OnClose();
		return nullptr;
	}
	// 만약 패킷 이상의 데이터를 받았을 경우...
	size -= mIoData[IO_READ].GetTotalByte();
	if (size > 0)
	{
		SLog(L"Too Many Data Recving Byte : [ %d ]", size);
		array<char, SOCKET_BUF_SIZE> _buffer;
		memcpy_s((void *)_buffer.data(), SOCKET_BUF_SIZE,
			mIoData[IO_READ].GetData() + mIoData[IO_READ].GetTotalByte(), size);
		mIoData[IO_READ].Clear();
		memcpy_s((void *)mIoData[IO_READ].GetData(), SOCKET_BUF_SIZE, _buffer.data(), _buffer.size());
	}
	//this->recvStandBy();
	Package * package = new Package(this, packet);
	return package;

}

void IOCPSession::RecvStandBy()
{
	mIoData[IO_READ].Clear();

	WSABUF wsabuf;
	wsabuf.buf = mIoData[IO_READ].GetData();
	wsabuf.len = SOCKET_BUF_SIZE;

	this->Recv(wsabuf);
}