#pragma once
#include"stdafx.h"

class Packet;
class IoData;
class Package;
enum {
	SESSION_TYPE_TERMINAL,
	SESSION_TYPE_CLIENT,
};
struct SOCKET_DATA {
	SOCKET mSocket;
	SOCKADDR_IN mAddrInfo;
};
class Session
{
protected:
	SOCKET_DATA mSocketData;
	oid_t mId;
	INT64 mUid;
	int8_t mType;

	bool SetsocketOption();
	tick_t mLastHeartBeat; // 클라이언트와 신호가 일정 시간 이하로 없을 경우 확인용

public:
	Session();
	virtual ~Session();

	virtual bool OnAccept(SOCKET socket, SOCKADDR_IN addrinfo);
	virtual void OnSend(size_t transferSzie, IoData * data) = 0;
	virtual void SendPacket(Packet * packet) = 0;
	virtual Package * OnRecv(INT64 & transfersize) = 0;
	virtual void RecvStandBy() {};
	virtual void OnClose();

	SOCKET & GetSocket();
	str_t ClientAddress();

	oid_t GetId();
	void SetId(oid_t id);
	void Setuid(INT64 uid) { mUid = uid; }
	INT64 Uid() { return mUid; }

	int8_t GetType();
	void Settype(int8_t type);

	tick_t HeartBeat();
	void UpdateHeartBeat();

	
};