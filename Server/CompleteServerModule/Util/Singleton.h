#pragma once
#include"stdafx.h"

template<class T>
class Singleton
{
protected:
	Singleton() {}
	~Singleton() {
		cout << "�Ҹ��� ȣ��" << endl;
	}
public:
	Singleton(const Singleton&);
	Singleton& operator = (const Singleton &);
	static T& getInstance()
	{
		static T instance;
		return instance;
	}
};