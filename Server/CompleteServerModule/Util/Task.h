#pragma once
#include"stdafx.h"

#define TICK_INFINTY 0 
class Work;

class TaskNode
{
public:
	// tick_t _nextTick;
	INT64 mNextTick;

	TaskNode(Work * inWorkObject , int inFreqSec, int inDuraSec);
	~TaskNode();

	void NextTick();

	bool IsExpired();

	void Tick(HANDLE * handle);

	void Tick();
private:
	Work * mWorkobject;
	int mFreqencySec;
	int mDurationSec;
};
class mycomp
{
public:
	bool operator() (const TaskNode * lhs, const TaskNode * rhs) const
	{
		return (lhs->mNextTick > rhs->mNextTick);
	}
};

class TaskManager : public Singleton<TaskManager>
{
public:
	TaskManager();
	~TaskManager();
	void Add(TaskNode * taskNode);

	void Process();
	void Run();
#if BESIDEYOU
	void AddiocpHandle(HANDLE * handle);
#endif
private:
#if BESIDEYOU
	HANDLE *												mIocpHandle;
#endif
	priority_queue<TaskNode *, vector<TaskNode *>, mycomp>  mTaskqueue;
	Thread *												mThread;
	Lock													mLock;
	int														mId;
};
