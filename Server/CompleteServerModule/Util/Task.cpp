#include"stdafx.h"
#include"../Network/Session/IOCPSession.h"
#include "Task.h"

TaskNode::TaskNode(Work * workObject,int freqSec, int duraSec)
{
	mWorkobject = workObject;
	mFreqencySec= freqSec;
	mDurationSec = duraSec;
	this->NextTick();
}

TaskNode::~TaskNode()
{
	SAFE_DELETE(mWorkobject);
}

void TaskNode::NextTick()
{
	mNextTick = NOW_TICKTP() + mFreqencySec;
}

bool TaskNode::IsExpired()
{
	// 
	if (mWorkobject == nullptr) {
		return true;
	}
	if (mDurationSec != TICK_INFINTY) {
		// 지속적으로 Task가 아닌경우
		if (mDurationSec < NOW_TICKTP()) {
			// Task의 총 시간이 끝났을경우
			return true;
		}
	}
	return false;
}
void TaskNode::Tick()
{
	auto now_ms = NOW_TICKTP();
	if (now_ms > mNextTick) {
		mWorkobject->Tick();
		this->NextTick();
	}
}

void TaskNode::Tick(HANDLE * handle)
{
	auto now_ms = NOW_TICKTP();
	if (now_ms > mNextTick) {
		// ERR 발생 위험..
		if (handle != nullptr) {
			IOCPSession * session = new IOCPSession();
			session->mIoData[IO_READ].SetType(IO_FUNC);
			session->mWork = mWorkobject;
			auto result = PostQueuedCompletionStatus(*handle, 1, (ULONG_PTR)session, session->mIoData[IO_READ].GetOverlapped());
			if (!result) {
				auto errcode = ::GetLastError();
				SLog(L"!! ERR . postqueuedCompletionStatus %d", errcode);

			}
		}
		this->NextTick();
	}
}

TaskManager::TaskManager() : mLock(L"TaskManager")
{
	this->Run();
}

TaskManager::~TaskManager()
{
	TaskNode * deleNode;
	while (!mTaskqueue.empty()) {
		deleNode = mTaskqueue.top();
		mTaskqueue.pop();
		SAFE_DELETE(deleNode);
	}
}

void TaskManager::Add(TaskNode * taskNode)
{
	mLock.lock(_W(__FILE__), __LINE__);
	mTaskqueue.push(taskNode);
	mLock.Unlock();
}

void TaskManager::Process()
{
	while (!_shutdown) {
		CONTEXT_SWITCH;
		mLock.lock(_W(__FILE__), __LINE__);
		while(!mTaskqueue.empty()) {
			if (mTaskqueue.top()->IsExpired()) 
			{
				// 만료 Task 삭제
				TaskNode * delNode = mTaskqueue.top();
				mTaskqueue.pop();
				SAFE_DELETE(delNode);
				continue;
			}
			TaskNode * addNode = mTaskqueue.top();
			mTaskqueue.pop();
#if BESIDEYOU
			addNode->Tick(mIocpHandle);
#endif
			mTaskqueue.push(addNode);
			break;
		}
		mLock.Unlock();
	}
}

void TaskManager::Run()
{
	mThread = MAKE_THREAD(TaskManager, Process);
	printf("** Task Thread Create \n");
}
#if BESIDEYOU
void TaskManager::AddiocpHandle(HANDLE * handle)
{
	mIocpHandle = handle;
}
#endif
