#include"stdafx.h"
#include"Thread.h"

Thread::Thread(thread_t * thread, wstr_t name)
{
	std::hash<std::thread::id> hasher;
	mName = name;
	mThread = thread;
	mId = hasher(mThread->get_id());

	ThreadManager::getInstance().Put(this); // ThreadManager등록 1
}

Thread::~Thread()
{
	mThread->join();
	SAFE_DELETE(mThread);
	SAFE_DELETE(mLock);
}

size_t Thread::GetId()
{
	return mId;
}

wstr_t & Thread::GetName()
{
	return mName;
}

void Thread::SetLock(Lock * lock)
{
	mLock = lock;
}

Lock * Thread::Getlock()
{
	return mLock;
}

ThreadManager::~ThreadManager()
{
	for (auto thread : mThreadPool) {
		SAFE_DELETE(thread.second);
	}
}

void ThreadManager::Put(Thread * thread)
{
	mThreadPool.emplace(thread->GetId(), thread);  // ThreadManager등록 2
	SLog(L"** create thread: id[0x%x] || name[%s], pool size[%d]", 
		thread->GetId(), thread->GetName().c_str(), mThreadPool.size());
}

void ThreadManager::Remove(size_t id)
{
	auto iter = mThreadPool.find(id);
	if (iter == mThreadPool.end()) {
		return;
	}
	auto thread = iter->second;
	mThreadPool.erase(iter);  // ThreadManager 삭제 1
}

Thread * ThreadManager::At(size_t id) // Thread return;
{
	if (mThreadPool.empty()) {
		return nullptr;
	}
	auto iter = mThreadPool.find(id);
	if (iter == mThreadPool.end()) {
		return nullptr;
	}
	auto thread = iter->second;
	return thread;
}
