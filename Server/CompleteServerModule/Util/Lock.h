#pragma once
#include"stdafx.h"
// Lock class

// Lock사용의 추적을 위한 클래스
// 추적을 위해 !!
class Lock
{
public:
	Lock(WCHAR * name);
	virtual ~Lock();

	const WCHAR* Name();
	size_t LockId();

	lock_t & Mutex();
	void lock(LPCWSTR inFileName, int inLineNumber);
	void Unlock();

	void SetThreadId(size_t inId);
	size_t ThreadId();
private:
	lock_t mMutex;
	wstr_t mName;
	size_t mLockId;
	size_t mThreadId;

	wstr_t mCheakingFile;
	int mCheakingLine;

};


class LockSafe {
public:
	LockSafe(Lock * inLock, LPCWSTR inFileName, int inLineNo);
	~LockSafe();
private:
	Lock * mLock;

};

#define SAFE_LOCK(lock)		LockSafe __locksafe(&lock, _W(__FILE__), __LINE__);
class LockManager : public Singleton < LockManager >
{
	size_t mIdSeed;
public:
	std::hash<thread::id> mHashId;

public:
	LockManager();

	Lock * SearchLockCycle(Lock * inNewLock);
	Lock * CheckDeadLock(Lock * Thislock);

	size_t GeneralId();
};