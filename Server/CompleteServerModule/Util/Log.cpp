#include "stdafx.h"
#include "Log.h"

LogPrintf::LogPrintf()
{
	printf("- Log create : printf Log Mode\n");
}

void LogPrintf::Log(WCHAR * inLogStr)
{
	// 햇갈리기 쉬운 %s , %S -> 
	// %s 사용하는 함수와 동일한 문자열 타입으로 출력
	// %S 사용하는 함수와 반대되는 문자열 타입으로 출력
	// wachr_t * str;
	// printf("%S", str); 정답
	// %ws : 함수와 상관없이 'wchar_t *' 로 변환
	// %hs : 함수와 상관없이 'char *' 로 변환
	printf("%ws", inLogStr);
}

LogFile::LogFile(jsonValue_t * inConfigPtr)
{
	jsonValue_t log = (*inConfigPtr)["Log"];
	string cpath;
	std::array<WCHAR, SIZE_256> path;
	cpath = log.get("Path", "").asString();

	StrConvA2W(&log.get("Path", "").asString().front(), path.data(), path.max_size());
	printf("* Log is Create : [%ws]file Log mode.. \n", path.data());
	this->Initialize(path.data());
}

LogFile::~LogFile()
{
	mFile.close();
	mFile.clear();

	size_t found = mFileName.find(L".log");
	if (found == wstr_t::npos) {
		return;
	}

	wstr_t closeFilename = mFileName.substr(0, found);
	closeFilename += CLOCK.NowTime(L"_%Y%m%d-%H%M%S.log");
	// LogFile name 변경 함수
	_wrename(mFileName.c_str(), closeFilename.c_str());
}

void LogFile::Initialize(WCHAR * inLogFileName)
{
	mFileName = inLogFileName;
	mFile.open(inLogFileName, std::ios::out | std::ios::trunc);
	if (mFile.fail()) {
		printf("! 에러발생\n");
	}
	if (mFile.bad()) {
		printf("! LogFile error .... file open Failed.\n");
		assert(false);
	}
}

void LogFile::Log(WCHAR * inLogStr)
{
	printf("%ws", inLogStr);
	mFile << inLogStr;
	mFile.flush();
}

SystemLog::SystemLog()
{
	jsonValue_t config;
	if (!loadConfig(&config)) {
		printf(" !!! have not config file !!! \n");
		exit(0);
		return;
	}
	this->Initialize(&config);
}

SystemLog::~SystemLog()
{
}

void SystemLog::Initialize(jsonValue_t * inConfig)
{
	// FIXME : if가 재대로 걸릴지 안 걸릴지 모름.... 테스트가 필요하다!
	if (!inConfig->get("Log", "").isObject()) {
		printf("@ not exist Log setting @\n");
		BaseLog *base = new LogPrintf();
		mLogWrite.SetLogger(base, L"LiveServer");
		return;
	}

	jsonValue_t log = (*inConfig)["Log"];
	
	std::array<WCHAR, SIZE_256> temp;

	StrConvA2W((char *)log.get("Prefix", "").asString().c_str(),temp.data(), temp.max_size());
	wstr_t prefix = temp.data();
	
	BaseLog * base;
	std::string typetemp;
	typetemp = log.get("Type", "").asString();
	if (!strcmp(typetemp.c_str(), "WithFile")) {
		base = new LogFile(inConfig);
	}
	else {
		base = new LogPrintf();
	}
	mLogWrite.SetLogger(base, prefix.c_str());
}

void SystemLog::Log(WCHAR * fmt, ...)
{
	// 가변인자함수 제작.  va_list
	va_list arg;
	// 시작점 으로 이동
	va_start(arg, fmt);
	mLogWrite.Log(fmt, arg);
	va_end(arg);
}

LogWriter::LogWriter()
{
	mBase = nullptr;
}

LogWriter::~LogWriter()
{
	mPrefix.clear();
	SAFE_DELETE(mBase);
}

void LogWriter::SetLogger(BaseLog * inBase, const WCHAR * inLogPrefix)
{
	mPrefix.clear();
	mPrefix = inLogPrefix;

	if (mBase) {
		BaseLog * old = mBase;
		mBase = nullptr;
		SAFE_DELETE(old);
	}
	mBase = inBase;
	mBase->Initialize();
}

BaseLog * LogWriter::Logger()
{
	return mBase;
}

void LogWriter::Log(WCHAR * fmt, ...)
{
	va_list arg;
	va_start(arg, fmt);

	this->Log(fmt, arg);

	va_end(arg);
}

void LogWriter::Log(WCHAR * fmt, va_list args)
{
	wstr_t logMessage = CLOCK.NowTimeWithMilliSec();
	size_t threadId = 0;
	GET_CURRENT_THREAD_ID(&threadId);

	logMessage += L"\t";

	// 스레드 정보 기입
	// 위험요소 코드... 나중에 확인해야함.
	Thread * pthread = ThreadManager::getInstance().At(threadId);
	if (pthread) {
		logMessage += pthread->GetName();
	}
	else {
		logMessage += mPrefix;
	}

	array<WCHAR, SIZE_8 * 2> threadIdStr;
	snwprintf(threadIdStr, L"0x%X", threadId);

	logMessage += L" : ";
	logMessage += threadIdStr.data();
	logMessage += L"\t";

	array<WCHAR, SIZE_1024> logStr;

	vswprintf_s(logStr.data(), logStr.size(), fmt, args);

	logMessage += logStr.data();
	logMessage += L"\n";
	mBase->Log((WCHAR *)logMessage.c_str());
}
