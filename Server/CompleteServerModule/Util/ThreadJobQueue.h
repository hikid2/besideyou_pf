#pragma once
#include"stdafx.h"
#include<queue>
template<class T>
class ThreadJobQueue {
public:
	ThreadJobQueue(WCHAR * name) : mLock(name)
	{
		mWriteQueuePtr = &mQueue[WRITE_QUEUE];
		mReadQueuePtr = &mQueue[READ_QUEUE];
	}
	~ThreadJobQueue()
	{
		if (!this->mReadQueuePtr->empty()) {
			// TODO : 예상못한 큐의 제거시 필요한 작업 작성
		}
		
		if (!this->mWriteQueuePtr->empty()) {
			// TODO : 예상못한 큐의 제거시 필요한 작업 작성
		}
	}
	inline void Push(const T & inElement)
	{
		SAFE_LOCK(mLock);
		mWriteQueuePtr->push(inElement);
	}
	// 
	inline bool Pop(T & inElement) {
		SAFE_LOCK(mLock);
		size_t temtSize = this->Size();
		if (temtSize == 0) {
			return false;
		}
		if (mReadQueuePtr->empty()) {
			this->Swap();
		}
		inElement = mReadQueuePtr ->front();
		mReadQueuePtr->pop();
		return true;
		
	}

	inline void Swap()
	{
		SAFE_LOCK(mLock);
		if (mWriteQueuePtr == &mQueue[WRITE_QUEUE]) {
			mWriteQueuePtr = &mQueue[READ_QUEUE];
			mReadQueuePtr = &mQueue[WRITE_QUEUE];
		}
		else {
			mWriteQueuePtr = &mQueue[WRITE_QUEUE];
			mReadQueuePtr = &mQueue[READ_QUEUE];
		}
	}

	inline bool IsEmpty() { 
		return mReadQueuePtr->empty(); 
	}

	inline size_t Size()
	{
		SAFE_LOCK(mLock);
		size_t tempSize = (size_t)(mQueue[WRITE_QUEUE].size() + mQueue[READ_QUEUE].size());
		return tempSize;
	}

private:
	enum {
		WRITE_QUEUE,
		READ_QUEUE,
		MAX_QUEUE
	};

	std::queue<T> mQueue[MAX_QUEUE]; // 0 : WRITE_QUEUE, 1 : READ_QUEUE

	queue<T> * mWriteQueuePtr;

	queue<T> * mReadQueuePtr;

	Lock mLock;
};