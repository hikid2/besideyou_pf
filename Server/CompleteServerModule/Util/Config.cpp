#include"stdafx.h"
#include"Config.h"
#include<fstream>

bool ReadFromFile(const char * inFileName, str_t & inReadBuffer)
{
	ifstream file(inFileName);
	if (!file.is_open()) {
		printf("! File err.have not Config file\n");
		return false;
	}
	file.seekg(0, ios::end);
	int size = (int)file.tellg();
	file.seekg(0, ios::beg);
	inReadBuffer.resize(size);
	file.read(&inReadBuffer[0], size);
	file.close();
	return true;
}

bool loadConfig(jsonValue_t * root)
{
	str_t readBuffer;
	if (!ReadFromFile(".\\config.json", readBuffer)) {
		return false;
	}
	jsonReader_t reader;
	if (!reader.parse(readBuffer, *root)) {
		printf("!! Json Parse Err. !!\n");
		return false;
	}
	return true;
}

