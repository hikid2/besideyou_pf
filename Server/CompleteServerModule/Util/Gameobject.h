#pragma once
#include "stdafx.h"

class Object
{
	wstr_t			allocFile_;
	int				allocLine_;
};

class NameObject
{
	wstr_t mName;
public:
	wstr_t& GetName()
	{
		return mName;
	}

	void SetName(wstr_t name)
	{
		mName = name;
	}
};

class Work
{
public:
	virtual void Tick() = 0;
	virtual void wakeup() {};
	virtual void suspend() {};
	virtual void stop() {};
	virtual void start() {};
};

class GameObject : public NameObject, public Work
{
public:
	// 클래스 이름 마다 소멸자 이름이 다르므로, free로 통일 시키자
	virtual ~GameObject()
	{
		this->free();
	}
	virtual void Tick() {};
	virtual void Initialize() {};
	virtual void free() {};
	virtual void SendOutgoingPackets() {};
	void SetIndexInWorld(const INT64 & index) { mindexInWorld = index; }
	INT64 GetIndexInWorld() const { return mindexInWorld; }
private:
	INT64		mindexInWorld;
};


//typedef shared_ptr< GameObject >	GameObjectPtr;


class World : public Singleton<World>
{
	std::vector< GameObject * >	mGameObjects;
	Lock mLock;
public:
	World() : mLock(L"World"){}
	virtual ~World(){}
	void AddGameObject(GameObject * ptr)
	{
		SAFE_LOCK(mLock);
		mGameObjects.push_back(ptr);
		ptr->SetIndexInWorld(mGameObjects.size() - 1);
	}
	void RemoveGameObject(GameObject * inGameObject)
	{
		SAFE_LOCK(mLock);
		int64_t index = inGameObject->GetIndexInWorld();

		int64_t lastIndex = (int64_t)mGameObjects.size() - 1;
		if (index != lastIndex)
		{
			mGameObjects[index] = mGameObjects[lastIndex];
			mGameObjects[index]->SetIndexInWorld(index);
		}

		inGameObject->SetIndexInWorld(-1);

		mGameObjects.pop_back();
	}

	void Update()
	{
		SAFE_LOCK(mLock);
		for (int i = 0, c = (int)mGameObjects.size(); i < c; ++i)
		{
			GameObject * go = mGameObjects[i];

			go->Tick();
		}
	}
	
};