#pragma once
#include "stdafx.h"
#include <chrono>
#include <ctime>

#define CLOCK					Clock::getInstance()
#define NOW_TICK				CLOCK.SystemTick
#define NOW_TICKTP				CLOCK.SystemTickTP
#define NOW_STRING				CLOCK.NowTime

#define TICK_MIN                (60)
#define TICK_HOUR               (TICK_MIN * 60)
#define TICK_DAY                (TICK_HOUR * 24)

#define TICK_TO_MIN(x)          (x / TICK_MIN)
#define MIN_TO_TICK(x)          (x * TICK_MIN)

#define TICK_TO_HOUR(x)         (x / TICK_HOUR)        
#define HOUR_TO_TICK(x)         (x * TICK_HOUR)

#define TICK_TO_DAY(x)          (x / TICK_DAY)
#define DAY_TO_TICK(x)          (x * TICK_DAY)

typedef enum {
	DAY_SUNDAY = 0,
	DAY_MONDAY = 1,        //�߱���� �������� ��Ѣ��
	DAY_TUESDAY = 2,        //�߱���� ȭ������ ��Ѣ�
	DAY_WEDNESDAY = 3,        //...
	DAY_THURSDAY = 4,
	DAY_FRIDAY = 5,
	DAY_SATURDAY = 6,
}DayOfTheWeek;

#define DATETIME_FORMAT         L"D%Y-%m-%dT%H:%M:%S"
#define DATE_FORMAT             L"%Y-%m-%d"
#define TIME_FORMAT             L"%H:%M:%S"
#define DB_TIME_FORMAT          L"%4d-%2d-%2d %2d:%2d:%2d"

using namespace std::chrono;
typedef system_clock::time_point timePoint;

namespace
{
	high_resolution_clock::time_point sStartTime;
}

class Clock : public Singleton<Clock>
{
	tick_t	serverStartTick_;

	float	mDeltaTime;
	UINT64	mdeltaTick;
	double	mLasfFrameStartTime;
	float	mFrameStartTimef;
	double	mPerfCountDuration;
public:
	void Update()
	{
		double currentTime = GetTime();

		mDeltaTime = (float)(currentTime - mLasfFrameStartTime);

		mLasfFrameStartTime = currentTime;
		mFrameStartTimef = static_cast< float > (mLasfFrameStartTime);

	}
	double GetTime() const
	{
		auto now = high_resolution_clock::now();
		auto ms = duration_cast< milliseconds >(now - sStartTime).count();
		//a little uncool to then convert into a double just to go back, but oh well.
		return static_cast< double >(ms) / 1000;
	}
	float GetTimef() const
	{
		return static_cast< float >(GetTime());
	}

	wstr_t	TickToStr(tick_t tick, WCHAR *fmt = DATETIME_FORMAT);

public:
	Clock();
	~Clock();

	tick_t	ServerStartTick();
	tick_t	SystemTick();
	tick_t	StrToTick(wstr_t str, WCHAR *fmt = DB_TIME_FORMAT);

	wstr_t	NowTime(WCHAR *fmt = DATETIME_FORMAT);
	wstr_t	NowTimeWithMilliSec(WCHAR *fmt = DATETIME_FORMAT);

	wstr_t Today();
	wstr_t Tomorrow();
	wstr_t Yesterday();

	DayOfTheWeek TodayOfTheWeek();
	INT64 SystemTickTP();
};