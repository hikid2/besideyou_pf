#pragma once
#include"stdafx.h"


// # 연산자(매개변수 변환용) , ## 연산자(전달인자를 다른대상에 이어줄때 사용.)
#define MAKE_THREAD(className, process) (new Thread(new thread_t(&className##::##process, this), L#className))
#define GET_CURRENT_THREAD_ID(number)															\
{																								\
	std::hash<std::thread::id> hasher;															\
	*number = hasher(std::this_thread::get_id());												\
}

class Lock;

typedef std::function<void(void *)> ThreadFunction;

//  Mean  : typedef void(*ThreadFuncttion)(void *)

class Thread {
public:
	Thread(thread_t * thread, wstr_t name);

	~Thread();

	size_t GetId();

	wstr_t &GetName();

	void SetLock(Lock *lock);

	Lock * Getlock();
private:
	size_t		mId;
	wstr_t		mName;
	thread_t *	mThread;			// 생성주기를 외부로!
	Lock	 *	mLock;

};

class ThreadManager : public Singleton <ThreadManager>
{
public:
	virtual ~ThreadManager();

	void Put(Thread * inThreadPtr);

	void Remove(size_t inId);

	Thread * At(size_t inId);
private:
	std::map <size_t, Thread *> mThreadPool;
};