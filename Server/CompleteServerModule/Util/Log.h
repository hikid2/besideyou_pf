#pragma once
#include"stdafx.h"
#include<fstream>
#include"Singleton.h"
#include"Type.h"


#define SLog(arg, ...)		SystemLog::getInstance().Log(arg, __VA_ARGS__);
#define SErrLog(arg, ...)	SystemLog::getInstance().Log(arg, __VA_ARGS__); ::ExitProcess(0);


class BaseLog
{
public:
	BaseLog() {}
	virtual ~BaseLog() {}
	virtual void Initialize() {}
	virtual void Log(WCHAR * inLogStr) = 0;
};


class LogPrintf : public BaseLog
{
public:
	LogPrintf();

	void Log(WCHAR * inLogStr);
};

class LogFile : public BaseLog
{
public:
	LogFile(jsonValue_t * config);

	virtual ~LogFile();

	void Initialize() {}

	void Initialize(WCHAR * logFileName);

	void Log(WCHAR * logStr);

private:
	std::wfstream mFile;
	wstr_t mFileName;
};

class LogWriter
{
public:
	LogWriter();

	virtual ~LogWriter();

	void SetLogger(BaseLog *base, const WCHAR *logPrefix);

	BaseLog * Logger();

	void Log(WCHAR *fmt, ...);
	 
	void Log(WCHAR *fmt, va_list args);
private:
	BaseLog * mBase;
	wstr_t mPrefix;
};


typedef LogWriter* LogWriterPtr;

class SystemLog : public Singleton<SystemLog>
{
private:
	LogWriter mLogWrite;
public:
	SystemLog();
	virtual ~SystemLog();

	void Initialize(jsonValue_t * config);

	void Log(WCHAR *fmt, ...);
};