#pragma once
#include"stdafx.h"
class TerminalManager : public Singleton<TerminalManager>
{
public:
	TerminalManager();
	virtual ~TerminalManager();

	void initialize(jsonValue_t * config);

	void put(wstr_t inServerName, Terminal * inTerminal);

	Terminal* terminal(wstr_t inName);
	
	bool isTerminal(const char * inIp);

	void run(Server * inServer);

private:
	unordered_map<wstr_t, Terminal *> mTerminalPool;
	Server * mServer;

};