#include"stdafx.h"
#include"TerminalManager.h"

TerminalManager::TerminalManager()
{
}

TerminalManager::~TerminalManager()
{
	for (auto  & iter : mTerminalPool) {
		auto & terminal = iter.second;
		SAFE_DELETE(terminal);
	}
}

void TerminalManager::initialize(jsonValue_t * config)
{
	jsonValue_t tervalue = (*config)["Terminal"];
	if (tervalue == NULL) {
		SErrLog(L"** not exist terminal setting **");
		return;
	}
	
	int terminalcount = tervalue.get("TerminalCount", 0).asInt();
	auto nameList = tervalue["NAME"];
	for (auto cnt = 0; cnt < terminalcount; ++cnt) {
		str_t temp;
		array<WCHAR, _MAX_PATH> terminalName;
		temp = nameList[cnt].asString();
		StrConvA2W((char *)temp.c_str(),terminalName.data(), terminalName.max_size());
		Terminal * terminal = new Terminal(mServer, terminalName.data());
		terminal->Initialize(&tervalue, cnt);

		this->put(terminalName.data(), terminal);
	}

	SLog(L"### Terminal Setting complete ~! ###");
}

void TerminalManager::put(wstr_t serverName, Terminal * terminal)
{
	mTerminalPool.insert(make_pair(serverName, terminal));
}

Terminal * TerminalManager::terminal(wstr_t name)
{
	return mTerminalPool.at(name);
}

bool TerminalManager::isTerminal(const char * inIp)
{
	for (auto iter : mTerminalPool) {
		if (!strcmp(inIp, iter.second->Ip())) {
			return true;
		}
	}
	return false;
}

void TerminalManager::run(Server * inServer)
{
	mServer = inServer;


	jsonValue_t config;
	if (!loadConfig(&config)) {
		return;
	}
	this->initialize(&config);
}
