#pragma once
#include"stdafx.h"

typedef enum {
	TERMINAL_STOP,
	TERMINAL_READY,
}TERMINAL_STATE;

class Server;
class Terminal
{
protected:
	Server *			mServer;
	TERMINAL_STATE		mState;
	wstr_t				mName;

	char				mIp[16];
	int					mPort;

	TerminalSession		mSession;
	Thread				* mTerminalthread;
public:
	Terminal(Server * inServer, wstr_t inName);
	virtual ~Terminal();
	TERMINAL_STATE &status();

	void		Initialize(jsonValue_t * config, int index);
	void		SendPacket(Packet * inPacketPtr);
	const char *Ip();
	int			Port();

private:
	void		ConnectProcess();
	void		Run();

};