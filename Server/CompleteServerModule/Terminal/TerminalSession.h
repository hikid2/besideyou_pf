#pragma once
#include"stdafx.h"
class TerminalSession : public Session
{
public:
	bool		ConnectTo(char * inIp, int inPort);
	void		OnSend(size_t inTransferSize, IoData * inData);
	void		SendPacket(Packet * inPacketPtr);

	Package*	OnRecv(INT64 & inTransferSize);
};