#include"stdafx.h"
#include"Terminal.h"

Terminal::Terminal(Server * server, wstr_t name)
{
	mServer = server;
	mName = name;
}

Terminal::~Terminal()
{
	mState = TERMINAL_STOP;
}

TERMINAL_STATE & Terminal::status()
{
	return mState;
}

void Terminal::Initialize(jsonValue_t * config, int index)
{
	jsonValue_t ip = config->get("IP", "");
	jsonValue_t port = config->get("PORT", "");
	strcpy_s(mIp, ip[index].asString().c_str());
	mPort = port[index].asInt();
	this->Run();
}

void Terminal::SendPacket(Packet * packet)
{
	if (mState == TERMINAL_READY) {
		mSession.SendPacket(packet);
	}
}

const char * Terminal::Ip()
{
	return mIp;
}

int Terminal::Port()
{
	return mPort;
}

void Terminal::ConnectProcess()
{
CONNECT_START:
	int connectTrycnt = 0;
	while (_shutdown == false) {
		if (mSession.ConnectTo(mIp, mPort)) {
			break;
		}
		SLog(L"** try connect [%s], server[%S]:[%d]... [%d]", mName.c_str(), mIp, mPort, connectTrycnt++);
		// 5초에 1번씩 연결 시도
		Sleep(5000);
	}
	// 연결 완료 상태 변경
	mState = TERMINAL_READY;
	// TODO : 터미널 섹션임을 알리는 패킷 제작 필요.
	PK_T_NTY_TERMINAL terminalPacket;
	this->SendPacket(&terminalPacket);

	SLog(L"****  [%s]terminal connect [%S]:[%d] ready  ****", mName.c_str(), mIp, mPort);

	while (_shutdown == false) {
		INT64 nothing = 0;
		Package * Tpackage = mSession.OnRecv(nothing);

		if (Tpackage == nullptr) {
			SLog(L"!! terminal [%s] disconnected !!", mName.c_str());
			mSession.OnClose();
			goto CONNECT_START;
			// 서버의 문제로 재부팅을 고려하여 다시 연결시도하게...
		}
		mServer->PutPackage(Tpackage);
	}
	
}

void Terminal::Run()
{
	mTerminalthread = MAKE_THREAD(Terminal, ConnectProcess);
}
