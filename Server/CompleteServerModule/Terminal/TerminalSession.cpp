#include"stdafx.h"
#include"TerminalSession.h"

bool TerminalSession::ConnectTo(char * inIp, int inPort)
{
	// 클라이언트로 연결 하는것과 같이....
	mSocketData.mSocket = ::socket(AF_INET, SOCK_STREAM, 0);
	if (mSocketData.mSocket == INVALID_SOCKET) {
		SLog(L"!!! Terminal socket fail !!");
		return false;
	}
	ZeroMemory(&mSocketData.mAddrInfo, sizeof(mSocketData.mAddrInfo));
	mSocketData.mAddrInfo.sin_family = AF_INET;
	mSocketData.mAddrInfo.sin_port = htons(inPort);
	inet_pton(AF_INET, inIp, &(mSocketData.mAddrInfo.sin_addr));

	this->SetsocketOption();

	int retval = ::connect(mSocketData.mSocket, (sockaddr *)&mSocketData.mAddrInfo, sizeof(mSocketData.mAddrInfo));
	if (retval == SOCKET_ERROR) {
		SLog(L"!! Terminal socket connect fail... !!");
		return false;
	}
	return true;
}

void TerminalSession::OnSend(size_t transferSize,IoData * data)
{
	// NOTYFY : 터미널에서 재 전송확인 필요 없음. 이유는 TCP라서 !!!
}

void TerminalSession::SendPacket(Packet * inPacket)
{
	Stream stream;
	inPacket->encode(stream);

	packet_size_t offset = 0;

	array<char, SOCKET_BUF_SIZE> buffer;
	// Headersize : 패킷의 총 크기 표현 크기
	const size_t packetHeadersize = sizeof(packet_size_t);
	packet_size_t packetLen = packetHeadersize + (packet_size_t)stream.GetSize();

	memcpy_s(buffer.data() + offset, buffer.max_size(), (void*)&packetLen, packetHeadersize);

	offset += packetHeadersize;
	memcpy_s(buffer.data() + offset, buffer.max_size(), (void*)stream.GetData(), (int32_t)stream.GetSize());

	offset += (packet_size_t)stream.GetSize();

	::send(mSocketData.mSocket, buffer.data(), offset, 0);
}

Package * TerminalSession::OnRecv(INT64 & inTransferSize)
{
	array<char, SOCKET_BUF_SIZE> data;
	int retval = ::recv(mSocketData.mSocket, data.data(), (int)data.size(), 0);
	if (retval <= 0) {
		return nullptr;
	}

	packet_size_t offset = 0;
	packet_size_t packetLen = 0;
	
	// 총 패킷 데이터 크기 
	memcpy_s((void *)packetLen, sizeof(packetLen), (void *)data.data(), sizeof(packetLen));
	
	offset += sizeof(packetLen);

	Packet * packet = PacketAnalyzer::getInstance().analyzer(data.data() + offset, packetLen);
	if (packet == nullptr) {
		return nullptr;
	}
	Package * package = new Package(this, packet);
	return package;
}
