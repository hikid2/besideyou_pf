#pragma once
#include"stdafx.h"
#include<sql.h>
#include<sqlext.h>
typedef enum
{
	DB_STOP,
	DB_STANDBY,
	DB_RUNING
}DB_STATE;
class Database
{
protected:
	DB_STATE	mState;
public:
	Database() {}
	virtual ~Database() {}
	virtual bool connect(const wchar_t * dbName, const wchar_t * id, const wchar_t *pw) = 0;
	virtual bool connect() = 0;
	virtual bool connected() = 0;
	virtual bool disconnect() = 0;

	virtual void run() = 0;
	DB_STATE &state() { return mState; }
};

class ODBCDatabase : public Database
{
private:
	SQLRETURN mRetcode;			// ��� ��
	SQLHSTMT mHstmt;			// statement Handle
	SQLHDBC mHdbc;				//  connection handle
	SQLHENV mHenv;				// environment handle
	wstr_t mDBName;				// DBName
	wstr_t mConnectionStr;		// ConnectionStr;
	bool mConnected;			// true = connnetion, false = disconnection

	Thread * _thread;
private:

	void HandleDiagnosticRecord(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode);

	void execute();
	void process();
	void run();
public:
	ODBCDatabase();
	virtual ~ODBCDatabase();
	virtual bool connect();
	bool connect(const wchar_t * drive, const wchar_t * server, const wchar_t * dataBase, const wchar_t * uid, const wchar_t * pw);
	virtual bool connect(const wchar_t * dbName, const wchar_t * id, const wchar_t *pw);
	virtual bool connected();
	virtual bool disconnect();

	
};