#include "stdafx.h"
#include "Query.h"
#include "ODBCDatabase.h"
#include "DBManager.h"

ODBCDatabase::ODBCDatabase()
{
	mHstmt = SQL_NULL_HSTMT;
	mHdbc = SQL_NULL_HDBC;
	mHenv = SQL_NULL_HENV;
	mState = DB_STOP;
	mConnected = false;
}

ODBCDatabase::~ODBCDatabase()
{
	if (connected())
		this->disconnect();
	SAFE_DELETE(_thread);
}

bool ODBCDatabase::connect()
{
	short short_result = 0;
	SQLWCHAR out_connect_str[1024] = L"";
	try
	{
		mRetcode = SQLAllocHandle(SQL_HANDLE_ENV, NULL, &mHenv);
		if (mRetcode != SQL_SUCCESS && mRetcode != SQL_SUCCESS_WITH_INFO) throw 0;
		mRetcode = SQLSetEnvAttr(mHenv, SQL_ATTR_ODBC_VERSION, (void*)SQL_OV_ODBC3, 0);
		if (mRetcode != SQL_SUCCESS && mRetcode != SQL_SUCCESS_WITH_INFO) throw 1;
		mRetcode = SQLAllocHandle(SQL_HANDLE_DBC, mHenv, &mHdbc);
		if (mRetcode != SQL_SUCCESS && mRetcode != SQL_SUCCESS_WITH_INFO) throw 2;
		SQLSetConnectAttr(mHdbc, SQL_LOGIN_TIMEOUT, (SQLPOINTER)5, 0);
		
		mRetcode = SQLDriverConnect(mHdbc, NULL, (SQLWCHAR *)&mConnectionStr.front(), lstrlenW(mConnectionStr.data()), out_connect_str, sizeof(wchar_t) * 1024, &short_result, SQL_DRIVER_NOPROMPT);
		if (mRetcode != SQL_SUCCESS && mRetcode != SQL_SUCCESS_WITH_INFO) throw 3;
		mRetcode = SQLAllocHandle(SQL_HANDLE_STMT, mHdbc, &mHstmt);
		if (mRetcode != SQL_SUCCESS && mRetcode != SQL_SUCCESS_WITH_INFO) throw 4;
	}
	catch (int thrNum)
	{
		switch (thrNum) {
		case 0:
			SLog(L"!! ODBC ERROR(0) :  SQLAllocHandle(SQL_HANDLE_ENV, NULL, &mHenv) !!");
			break;
		case 1:
			SLog(L"!! ODBC ERROR(1) :  SQLSetEnvAttr(mHenv, SQL_ATTR_ODBC_VERSION, (void*)SQL_OV_ODBC3, 0) !!");
			SQLFreeHandle(SQL_HANDLE_ENV, mHenv);
			break;
		case 2:
			SLog(L"!! ODBC ERROR(2) :  SQLAllocHandle(SQL_HANDLE_DBC, mHenv, &mHdbc) !!");
			SQLFreeHandle(SQL_HANDLE_ENV, mHenv);
			break;
		case 3:
			SLog(L"!! ODBC ERROR(3) :  SQLDriverConnect() !!");
			HandleDiagnosticRecord(mHdbc, SQL_HANDLE_DBC, mRetcode);
			SQLFreeHandle(SQL_HANDLE_DBC, mHdbc);
			SQLFreeHandle(SQL_HANDLE_ENV, mHenv);
			break;
		case 4:
			SLog(L"!! ODBC ERROR(4) :  SQLAllocHandle(SQL_HANDLE_STMT, mHdbc, &mHstmt) !!");
			SQLDisconnect(mHdbc);
			SQLFreeHandle(SQL_HANDLE_DBC, mHdbc);
			SQLFreeHandle(SQL_HANDLE_ENV, mHenv);
			break;
		default:
			break;
		}
		return false;
	}
	SLog(L"ODBC DataBase Connection Success");
	mConnected = true;
	mState = DB_STANDBY;
	return true;;
}

bool ODBCDatabase::connect(const wchar_t * drive, const wchar_t * server, const wchar_t * dataBase, const wchar_t * uid, const wchar_t * pw)
{
	// Drive : 현재 로컬 컴퓨터에 설치된 서버 혹은 클라이언트 트라이버명을 저확하게 명시한다.
	// Server : 서버의 TNS 이름을 "IP"형식으로 명시한다.
	// DataBase : "데이터 베이스의 이름을 명시한다."
	// Uid : 접속할 계정의 ID를 명시한다.
	// PWD : UID에 명시한 계정의 패스워드를 명시한다.
	// ex : char * connect_std = "DRIVE = {SQL Server};SERVER=xxx.xxx.xxx.xxx; DATABASE=TESTSOFT;UID=user_id;PWD=user_password;"
	std::array<wchar_t, SIZE_128> connect_std;

	snwprintf(connect_std, L"DRIVE={%s};SERVER=%s;DATABASE=%s;UID=%s;PWD=%s", drive, server, dataBase, uid, pw);
	SLog(L"Input connected Data : %s", connect_std.data());
	std::copy(connect_std.begin(), connect_std.end(), mConnectionStr.begin());

	return this->connect();
}

bool ODBCDatabase::connect(const wchar_t * dbName, const wchar_t * id, const wchar_t * pw)
{
	mDBName = dbName;
	// Allocate environment handle
	mRetcode = SQLAllocHandle(SQL_HANDLE_ENV, NULL, &mHenv);
	if (mRetcode != SQL_SUCCESS && mRetcode != SQL_SUCCESS_WITH_INFO) 
		return false; 

	// Set the ODBC version environment attribute
	mRetcode = SQLSetEnvAttr(mHenv, SQL_ATTR_ODBC_VERSION, (void*)SQL_OV_ODBC3, 0);
	if (mRetcode != SQL_SUCCESS && mRetcode != SQL_SUCCESS_WITH_INFO) {
		SQLFreeHandle(SQL_HANDLE_ENV, mHenv);
		return false; 
	}

	// Allocate connection handle
	mRetcode = SQLAllocHandle(SQL_HANDLE_DBC, mHenv, &mHdbc);
	if (mRetcode != SQL_SUCCESS && mRetcode != SQL_SUCCESS_WITH_INFO) {
		SQLFreeHandle(SQL_HANDLE_ENV, mHenv);
		return false;
	}

	// Login Timeout 5 Seconds
	SQLSetConnectAttr(mHdbc, SQL_LOGIN_TIMEOUT, (SQLPOINTER)5, 0);

	// Connect to data source
	mRetcode = SQLConnect(mHdbc, (SQLWCHAR*)dbName, SQL_NTS, (SQLWCHAR*)id, SQL_NTS, (SQLWCHAR*)pw, SQL_NTS);
	if (mRetcode != SQL_SUCCESS && mRetcode != SQL_SUCCESS_WITH_INFO) {
		HandleDiagnosticRecord(mHdbc, SQL_HANDLE_DBC, mRetcode);
		SQLFreeHandle(SQL_HANDLE_DBC, mHdbc);
		SQLFreeHandle(SQL_HANDLE_ENV, mHenv);
		return false;
	}

	// Allocate statement handle
	mRetcode = SQLAllocHandle(SQL_HANDLE_STMT, mHdbc, &mHstmt);
	if (mRetcode != SQL_SUCCESS && mRetcode != SQL_SUCCESS_WITH_INFO) {
		SQLDisconnect(mHdbc);
		SQLFreeHandle(SQL_HANDLE_DBC, mHdbc);
		SQLFreeHandle(SQL_HANDLE_ENV, mHenv);
		return false;
	}
	mConnected = true;
	SLog(L"ODBC DataBase Connection Success");
	mState = DB_STANDBY;
	return true;
}

bool ODBCDatabase::connected()
{
	return mConnected;
}

bool ODBCDatabase::disconnect()
{
	if (mState == DB_RUNING) {
		return false;
	}
	if(mHstmt)SQLFreeHandle(SQL_HANDLE_STMT, mHstmt);
	if(mHdbc)SQLDisconnect(mHdbc);
	if(mHdbc)SQLFreeHandle(SQL_HANDLE_DBC, mHdbc);
	if(mHenv)SQLFreeHandle(SQL_HANDLE_ENV, mHenv);
	mConnected = false;
	mState = DB_STOP;
	return true;
}

void ODBCDatabase::HandleDiagnosticRecord(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode)
{
	SQLSMALLINT iRec = 0;
	SQLINTEGER  iError;
	WCHAR       wszMessage[1000];
	WCHAR       wszState[SQL_SQLSTATE_SIZE + 1];


	if (RetCode == SQL_INVALID_HANDLE)
	{
		fwprintf(stderr, L"Invalid handle!\n");
		return;
	}

	while (SQLGetDiagRec(hType,
		hHandle,
		++iRec,
		wszState,
		&iError,
		wszMessage,
		(SQLSMALLINT)(sizeof(wszMessage) / sizeof(WCHAR)),
		(SQLSMALLINT *)NULL) == SQL_SUCCESS)
	{
		// Hide data truncated..
		if (wcsncmp(wszState, L"01004", 5))
		{
			fwprintf(stderr, L"[%5.5s] %s (%d)\n", wszState, wszMessage, iError);
		}
	}
}

void ODBCDatabase::execute()
{
	// 1. 큐에 값이 없을경우 return
	if (DBManager::getInstance().runQueryCount() == 0) {
		return;
	}

	// 2. 큐에 데이터를 꺼냄
	Query * qy = nullptr;
	if (!DBManager::getInstance().popQuery(&qy)){
		SLog(L"!! DBQueue Pop is Not Completeed");
		return;
	}
	mState = DB_RUNING;
	QueryStatement * state = qy->statement();
	// 3. 꺼낸 데이터를 db에 전송
	try {
		mRetcode = SQLExecDirect(mHstmt, (SQLWCHAR*)state->query(), SQL_NTS);
		if (mRetcode != SQL_SUCCESS && mRetcode != SQL_SUCCESS_WITH_INFO)throw;
		if (mRetcode == SQL_ERROR) {
			MessageBox(NULL, L"에러 발생", L"에러발생", NULL);
		}
	}
	catch (...) {
		DBManager::getInstance().pushQuery(qy);
		SQLFreeStmt(mHstmt, SQL_CLOSE);
		mState = DB_STANDBY;
		return;
	}
	switch (state->type())
	{
	case QUERY_NOT_RETURN:
		break;
	case QUERY_CALL_BACK:
		qy->recode().setColumParam(&mHstmt);
		break;
	case QUERY_WAIT_RETURN:
		qy->recode().setColumParam(&mHstmt);
		break;
	}
	// 4. 후 처리 작업.
	SAFE_DELETE(qy);
	SQLFreeStmt(mHstmt, SQL_CLOSE);
	mState = DB_STANDBY;
	return;
}

void ODBCDatabase::process()
{
	while (_shutdown == false) {
		if (!this->connected()) {
			SLog(L"!! db[%s] Connection is disconnection", mDBName.c_str());
			ASSERT(FALSE);
		}
		this->execute();
		CONTEXT_SWITCH;
	}
}

void ODBCDatabase::run()
{
	_thread = MAKE_THREAD(ODBCDatabase, process);
}
