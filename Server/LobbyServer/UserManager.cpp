#include"stdafx.h"
#include"User.h"
#include"UserManager.h"

UserManager::UserManager() : mLock(L"UserManager")
{
	for (auto i = 0; i < 500; ++i) {
		mUserPool[i] = new User();
	}
	SLog(L"** _userPool 'new' Create **");
}

UserManager::~UserManager()
{
	for (auto & user : mUserPool)
		SAFE_DELETE(user);
	SLog(L"** _userPool ALL DELETE **");
}

INT64 UserManager::login(UINT64 & inOID, wstr_t & inStringID)
{
	if (!mLogoutIndex.empty()) {
		mLock.lock(__FILEW__, __LINE__);
		int64_t index = *mLogoutIndex.begin();
		mLogoutIndex.erase(mLogoutIndex.begin());
		auto result = mLoginIndex.insert(index);
		mLock.Unlock();
		if (result.second == false) SLog(L" !!!! UserManager Login Error !!!");
		mUserPool[index]->SetData(inOID, index, inStringID);
		return index;
	}
	for (auto i = 0; i < 500; ++i) {
		if (mUserPool[i]->GetUid() != -1) continue;
		mUserPool[i]->SetData(inOID, i, inStringID);
		mLock.lock(__FILEW__, __LINE__);
		auto result = mLoginIndex.insert(i);
		mLock.Unlock();
		return i;
	}
	return -1;
}

void UserManager::logout(INT64 inUID)
{
	SAFE_LOCK(mLock);
	mLoginIndex.erase(inUID);
	mLogoutIndex.insert(inUID);
	mUserPool[inUID]->Clear();
}

User * UserManager::getUser(INT64 uid)
{
	return mUserPool[uid];
}
