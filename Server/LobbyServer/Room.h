#pragma once

class Room
{
public:
	Room() {}

	Room(int inNumbering) : mRoomNumber(inNumbering) {}

	~Room() {}

	bool Insert(User * inUser);

	void Erase(const INT64 & inoid);

	void UpdateRatingScore();

	std::pair<int, int> JobCount();

	const size_t size() const { return mUserList.size(); }

	const int & GetKillerScore()const { return mRating.GetKillerScore(); }

	const int & GetSurvivorScore()const { return mRating.GetSurvivorScore(); }


	void PlayerStateUpdate(INT64 inUid, bool inReady);

	void SendAddrass();

	void EndGame();

	bool IsStart() { return mIsStart; }
private:
	list<User * > mUserList;
	RoomRater mRating;
	int mRoomNumber;
	bool InsertAvaliable(User * inUser);
	bool mIsStart = false;
};