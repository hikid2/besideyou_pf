#include "stdafx.h"
#include "User.h"

int16_t User::GetRoomNumber()
{
	switch (mLocation)
	{
	case LocationState::INGAME:
		cout << "[error 101 ]" << endl;
		return -1;
		break;
	case LocationState::LOBBY:
		cout << "[error 102 ]" << endl;
		return -1;
		break;
	case LocationState::ROOM:
		return mRoomNumber;
		break;
	default:
		break;
	}
	return -1;
}

void User::SetData(const UINT64 & inOid, const INT64 & inUid, const wstring & inName)
{
	mName.assign(inName.begin(), inName.end());
	mOid = inOid;
	mUid = inUid;
}

void User::Clear()
{
	mName.clear();
	mOid = 0;
	mUid = -1;
	mStatus = JobState::NOT_SELETE;
	mLocation = LocationState::LOBBY;
	mRoomNumber = -1;
}
