#pragma once

const int MaxRoomCount = 200;


class RoomManager : public Singleton<RoomManager>
{
	typedef std::pair<bool, User *> EntranceNode;
public:
	RoomManager();

	virtual ~RoomManager() { Release(); }

	void Initialize();

	void Release();

	void Reservation(User * inUser, bool inEnter = true);

	void Execute();

	void UserReadyExecute(const INT64 inUid, const INT32 & inRoomNumber, const bool inReady = true);

	void SendRoomServerAddress(const INT64 & inRoomNumber);

	void EndGameTodoclean(const INT64 & inRoomNumber);

private:
	std::array<Room *, MaxRoomCount>			mRoomAry;
	std::array<KillerWaitings, MaxRoomCount>	mkillerScore;
	std::array<SurvivorWaitings, MaxRoomCount>	mSurvivorScore;
	std::queue<EntranceNode>					mEntranceQue;

	void Enter(User * inUser);
	void Leave(User * inUser);
};


class AutoIoSupervise : public Work
{
public:
	AutoIoSupervise();

	void Tick()
	{
		RoomManager::getInstance().Execute();
	}
};

static AutoIoSupervise autoiosupervisor;