#include"stdafx.h"
#include"RoomRater.h"

RoomRater::RoomRater()
	: mTotalSurvivorScore(100), mTotalKillerScore(100)
{}

RoomRater::RoomRater(const RoomRater & instance)
{
	mTotalKillerScore = instance.mTotalKillerScore;
	mTotalSurvivorScore = instance.mTotalSurvivorScore;
}

RoomRater::RoomRater(RoomRater && instance)
	: mTotalKillerScore(std::move(instance.mTotalKillerScore)), mTotalSurvivorScore(std::move(instance.mTotalSurvivorScore))
{}

RoomRater::~RoomRater() {}

/**
@return : void

@param1 : 방의 생존자 인원

@param2 : 방의 살인자 인원

@brief : 표준점수를 생성후 방의 특성에 맞게 생존자 유입의  평가점수와 살인자 유입의 평가점수를 생성한다

@warning : 이식후 테스팅 필요.
*/
void RoomRater::Update(INT32 inSurvivorCnt, INT32 inKillerCnt)
{
	mTotalSurvivorScore = 100;
	mTotalKillerScore = 100;
	EvaluateKiller(inSurvivorCnt, inKillerCnt);
	EvaluateSurvivors(inSurvivorCnt, inKillerCnt);
}

/**
@return : void

@param1 : 방의 생존자 인원

@param2 : 방의 살인자 인원

@brief : 살인자 유입기준의 평가점수를 매긴다

@warning : Not Notify
*/
void RoomRater::EvaluateKiller(const INT32 & inSurvivorCnt, const INT32 & inKillerCnt)
{
	if (inKillerCnt == 1)
	{
		mTotalKillerScore = 0;
		return;
	}
	switch (inSurvivorCnt)
	{
	case 0:
		mTotalKillerScore -= 90;
		break;
	case 1:
		mTotalKillerScore -= 70;
		break;
	case 2:
		mTotalKillerScore -= 40;
		break;
	case 3:
		break;
	default:
		break;
	}
}

/**
@return : void

@param1 : 방의 생존자 인원

@param2 : 방의 살인자 인원

@brief : 생존자 유입기준의 평가점수를 매긴다

@warning : Not Notify
*/
void RoomRater::EvaluateSurvivors(const INT32 & inSurvivorCnt, const INT32 & inKillerCnt)
{
	if (inKillerCnt == 0)
	{
		mTotalSurvivorScore -= 60;
	}
	switch (inSurvivorCnt)
	{
	case 1:
		mTotalSurvivorScore -= 20;
		break;
	case 2:
		break;
	case 0:
		mTotalSurvivorScore -= 30;
		break;
	case 3:
		mTotalSurvivorScore = 0;
		break;
	default:
		break;
	}
}
