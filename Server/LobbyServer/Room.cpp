#include"stdafx.h"
#include"ServerHeader.h"
#include"User.h"
#include"Room.h"


/**
@return : true = Room Insert Success, false = Fail

@param1 : User Instance Pointer

@brief :  InsertAvailable() 메소드로 방에 진입 가능 체크 및 처리 후 결과를 리턴

@warning : NOt Notify
*/
bool Room::Insert(User * inUser)
{
	if (InsertAvaliable(inUser))
	{
		mUserList.push_back(inUser);
		UpdateRatingScore();

		PK_S_ANS_ENTERROOMSUCC resultPacket;
		resultPacket.roomNumber = mRoomNumber;
		auto count = (BYTE)mUserList.size();
		for (auto & iter : mUserList)
		{
			resultPacket.uid.push_back(iter->GetUid());
			resultPacket.id.push_back(iter->GetName());
			resultPacket.role.push_back((BYTE)iter->GetJobStatus());
		}

		// 진입하는 유저에게 방에 대한 데이터 송신
		Session * session = SessionManager::getInstance().GetSession(inUser->GetOid());
		session->SendPacket(&resultPacket);


		PK_S_BRD_ROOMSTATE broadPacket;
		broadPacket.uid = inUser->GetUid();
		broadPacket.update = (inUser->GetJobStatus() == JobState::KILLER) ? 10 : 11;
		broadPacket.id.assign(inUser->GetName());
		// 기존 방에 있던 유저들에게 신규 유저 데이터 송신
		for (auto iter : mUserList) {
			if (inUser->GetOid() == iter->GetOid()) continue;
			session = SessionManager::getInstance().GetSession(iter->GetOid());
			session->SendPacket(&broadPacket);
		}
		return true;
	}
	return false;
}

/**
@return : void

@param1 : User oid

@brief : 방안의 유저들 중 Oid를 찾아 제거

@warning :
*/
void Room::Erase(const INT64 & inOid)
{
	if (size() != 0)
	{
		INT64 uid = -1;
		for (auto iter = mUserList.begin(); iter != mUserList.end();)
		{
			if ((*iter)->GetOid() == inOid)
			{
				uid = (*iter)->GetUid();
				mUserList.erase(iter);
				break;
			}
			++iter;
		}
		UpdateRatingScore();

		PK_S_ANS_OUTROOMSUCC resultPacket;
		PK_S_BRD_ROOMSTATE broadPacket;

		broadPacket.uid = uid;
		broadPacket.update = 2;
		resultPacket.uid = broadPacket.uid;
		Session * session = SessionManager::getInstance().GetSession(inOid);
		session->SendPacket(&resultPacket);
		// 방의 변경 정보를 방에 있는 유저들에세 송신
		for (auto iter : mUserList) {
			session = SessionManager::getInstance().GetSession(iter->GetOid());
			session->SendPacket(&broadPacket);
		}
	}
}

/**
@return : void

@brief : 방 안의 역할별 인원을 카운팅

@warning :
*/
void Room::UpdateRatingScore()
{
	int killercount = 0;
	int survivorcount = 0;
	for (const auto & iter : mUserList)
	{
		if (iter->GetJobStatus() == 1)
		{
			++killercount;
			continue;
		}
		else if (iter->GetJobStatus() == 2)
		{
			++survivorcount;
			continue;
		}
	}
	mRating.Update(survivorcount, killercount);
}

std::pair<int, int> Room::JobCount()
{
	int killercount = 0;
	int survivorcount = 0;
	for (const auto & iter : mUserList)
	{
		if (iter->GetJobStatus() == 1)
		{
			++killercount;
			continue;
		}
		else if (iter->GetJobStatus() == 2)
		{
			++survivorcount;
			continue;
		}
	}
	return std::make_pair(killercount, survivorcount);
}

void Room::PlayerStateUpdate(INT64 inUid, bool inReady)
{
	auto iter = std::find_if(mUserList.begin(), mUserList.end(), [inUid](User * ptr) {
		return (inUid == ptr->GetUid()); });
	if (iter != mUserList.end())
	{
		if (inReady)
			(*iter)->Ready();
		else
			(*iter)->UnReady();
	}
	if (inReady)
	{
		PK_S_BRD_ROOMSTATE sPacket;
		int killerCount = 0;
		int survivorCount = 0;
		int ReadyCount = 0;
		sPacket.update = 3;
		sPacket.uid = inUid;
		for (auto iter : mUserList)
		{
			if (iter->IsReady())
				ReadyCount++;
			auto job = iter->GetJobStatus();
			if (job == JobState::KILLER)
				killerCount += 1;
			else if (job == JobState::SIRVIVOR)
				survivorCount += 1;

			Session * sessionPtr = SessionManager::getInstance().GetSession(iter->GetOid());
			sessionPtr->SendPacket(&sPacket);
		}

		if (mUserList.size() > 1 && inReady)
		{
			if ((killerCount > 0) && (survivorCount > 0) && (mUserList.size() == ReadyCount))
			{
				mIsStart = true;
				PK_T_NTF_STARTUSERDATA Tpacket;
				Tpacket.roomNumber = mRoomNumber;
				for (auto iter : mUserList)
				{
					Tpacket.uid.emplace_back(iter->GetUid());
					Tpacket.id.emplace_back(iter->GetName());
					Tpacket.role.emplace_back(iter->GetJobStatus());
				}
				TerminalManager::getInstance().terminal(L"RoomServer")->SendPacket(&Tpacket);
			}
		}

	}
	else
	{
		PK_S_BRD_ROOMSTATE sPacket;
		sPacket.update = 4;
		sPacket.uid = inUid;
		for (auto iter : mUserList)
		{
			Session * sessionPtr = SessionManager::getInstance().GetSession(iter->GetOid());
			sessionPtr->SendPacket(&sPacket);
		}
	}


}

void Room::SendAddrass()
{
	PK_S_BRD_GAMESTARTSUCC broadPacket;

	auto pterminal = TerminalManager::getInstance().terminal(L"RoomServer");
	broadPacket.IP = pterminal->Ip();
	broadPacket.PORT = pterminal->Port();
	Session * pSession = nullptr;
	for (auto iter : mUserList) {
		pSession = SessionManager::getInstance().GetSession(iter->GetOid());
		pSession->SendPacket(&broadPacket);
	}
}

void Room::EndGame()
{
	for (auto & iter : mUserList)
	{
		RoomManager::getInstance().Reservation(iter, false);
	}
	mIsStart = false;
}

/**
@return : true = Insert 가능, false = Insert 불가능

@param1 : User Pointer

@brief : 진입하려는 방을 기준으로 진입의 가능여부를 확인하여 반환한다.

@warning :
*/
bool Room::InsertAvaliable(User * inUser)
{
	if (mUserList.size() > 3) return false;
	int job = inUser->GetJobStatus();

	int killercount = 0;
	int survivorcount = 0;

	for (const auto & iter : mUserList)
	{
		if (iter->GetJobStatus() == 1)
		{
			++killercount;
			continue;
		}
		else if (iter->GetJobStatus() == 2)
		{
			++survivorcount;
			continue;
		}
	}
	if (job == 1 && killercount == 0)
	{
		return true;
	}
	else if (job == 2 && survivorcount < 3)
	{
		return true;
	}
	return false;
}



