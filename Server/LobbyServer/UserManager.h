#pragma once
#include"stdafx.h"

class User;
class UserManager : public  Singleton<UserManager>
{
public:
	UserManager();

	virtual ~UserManager();

	INT64 login(UINT64 & inOID, wstr_t & inStringID);

	void logout(INT64 inUID);

	User * getUser(INT64  inUID);
private:
	std::array<User *, 2000> mUserPool;
	std::set<INT64> mLoginIndex;
	std::set<INT64> mLogoutIndex;
	Lock mLock;
};