#pragma once
#include"stdafx.h"

typedef enum
{
	NOT_SELETE,
	KILLER,
	SIRVIVOR,
}JobState;

typedef enum
{
	LOBBY,
	ROOM,
	INGAME
}LocationState;

class User
{
public:
	User() : mOid(0), mUid(-1), mStatus(JobState::NOT_SELETE), mLocation(LocationState::LOBBY) {}
	User(UINT64 inOid, INT64 inUid) : mOid(inOid), mUid(inUid), mStatus(JobState::NOT_SELETE) {}
	~User() {}

	void SetJobStatus(int inJob) { mStatus = (JobState)inJob; }

	int GetJobStatus() { return (int)mStatus; }

	void EnterRoom(const int16_t inCnt) { mRoomNumber = inCnt; mLocation = LocationState::ROOM; }

	void LeaveRoom() { mRoomNumber = -1; }

	int16_t GetRoomNumber();

	UINT64 GetOid() { return mOid; }

	INT64 GetUid() { return mUid; }

	void SetData(const UINT64 & inOid, const INT64 & inUid, const wstring & inName);

	void Clear();

	void Ready() { mIsReady = true; }
	void UnReady() { mIsReady = false; }

	bool IsReady() { return mIsReady; }

	wstring & GetName() { return mName; }
private:
	wstring			mName;
	UINT64			mOid;
	INT64			mUid;
	JobState		mStatus;
	LocationState	mLocation;
	int16_t			mRoomNumber = -1;
	bool			mIsReady = false;
};
