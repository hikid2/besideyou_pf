#include"stdafx.h"
#include"User.h"
#include"LobbyProcess.h"

class Query_Login : public Query
{
public:
	Query_Login() {
		_statement->setQuery(this->procedure(), QUERY_WAIT_RETURN);
	}
	virtual ~Query_Login() {
		int retval = 100;
		_recode.get(L"result", &retval);
		Session * session = SessionManager::getInstance().GetSession(mOid);
		if (retval == 0) {
			// Packet create
			PK_S_ANS_LOGINSUCC tempPacket;
			// user uid get... userManager으로 User 설정 완료.
			tempPacket.uid = UserManager::getInstance().login(mOid, mStringId);
			session->Setuid(tempPacket.uid);
			SLog(L"** User Login    id : [ %s ]  oid : [%I64u]  , uid : [%I64d]", mStringId.c_str(), mOid, mUid);
			// TODO : SendPacket을 해줘야 한다. -> Contents로? 아니면 여기서? 
			// 어떻게 함? 어차피 Send가 논블로킹이라서 return value 바로 나온다.
			session->SendPacket(&tempPacket);
		}
		else {
			PK_S_ANS_LOGINFAIL tempPacket;
			tempPacket.errcode = retval;
			session->SendPacket(&tempPacket);
		}
	}
	WCHAR * procedure()
	{
		return L"dbo.p_USERLOGIN";
	}

public:
	uint64_t mOid;
	int64_t mUid;
	wstr_t mStringId;
};

class Query_Logout : public Query
{
public:
	
	Query_Logout() {
		_statement->setQuery(this->procedure(), QUERY_NOT_RETURN);
	}
	virtual ~Query_Logout() {}
	WCHAR * procedure()
	{
		return L"dbo.p_USERLOGOUT";
	}
	wstring mStringId;
};

class Query_Join : public Query
{
public:
	Query_Join() {
		_statement->setQuery(this->procedure(), QUERY_WAIT_RETURN);
	}
	virtual ~Query_Join() {
		int retval = 100;
		_recode.get(L"result", (int*)&retval);
		Session * session = SessionManager::getInstance().GetSession(mOid);
		if (retval == 100) {
			PK_S_ANS_JOINSUCC tempPacket;
			session->SendPacket(&tempPacket);
		}
		else {
			PK_S_ANS_JOINFAIL tempPacket;
			tempPacket.errcode = retval;
			session->SendPacket(&tempPacket);
		}

	}
	WCHAR * procedure()
	{
		return L"dbo.p_USERLOGIN_INSERT";
	}
public:
	uint64_t mOid;
};

LobbyProcess::LobbyProcess()
{
	this->RegistSubContents();
}

/**
	@return : void

	@brief : 상속된 클래스의 Contents를 PacketType과 쌍을 이루어 unordered_map에 저장.

	@warning : NO
*/
void LobbyProcess::RegistSubContents()
{
	mContentsMethodTable.insert(make_pair(PE_C_REQ_LOGIN, &LobbyProcess::CPacket_Login));
	mContentsMethodTable.insert(make_pair(PE_C_REQ_JOIN, &LobbyProcess::CPacket_Join));
	mContentsMethodTable.insert(make_pair(PE_C_REQ_SELECTPART, &LobbyProcess::CPacket_SelectPart));
	mContentsMethodTable.insert(make_pair(PE_C_REQ_SEARCHROOM, &LobbyProcess::CPacket_SearchRoom));
	mContentsMethodTable.insert(make_pair(PE_C_REQ_GAMEREADYON, &LobbyProcess::CPacket_GameReadyOn));
	mContentsMethodTable.insert(make_pair(PE_C_REQ_GAMEREADYOFF, &LobbyProcess::CPacket_GameReadyOff));
	mContentsMethodTable.insert(make_pair(PE_C_REQ_EXITROOM, &LobbyProcess::CPacket_ExitRoom));
	mContentsMethodTable.insert(make_pair(PE_C_REQ_GAMEOUT, &LobbyProcess::CPacket_GameOut));
	mContentsMethodTable.insert(make_pair(PE_T_NTY_USERBUILDSUCC, &LobbyProcess::TPacket_BuildSuccess));
	mContentsMethodTable.insert(make_pair(PE_T_NTY_GAMECLEAR, &LobbyProcess::TPacket_EndGameClear));
}

/**
	@return : void

	@param 1 : SessionPtr

	@param 2 : 전달하려는 Packet

	@brief : 로그인 요청에 따른 작업

	@warning : No
*/
void LobbyProcess::CPacket_Login(Session *inSession, Packet * inRowPacket)
{
	PK_C_REQ_LOGIN * recvPacketPtr = (PK_C_REQ_LOGIN *)inRowPacket;
#if NONEDB
#ifndef Test

	PK_S_ANS_LOGINSUCC resultPacket;

	auto oid = inSession->GetId();

	resultPacket.uid = UserManager::getInstance().login(oid, recvPacketPtr->id);

	inSession->Setuid(resultPacket.uid);

	SLog(L"** User Login   - id : [ %s ]  oid : [%I64u]  , uid : [%I64d]", recvPacketPtr->id.c_str(), oid, resultPacket.uid);

	inSession->SendPacket(&resultPacket);
#else
	PK_S_ANS_LOGINSUCC resultPacket;
	// user uid get... userManager으로 User 설정 완료.
	auto oid = session->getId();
	resultPacket.uid = UserManager::getInstance().login(oid, packet->id);
	session->setuid(resultPacket.uid);
	SLog(L"** User Login   - id : [ %s ]  oid : [%I64u]  , uid : [%I64d]", packet->id.c_str(), oid, resultPacket.uid);
	// TODO : SendPacket을 해줘야 한다. -> Contents로? 아니면 여기서? 
	// 어떻게 함? 어차피 Send가 논블로킹이라서 return value 바로 나온다.
	session->sendPacket(&resultPacket);
#endif // !Test



#else
	Query_Login * query = new Query_Login();
	query->_oid = session->getId();
	// 유저 아이디 User등록을 위해 지정......
	query->_id.assign(packet->id.begin(), packet->id.end());

	QueryStatement *statement = query->statement();
	statement->addParam((WCHAR *)packet->id.c_str());
	statement->addParam((WCHAR *)packet->pw.c_str());
	DBManager::getInstance().pushQuery(query);
#endif
}

/**
	@return : void

	@param 1 : SessionPtr

	@param 2 : 전달하려는 Packet

	@brief : 회원가입 요청

	@warning : NO
*/
void LobbyProcess::CPacket_Join(Session *inSession, Packet * inRowPacket)
{
	PK_C_REQ_JOIN * recvPacketPtr = (PK_C_REQ_JOIN *)inRowPacket;
	Query_Join * queryPtr = new Query_Join();
	queryPtr->mOid = inSession->GetId();

	QueryStatement * statement = queryPtr->statement();
	statement->addParam((WCHAR *)recvPacketPtr->id.c_str());
	statement->addParam((WCHAR *)recvPacketPtr->pw.c_str());
	statement->addParam((WCHAR *)recvPacketPtr->name.c_str());
	statement->addParam((WCHAR *)recvPacketPtr->email.c_str());
	DBManager::getInstance().pushQuery(queryPtr);
}

/**
	@return : void

	@param 1 : SessionPtr

	@param 2 : 전달하려는 Packet

	@brief : 역할 설정

	@warning : NO
*/
void LobbyProcess::CPacket_SelectPart(Session *inSession, Packet * inRowPacket)
{
	PK_C_REQ_SELECTPART * recvPacketPtr = (PK_C_REQ_SELECTPART *)inRowPacket;
	User * pUser = UserManager::getInstance().getUser(recvPacketPtr->uid);
	pUser->SetJobStatus((JobState)recvPacketPtr->role);

	SLog(L" -- [Uid : %d ] , [SelectPart : %d ] --", pUser->GetUid(), pUser->GetJobStatus());
	PK_S_ANS_SELECTPARTSUCC resultPacket;

	resultPacket.role = recvPacketPtr->role;

	inSession->SendPacket(&resultPacket);
}



/**
@return : void

@param 1 : SessionPtr

@param 2 : 전달하려는 Packet

@brief : 역할에 따라 우선순위로 방을 찾는다.

@warning : NO
*/
void LobbyProcess::CPacket_SearchRoom(Session *inSession, Packet * inRowPacket)
{
	PK_C_REQ_SEARCHROOM * recvPacketPtr = (PK_C_REQ_SEARCHROOM *)inRowPacket;
	User * pUser = UserManager::getInstance().getUser(recvPacketPtr->uid);
	if (pUser->GetJobStatus() == NOT_SELETE) {

		PK_S_NTF_ROOMSEARCHNOTROLE resultPacket;
		inSession->SendPacket(&resultPacket);
		return;
	}

	RoomManager::getInstance().Reservation(pUser);
}


/**
@return : void

@param 1 : SessionPtr

@param 2 : 전달하려는 Packet

@brief : 준비 상태로 변경을 시켜준 뒤 다른 유저들에게 상태 변경을 송신.

@warning : NO
*/
void LobbyProcess::CPacket_GameReadyOn(Session *inSession, Packet * inRowPacket)
{
	PK_C_REQ_GAMEREADYON * recvPacketPtr = (PK_C_REQ_GAMEREADYON *)inRowPacket;
	User * user = UserManager::getInstance().getUser(recvPacketPtr->uid);
	//vector <User *> * userinfo = RoomManager::getInstance().findRoom(user->roomNumber())->UserInfo();

	RoomManager::getInstance().UserReadyExecute(recvPacketPtr->uid, user->GetRoomNumber());



	/*if ((*userinfo).size() == 1)return;
	user->ready();*/


	//PK_S_BRD_ROOMSTATE resultPacket;
	//resultPacket.update = 3;
	//resultPacket.uid = packet->uid;
	//Session * pSession = nullptr;
	//// 유저를 찾아 Ready 상태로 변경

	//bool IsStart = true;
	//for (auto i : *userinfo) {
	//	pSession = SessionManager::getInstance().session(i->oid());
	//	pSession->sendPacket(&resultPacket);
	//	if (i->state() != STATE_ROOMREADY) IsStart = false;
	//}

	// 모든 유저가 다 준비 상태가 되었을 경우 룸 서버로 가게끔 유도.
#if SHOW
	/*if (IsStart)
	{
	PK_T_NTF_STARTUSERDATA Tpacket;
	Tpacket.roomNumber = user->roomNumber();
	for (auto iter = 0; iter < 2; ++iter) {
	Tpacket.uid.emplace_back(((*userinfo)[iter])->uid());
	Tpacket.id.emplace_back(((*userinfo)[iter])->id());
	Tpacket.role.emplace_back(((*userinfo)[iter])->role());
	}
	TerminalManager::getInstance().terminal(L"RoomServer")->sendPacket(&Tpacket);*/

	/*PK_S_BRD_GAMESTARTSUCC Ppacket;
	auto pterminal = TerminalManager::getInstance().terminal(L"RoomServer");
	Ppacket.IP = pterminal->ip();
	Ppacket.PORT = pterminal->port();
	for (auto i : *userinfo) {
	pSession = SessionManager::getInstance().session(i->oid());
	pSession->sendPacket(&Ppacket);
	}
	}*/
	return;
#endif
}


/**
@return : void

@param 1 : SessionPtr

@param 2 : 전달하려는 Packet

@brief : 준비해제 상태로 변경을 시켜준 뒤 다른 유저들에게 상태 변경을 송신.

@warning : NO
*/
void LobbyProcess::CPacket_GameReadyOff(Session *inSession, Packet * inRowPacket)
{
	PK_C_REQ_GAMEREADYOFF * recvPacketPtr = (PK_C_REQ_GAMEREADYOFF *)inRowPacket;
	User * user = UserManager::getInstance().getUser(recvPacketPtr->uid);

	RoomManager::getInstance().UserReadyExecute(recvPacketPtr->uid, user->GetRoomNumber(), false);
	//user->unready();
	//PK_S_BRD_ROOMSTATE resultPacket;
	//resultPacket.update = 4;
	//resultPacket.uid = packet->uid;
	//Session * pSession = nullptr;
	//// 유저를 찾아 UnReady 상태로 변경
	//vector <User *> * userinfo = RoomManager::getInstance().findRoom(user->roomNumber())->UserInfo();
	//for (auto i : *userinfo) {
	//	pSession = SessionManager::getInstance().session(i->oid());
	//	pSession->sendPacket(&resultPacket);
	//}
}

/**
@return : void

@param 1 : SessionPtr

@param 2 : 전달하려는 Packet

@brief : 나가기 데이터를 입력하여 RoomManager에게 전달 해야 한다.(완료)
         실제 방을 나가는 작업은 RoomManager에서(ContentsThread의 단일 스레드로 Lock 없게)

@warning : NO
*/
void LobbyProcess::CPacket_ExitRoom(Session *inSession, Packet * inRowPacket)
{
	PK_C_REQ_EXITROOM * recvPacketPtr = (PK_C_REQ_EXITROOM *)inRowPacket;

	User * pUser = UserManager::getInstance().getUser(recvPacketPtr->uid);

	RoomManager::getInstance().Reservation(pUser, false);
}

/**
@return : void

@param 1 : SessionPtr

@param 2 : 전달하려는 Packet

@brief : 게임 나가기

@warning : 유저의 게임종료로 지정되 있던 데이터들을 기본값(default)으로 정정필요
		   DB에 로그오프 시간 작성.
*/
void LobbyProcess::CPacket_GameOut(Session *inSession, Packet * inRowPacket)
{

	Query_Logout * query = new Query_Logout();
	PK_C_REQ_GAMEOUT * recvPacketPtr = (PK_C_REQ_GAMEOUT *)inRowPacket;
	User * pUser = UserManager::getInstance().getUser(recvPacketPtr->uid);
	query->mStringId.assign(pUser->GetName());

	DBManager::getInstance().pushQuery(query);									// DB 작성 요청

	UserManager::getInstance().logout(recvPacketPtr->uid);								// User 로그오프 작업


	SessionManager::getInstance().CloseSession(inSession);						// Session 닫기(소켓닫고 기타 등등등)
}

/**
@return : void

@param 1 : SessionPtr

@param 2 : 전달하려는 Packet

@brief : 룸 서버의 게임준비 상태가 완료됬을때 각 클라이언트들에게 RoomServer의 주소와 Port번호를 전달하는 기능

@warning : NO
*/
void LobbyProcess::TPacket_BuildSuccess(Session *inSession, Packet * inRowPacket)
{
	PK_T_NTY_USERBUILDSUCC * recvPacketPtr = (PK_T_NTY_USERBUILDSUCC*)inRowPacket;

	RoomManager::getInstance().SendRoomServerAddress(recvPacketPtr->roomNumber);

	/*PK_S_BRD_GAMESTARTSUCC Cpacket;

	vector <User *> * userinfo = RoomManager::getInstance().findRoom(packet->roomNumber)->UserInfo();


	auto pterminal = TerminalManager::getInstance().terminal(L"RoomServer");
	Cpacket.IP = pterminal->ip();
	Cpacket.PORT = pterminal->port();
	Session * pSession = nullptr;
	for (auto i : *userinfo) {
	pSession = SessionManager::getInstance().session(i->oid());
	pSession->sendPacket(&Cpacket);
	}*/
}

/**
@return : void

@param 1 : SessionPtr

@param 2 : 전달하려는 Packet

@brief : 종료된 게임의 상태를 Clear함

@warning : NO
*/
void LobbyProcess::TPacket_EndGameClear(Session *inSession, Packet * inRowPacket)
{
	PK_T_NTY_GAMECLEAR * recvPacketPtr = (PK_T_NTY_GAMECLEAR*)inRowPacket;
	RoomManager::getInstance().EndGameTodoclean(recvPacketPtr->roomNumber);
}

