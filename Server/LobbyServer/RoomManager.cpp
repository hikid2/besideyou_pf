#include"stdafx.h"
#include"RoomManager.h"

RoomManager::RoomManager()
{
	Initialize();
}

/**
@return : void

@brief : RoomManager에서 필요한 생성작업을 진행(New Room())

@warning :
*/
void RoomManager::Initialize()
{
	for (int num = 0; num < mRoomAry.size(); ++num)
	{
		mRoomAry[num] = new Room(num);
		mRoomAry[num]->UpdateRatingScore();
		mkillerScore[num] = make_pair(num, mRoomAry[num]->GetKillerScore());
		mSurvivorScore[num] = make_pair(num, mRoomAry[num]->GetSurvivorScore());
	}

}

/**
@return : void

@brief : 할당한 객체메모리를 반환하는 작업을 진행

@warning :
*/
void RoomManager::Release()
{
	for (auto & iter : mRoomAry)
	{
		delete iter;
	}
}

/**
@return : void

@param1 : User Porinter

@param2 : true = Enter, false = Leave

@brief : 방의 출입예약을 하는 함수

@warning :
*/
void RoomManager::Reservation(User * inUser, bool inEnter)
{
	int job = inUser->GetJobStatus();
	EntranceNode tempNode = make_pair(inEnter, inUser);
	mEntranceQue.push(tempNode);
}

/**
@return : void

@brief : 출입예약에 대한 작업을 진행한다.

@warning : 한번 작업을 할 때 50개씩 진행한다.
*/
void RoomManager::Execute()
{
	int RunCount = 50;
	try
	{
		if (mEntranceQue.size() == 0) throw 0;
		while (RunCount > 0)
		{
			EntranceNode tempNode = mEntranceQue.front();
			if (tempNode.first)
			{
				Enter(tempNode.second);
			}
			else
			{
				Leave(tempNode.second);
			}
			std::sort(mkillerScore.begin(), mkillerScore.end(),
				[](const KillerWaitings & left, const KillerWaitings & right) {
				return (left.second > right.second);
			});

			std::sort(mSurvivorScore.begin(), mSurvivorScore.end(),
				[](const SurvivorWaitings & left, const SurvivorWaitings & right) {
				return (left.second > right.second);

			});
			mEntranceQue.pop();
			if (mEntranceQue.empty())throw 0;
			--RunCount;
		}
	}
	catch (int exp)
	{
		if (exp == 1)
			printf("Entrance is zero.....\n");
	}
}

void RoomManager::UserReadyExecute(const INT64 inUid, const INT32 & inRoomNumber, const bool inReady)
{
	mRoomAry[inRoomNumber]->PlayerStateUpdate(inUid, inReady);
}

void RoomManager::SendRoomServerAddress(const INT64 & inRoomNumber)
{
	mRoomAry[inRoomNumber]->SendAddrass();
}

void RoomManager::EndGameTodoclean(const INT64 & inRoomNumber)
{
	mRoomAry[inRoomNumber]->EndGame();
}

/**
@return : void

@param1 : User Pointer

@brief : 출입예약중 입장에 대한 작업을 진행

@warning :
*/
void RoomManager::Enter(User * inUser)
{
	if (inUser->GetJobStatus() == 1)
	{
		for (auto index = 0; index < MaxRoomCount; ++index)
		{
			int roomindex = mkillerScore[index].first;
			if (mRoomAry[roomindex]->IsStart())
				continue;
			if (mRoomAry[roomindex]->Insert(inUser)) {
				inUser->EnterRoom(roomindex);
				mkillerScore[index].second = mRoomAry[roomindex]->GetKillerScore();
				auto retval = std::find_if(mSurvivorScore.begin(), mSurvivorScore.end(), [roomindex](const SurvivorWaitings & iter)->bool {
					return iter.first == roomindex;
				});
				if (retval != mSurvivorScore.end())
				{
					retval->second = mRoomAry[roomindex]->GetSurvivorScore();

				}

				break;
			}
		}
	}
	else if (inUser->GetJobStatus() == 2)
	{
		for (auto index = 0; index < MaxRoomCount; ++index)
		{
			int roomindex = mSurvivorScore[index].first;
			if (mRoomAry[roomindex]->IsStart())
				continue;
			if (mRoomAry[roomindex]->Insert(inUser)) {
				inUser->EnterRoom(roomindex);
				mSurvivorScore[index].second = mRoomAry[roomindex]->GetSurvivorScore();

				auto retval = std::find_if(mkillerScore.begin(), mkillerScore.end(), [roomindex](const KillerWaitings & iter)->bool {
					return iter.first == roomindex;
				});
				if (retval != mkillerScore.end())
				{
					retval->second = mRoomAry[roomindex]->GetKillerScore();
				}

				break;
			}
		}
	}
}

/**
@return : void

@param1 : User Pointer

@brief : 출입예약중 탈출에 대한 작업을 진행

@warning :
*/
void RoomManager::Leave(User * inUser)
{
	auto roomNumber = inUser->GetRoomNumber();
	try
	{
		if (roomNumber == -1) throw 1;
		mRoomAry[roomNumber]->Erase(inUser->GetOid());
		inUser->LeaveRoom();
		auto retval = std::find_if(mkillerScore.begin(), mkillerScore.end(), [roomNumber](const KillerWaitings & iter)->bool {
			return iter.first == roomNumber;
		});
		if (retval == mkillerScore.end())
		{
			throw 1;
		}
		else
		{
			retval->second = mRoomAry[roomNumber]->GetKillerScore();
		}
		auto retval2 = std::find_if(mSurvivorScore.begin(), mSurvivorScore.end(), [roomNumber](const SurvivorWaitings & iter)->bool {
			return iter.first == roomNumber;
		});
		if (retval2 == mSurvivorScore.end())
		{
			throw 1;
		}
		else
		{
			retval2->second = mRoomAry[roomNumber]->GetSurvivorScore();

		}
	}
	catch (const int exp)
	{

	}

}

/**
@brief : 1초에 1번씩 방의 출입 기능을 Task에 등록함.

@warning :
*/
AutoIoSupervise::AutoIoSupervise()
{
	const int AUTO_IO_SUPERVISE = 1000;
	TaskNode * node = new TaskNode(this, AUTO_IO_SUPERVISE, TICK_INFINTY);
	TaskManager::getInstance().Add(node);
}