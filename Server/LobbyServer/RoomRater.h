#pragma once

typedef std::pair<int, int> Waitings;
typedef std::pair<int, int> KillerWaitings;
typedef std::pair<int, int> SurvivorWaitings;


class RoomRater
{
public:
	RoomRater();

	RoomRater(const RoomRater & instance);

	RoomRater(RoomRater && instance);

	~RoomRater();

	void Update(INT32 inSurvivorCnt, INT32 inKillerCnt);

	const int & GetKillerScore() const { return mTotalKillerScore; }

	const int & GetSurvivorScore() const { return mTotalSurvivorScore; }

private:
	int mTotalSurvivorScore = 100;

	int mTotalKillerScore = 100;

	void EvaluateKiller(const INT32 & inSurvivorCnt, const INT32 & inKillerCnt);

	void EvaluateSurvivors(const INT32 & inSurvivorCnt, const INT32 & inKillerCnt);

};