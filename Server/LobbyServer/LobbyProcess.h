#pragma once
#include"stdafx.h"

class LobbyProcess : public ContentsProcess
{
public:
	LobbyProcess();
	virtual void RegistSubContents();
	
private:
	static void CPacket_Login(Session *inSession, Packet * inRowPacket);

	static void CPacket_Join(Session *inSession, Packet *inRowPacket);

	static void CPacket_SelectPart(Session * inSession, Packet * inRowPacket);

	static void CPacket_SearchRoom(Session *inSession, Packet * inRowPacket);

	static void CPacket_GameReadyOn(Session * inSession, Packet * inRowPacket);

	static void CPacket_GameReadyOff(Session * inSession, Packet * inRowPacket);

	static void CPacket_ExitRoom(Session *inSession, Packet *inRowPacket);

	static void CPacket_GameOut(Session *inSession, Packet *inRowPacket);

	static void TPacket_BuildSuccess(Session *inSsession, Packet *inRowPacket);

	static void TPacket_EndGameClear(Session *inSsession, Packet *inRowPacket); 
};
