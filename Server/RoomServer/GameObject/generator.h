#pragma once
#include"stdafx.h"

typedef enum
{
	GENERATIOR_STOP, // 발전기 정지
	GENERATIOR_START, // 발전기 가동
	GENERATIOR_SUSPEND, // 발전기 일시 정지
	GENERATIOR_END, // 발전기 가동 완료
}GEN_STATE;

class Generator : public GameObject
{

public:
	bool IsComplete();

	Generator();

	Generator(int index);

	virtual ~Generator();

	INT32 GetGenNumber();

	static Generator * instance(int index) { return new Generator(index); }

	void Initalize();					// 초기화

	void Tick();						// 일정 시간이 되면 해야 하는 작업

	void wakeup();						// 발전기를 가동 시켰을 시

	void suspend();

	void stop();						// STOP 시 아예 Clear()함수를 호출 하여 초기 상태로 되돌리자

	void start();						// Start()함수가 시간 값을 지정하고 

	void Write(Packet *& inPacket);

	int32_t Percent();

private:
	INT32		mGeneratorNumber;
	UINT64		mStartTime;		// 발전기가 가동이 시작된 시간
	INT64		mDurationTime;	// 
	UINT64		mNowTime;		// 현재 시간
	GEN_STATE	mGenState;	// 발전기 상태
};
class GeneratorManager : public Singleton<GeneratorManager>
{
public:

	unordered_map<int, Generator *> mIndexNumber;
	unordered_map<Generator*, int> mPointNumber;
public:
	GeneratorManager(){}
	virtual ~GeneratorManager() {};
	int GetGenIndex(Generator * pointer) {
		auto result = mPointNumber.find(pointer);
		if (result == mPointNumber.end())return -1;
		else
			return result->second;
	}
	Generator * GetGenPointer(int index) {
		auto result = mIndexNumber.find(index);
		if (result == mIndexNumber.end())return nullptr;
		else
			return result->second;
	}
	void AddGenerator(Generator * gen, int index)
	{
		mIndexNumber[index] = gen;
		mPointNumber[gen] = index;
	}
	void RemoveGenerator(int index)
	{
		Generator * ptr = mIndexNumber[index];
		mPointNumber.erase(ptr);
		mIndexNumber.erase(index);
		SAFE_DELETE(ptr);
	}
	void RemoveGenerator(Generator * index)
	{
		int ptr = mPointNumber[index];
		mIndexNumber.erase(ptr);
		mPointNumber.erase(index);
		SAFE_DELETE(index);
	}

};