#include"stdafx.h"
#include"generator.h"

bool Generator::IsComplete() {
	if (mGenState == GENERATIOR_END)
		return true;
	else
		return false;
}

Generator::Generator()
{
	mGeneratorNumber = 0;
	this->Initalize();
}

Generator::Generator(int index)
{
	mGeneratorNumber = index;
	this->Initalize();
}

Generator::~Generator()
{
}

INT32 Generator::GetGenNumber() { return mGeneratorNumber; }

void Generator::Initalize()
{
	mGenState = GENERATIOR_STOP;
	mStartTime = 0;
	mDurationTime = 0;
	mNowTime = 0;
}

void Generator::Tick()
{
	if (mGenState == GENERATIOR_END || mGenState == GENERATIOR_STOP || mGenState == GENERATIOR_SUSPEND) 
		return;
	if (mGenState == GENERATIOR_START) 
	{
		mDurationTime += GetTickCount64() - mStartTime;
		if (mDurationTime > 5000) 
			mGenState = GENERATIOR_END;
		mStartTime = GetTickCount64();
	}
}

void Generator::wakeup()
{
	if (mGenState == GENERATIOR_STOP) 
	{
		mGenState = GENERATIOR_START;
		mStartTime = GetTickCount64();
	}
}

void Generator::suspend()
{
	mGenState = GENERATIOR_SUSPEND;
	mStartTime = 0;
}

void Generator::stop()
{
	mDurationTime = 5000;
	mGenState = GENERATIOR_END;
}

void Generator::start()
{
	Initalize();
}

void Generator::Write(Packet *& inPacket)
{
	PK_S_NTY_PLAYERPOS * castPacketPtr = (PK_S_NTY_PLAYERPOS*)inPacket;
	castPacketPtr->GeneratorNumber.emplace_back(mGeneratorNumber);
	castPacketPtr->GeneratorP.push_back(this->Percent());
}

int32_t Generator::Percent() {
	int32_t complete = 100;
	if (mGenState == GENERATIOR_END)
		return complete;
	int32_t percent = (int32_t)(mDurationTime / 5000) * 100;
	return percent;
}

//GeneratorManager::GeneratorManager()
//{
//	for()
//}
//
//GeneratorManager::~GeneratorManager()
//{
//}
//
//void GeneratorManager::initalize()
//{
//}
