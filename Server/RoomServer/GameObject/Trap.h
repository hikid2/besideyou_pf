#pragma once
#include"stdafx.h"

typedef enum TUPDATE
{
	none = 0,
	install = 1 << 0,
	invoked = 1 << 1,
	toDelete = 1 << 2,
}TrapUpdateEnum;

//  Ʈ������ġ�� ���
//  Ʈ���� ���� ���
class Trap : public GameObject
{
public:
	Trap() { mCatchPlayerId = 0; mUseAble = true; }
	virtual ~Trap()
	{
		this->free();
	}
	virtual void tick() {};
	virtual void initialize() {};
	virtual void free() {};
	virtual void SendOutgoingPackets() {};

	bool IsCollision(const RectF & collisionbox);
	void ToInstall(const D3DXVECTOR3 & inPosition, D3DXVECTOR3 & inLookvector, uint64_t inPlayer);
	uint64_t GetTrapNumber() { return mTrapNumber; }
	D3DXVECTOR3 GetPosition() { return mPosition; }
	void SetCatchPlayerId(uint64_t inId) { mCatchPlayerId = inId; }
	uint64_t GetCatchPlayerId() { return mCatchPlayerId; }
	void Catch(uint64_t inPlayer);
	void Write(Packet * inPacket);
	uint8_t GetUpdate() { return mUpdate; }
private:
	uint8_t mUpdate;
	uint64_t mTrapNumber;
	uint64_t mCatchPlayerId;
	D3DXVECTOR3 mPosition;
	RectF mCollesionBox;
	bool mUseAble;

	bool InterSectRectF(const RectF * r1, const RectF * r2)
	{
		if (r1->left  <  r2->right &&
			r1->top  <  r2->bottom &&
			r1->right  >  r2->left &&
			r1->bottom   >  r2->top)
		{
			return true;
		}
		return false;
	}
};



class TrapManager : public Singleton<TrapManager>
{
public:
	TrapManager();
	virtual ~TrapManager();
	void Add(Trap * inTrap, int64_t roomNumber);
	TrapResult isColliesion(const RectF & colisionbox, int64_t inkey);
	void Write(Packet * inPacket, int64_t inKey);
private:
	multimap<int64_t, Trap *> mTrapMap;
	// Trap * Find(int32_t index);
	Lock * mLock;
};



