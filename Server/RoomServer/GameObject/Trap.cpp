#include "stdafx.h"
#include "trap.h"


void Trap::ToInstall(const D3DXVECTOR3 & inPosition, D3DXVECTOR3 & inLookvector, uint64_t inPlayer)
{
	static int64_t TrapCount = 0;
	mTrapNumber = TrapCount++;
	if (!mUseAble) return;
	D3DXVECTOR3 d3dxvShift = D3DXVECTOR3(0, 0, 0);
	D3DXVECTOR3 d3dx = D3DXVECTOR3(0, 0, 0);
	D3DXVec3Normalize(&d3dx, &inLookvector);
	d3dxvShift += d3dx * 20.0f;
	d3dxvShift += inPosition;
	mPosition = d3dxvShift;
	SLog(L"Trap Position : %f %f %f ",mPosition.x, mPosition.y, mPosition.z);
	mCollesionBox.left = d3dxvShift.x - 0.866f;
	mCollesionBox.right = d3dxvShift.x + 0.866f;
	mCollesionBox.top = d3dxvShift.z - 0.866f;
	mCollesionBox.bottom = d3dxvShift.z + 0.866f;
	mCatchPlayerId = inPlayer;
	mUpdate = install;
}

void Trap::Catch(uint64_t inPlayer)
{
	mUpdate |= TrapUpdateEnum::invoked;
	mCatchPlayerId = inPlayer;
}

void Trap::Write(Packet * inPacket)
{	
	auto pk = reinterpret_cast<PK_S_NTY_PLAYERPOS *>(inPacket);
	if (this->mUpdate & TrapUpdateEnum::install)
	{
		pk->Tupdate.push(mUpdate);
		pk->trapx.push(mPosition.x);
		pk->trapy.push(mPosition.z);
		pk->trapId.push(mTrapNumber);
		pk->catchId.push(mCatchPlayerId);
		mUpdate = TUPDATE::none;
	}
	else if (this->mUpdate & TrapUpdateEnum::invoked)
	{
		pk->Tupdate.push(mUpdate);
		pk->trapId.push(mTrapNumber);
		pk->catchId.push(mCatchPlayerId);
		mUpdate = TUPDATE::toDelete;
	}
	mCatchPlayerId = 0;
}

bool Trap::IsCollision(const RectF & collisionbox)
{
	if (!mUseAble)
		return false;
	if (InterSectRectF(&collisionbox, &this->mCollesionBox))
	{
		SLog(L"boom");
		mUseAble = false;
		return true;
	}
	return false;
}


TrapManager::TrapManager()
{
	mLock = new Lock(L"TrapManager");

}

TrapManager::~TrapManager()
{
	SAFE_DELETE(mLock);
}

void TrapManager::Add(Trap * inTrap, int64_t roomNumber)
{
	SAFE_LOCK(*mLock);
	mTrapMap.insert(make_pair(roomNumber, inTrap));
}

TrapResult TrapManager::isColliesion(const RectF & colisionbox, int64_t inkey)
{
	// 방안에 설치된 트랩들을 찾아서
	auto lower = mTrapMap.lower_bound(inkey);
	auto upper = mTrapMap.upper_bound(inkey);
	TrapResult result = make_pair<bool, Trap*>(false, nullptr);
	SAFE_LOCK(*mLock);
	for (lower; lower != upper; ++lower)
	{
		// 충돌이 되었는지 확인
		if (lower->second->IsCollision(colisionbox))
		{
			result.second = lower->second;
			result.first = true;
			return result;
		}
	}
	return result;
}

void TrapManager::Write(Packet * inPacket, int64_t inkey)
{
	// 방안에 설치된 트랩들을 찾아서
	auto lower = mTrapMap.lower_bound(inkey);
	auto upper = mTrapMap.upper_bound(inkey);
	SAFE_LOCK(*mLock);
	for (lower; lower != upper;)
	{
		lower->second->Write(inPacket);
		if (lower->second->GetUpdate() == 4)
		{
			delete lower->second;
			lower = mTrapMap.erase(lower);
			continue;
		}
		++lower;
	}
}
