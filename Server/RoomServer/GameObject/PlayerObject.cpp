#include"stdafx.h"
#include"Trap.h"
#include "PlayerObject.h"


DirVectorInfo::DirVectorInfo()
{
}

DirVectorInfo::DirVectorInfo(const D3DXVECTOR3 inLook, const D3DXVECTOR3 inRight)
{
	mLookVector = inLook;
	mRightVector = inRight;
}

DirVectorInfo::DirVectorInfo(const DirVectorInfo & other)
{
	mLookVector = other.mLookVector;
	mRightVector = other.mRightVector;
}

DirVectorInfo::DirVectorInfo(DirVectorInfo && other)
{
	*this = std::move(other);
}

DirVectorInfo & DirVectorInfo::operator=(DirVectorInfo && other)
{
	if (this != &other)
	{
		mLookVector = std::move(other.mLookVector);
		mRightVector = std::move(other.mRightVector);
	}
	return *this;
}

PlayerObject::PlayerObject() 
{
	mAbleGamePlayer = true;
	IsLastMoveTimestamp = false;
	mAnimation = TYPE_IDEL;
}

void PlayerObject::Initialize()
{
	mAbleGamePlayer = true;
	mKeepGenNumber = -1;
	mAnimation = TYPE_IDEL;
	mLastDirection = 0;
	auto radious = DegreeToRadian(30.f);
	D3DXVECTOR3 upVector{ 0.f, 1.f, 0.f };

	mLastDirecionVector.mLookVector = { 0.f,0.f, 1.f };
	mLastDirecionVector.mRightVector = { 1.f,0.f ,0.f };
	D3DXMATRIX rotateMatirx;
	D3DXMatrixRotationAxis(&rotateMatirx, &upVector, radious);
	D3DXVec3TransformCoord(&mLastDirecionVector.mLookVector, &mLastDirecionVector.mLookVector, &rotateMatirx);
	D3DXVec3TransformCoord(&mLastDirecionVector.mRightVector, &mLastDirecionVector.mRightVector, &rotateMatirx);

	IsLastMoveTimestamp = false;
}



PlayerObject::PlayerObject(wstr_t name, Int64 uid, USER_ROLE role, D3DXVECTOR3 position, FLOAT degree, InstanceGameWorld * inInstanceWorld)
{

	this->SetName(name);
	mUid = uid;
	mRole = role;
	mInstanceWorld = inInstanceWorld;
	mCurrentLocation = position;
	mNextFrameLocation = position;
	mPrevLocation = position;
	mFixLocation = { 0.f,0.f,0.f };


	mMoveState = RUN_STATE;
	if (role != ROLE_KILLER) {
		mHp = 3;
		mTrapCount = 5;
		mSpeed = Sur_run;
		mVelocity = { 0.f,0.f, mSpeed };
		mUpdate = PUPDATE::UpdateNot;
	}
	else {
		mHp = NULL;
		mTrapCount = NULL;
		mSpeed = 0.0537F;
		mVelocity = { 0.f,0.f,mSpeed };
		mUpdate = PUPDATE::UpdateNot;
	}
	this->Initialize();
}

void PlayerObject::Initialize(wstr_t name, Int64 uid, USER_ROLE role, D3DXVECTOR3 position, InstanceGameWorld * inInstanceWorld)
{
	this->SetName(name);
	mUid = uid;
	mRole = role;
	mInstanceWorld = inInstanceWorld;
	mCurrentLocation = position;
	mNextFrameLocation = position;
	mPrevLocation = position;
	mFixLocation = { 0.f,0.f,0.f };


	mMoveState = RUN_STATE;
	if (role != ROLE_KILLER) {
		mHp = 3;
		mTrapCount = 5;
		mSpeed = Sur_run;
		mVelocity = { 0.f,0.f,mSpeed };
		mUpdate = PUPDATE::UpdateNot;
	}
	else {
		mHp = NULL;
		mTrapCount = NULL;
		mSpeed = 0.0537F;
		mVelocity = { 0.f,0.f,mSpeed };
		mUpdate = PUPDATE::UpdateNot;
	}
	this->Initialize();
}

PlayerObject::PlayerObject(PlayerObject & instance)
{
	this->SetName(instance.GetName());
	mUid = instance.mUid;
	mRole = instance.mRole;
	mInstanceWorld = nullptr;

	mHp = instance.mHp;
	mTrapCount = instance.mTrapCount;

}

PlayerObject::~PlayerObject()
{}

void PlayerObject::SendOutgoingPackets()
{
	/*if (lastMoveTimestamp())
	{
		PK_S_NTY_LASTMOVETIMESTAMP packet;
		packet.lastTime = moveData_.LastTimeStamp();
		auto session = SessionManager::getInstance().session(_oid);
		session->sendPacket(&packet);
		setIslastMoveTimestamp(true);
	}*/
}

D3DXVECTOR3 & PlayerObject::GetPos()
{
	return mCurrentLocation;
}

void PlayerObject::SetClear()
{
	mUid = -1;
	mRole = ROLE_NONE;
	mAnimation = TYPE_IDEL;
	mUpdate = PUPDATE::UpdateNot;
	mMoveState = STOP_STATE;
	mStatusBar = IDLE_STATE;
	mSpeed = 0.0f;
	mHp = 0;
	mTrapCount = 0;
	mLatency = 0;
}

void PlayerObject::Setuid(Int64 uid)
{
	mUid = uid;
}
const Int64 & PlayerObject::GetUid()
{
	return mUid;
}

void PlayerObject::SetOid(oid_t oid)
{
	mOid = oid;
}

const oid_t & PlayerObject::GetOid()
{
	return mOid;
}

void PlayerObject::operator=(PlayerObject & ins)
{
	this->SetName(ins.GetName());
	mUid = ins.mUid;
	mRole = ins.mRole;
	mMoveState = ins.mMoveState;
	mStatusBar = ins.mStatusBar;
	mSpeed = ins.mSpeed;
	mHp = ins.mHp;
	mTrapCount = ins.mTrapCount;
	mLatency = ins.mLatency;
}

void PlayerObject::SetMoveState(MOVE_STATE & state)
{
	// Killer는 걷는 상태의 변화가 없으므로...
	if (mRole == ROLE_KILLER) return;
	mMoveState = state;
	switch (mMoveState)
	{
	case RUN_STATE:
		mSpeed = Sur_run;
		break;
	case WALK_STATE:
		mSpeed = Sur_walk;
		break;
	case CROUCH_STATE:
		mSpeed = Sur_CrouchWalk;
		break;
	}
}
UINT64 & PlayerObject::Latency()
{
	return mLatency;
}
void PlayerObject::SetLatency(UINT64 time)
{
	mLatency = time;
}

void PlayerObject::UseTrap()
{
	if (mTrapCount > 0) {
		mTrapCount--;
		mUpdate |= PUPDATE::UpdateTrap;
	}
}

USER_ROLE & PlayerObject::GetRole()
{
	return mRole;
}

STATE_BAR & PlayerObject::GetStatebar()
{
	return mStatusBar;
}

INT32 & PlayerObject::TrapCount()
{
	return mTrapCount;
}

INT32 & PlayerObject::GetHp()
{
	return mHp;
}

UINT64 & PlayerObject::SystemRecordTick()
{
	mServerTime = GetTickCount64();
	return mServerTime;
}

UINT64 & PlayerObject::SystemTick()
{
	return mServerTime;
}

void PlayerObject::SetRole(USER_ROLE role)
{
	mRole = role;
}

void PlayerObject::SetStatebar(STATE_BAR state)
{
	mStatusBar = state;
}

void PlayerObject::SetTrapcount(INT32 count)
{
	mTrapCount = count;
}


void PlayerObject::SetHp(INT32 hp)
{
	mHp = hp;
}
BOOL PlayerObject::SetCurrentPos(D3DXVECTOR3 v)
{
	mCurrentLocation = v;
	return true;
}
void PlayerObject::SetNextFramePos(D3DXVECTOR3 & v)
{
	mNextFrameLocation = v;
}
void PlayerObject::SetEvent(EVENT_TYPE type)
{
	mEventType = type;
}
EVENT_TYPE PlayerObject::GetEventType()
{
	return mEventType;
}
void PlayerObject::SetFixPos(D3DXVECTOR3 & pos)
{
	mFixLocation = pos;
}
D3DXVECTOR3 PlayerObject::FixPos()
{
	return D3DXVECTOR3(mFixLocation);
}
void PlayerObject::FixPosClear()
{
	ZeroMemory(mFixLocation, sizeof(D3DXVECTOR3));
}

void PlayerObject::SetPrevPos(D3DXVECTOR3 & v)
{
	mPrevLocation = v;
}
//void PlayerObject::SetRotate(D3DXMATRIX & r)
//{
//	/*else if (up) { D3DXVECTOR3(0.f, 0.f, 1.f); }
//	else if (down) { D3DXVECTOR3(0.f, 0.f, -1.f); }
//	else if (left) { D3DXVECTOR3(-1.f, 0.f, 0.f); }
//	else if (right) { D3DXVECTOR3(1.f, 0.f, 0.f); }*/
//	RotateM_ = r;
//}
void PlayerObject::SetVelocity(float velocity)
{
	mVelocity = D3DXVECTOR3(0.f, 0.f, velocity);
}
D3DXVECTOR3 & PlayerObject::Velocity_D()
{
	return mVelocity;
}
float & PlayerObject::Velocity_F()
{
	return mVelocity.z;
}




RectF PlayerObject::TimeRollPos(int halfRTT)
{

	int offset = halfRTT;
	for (auto back = mRecordPos.rbegin(); back != mRecordPos.rend(); ++back)
	{
		offset -= (*back)->first;
		if (offset < 0)
		{
			return (*back)->second;
		}
	}
	RectF RollPos;
	ZeroMemory(&RollPos, sizeof(RollPos));
	return RollPos;
}
bool PlayerObject::IsColision(RectF colisionbox)
{
	RectF ownBox;
	D3DXVECTOR3 d3dxvShift = D3DXVECTOR3(0, 0, 0);
	d3dxvShift += mLastDirecionVector.mRightVector * 3;
	d3dxvShift += mCurrentLocation;
	ownBox.left = d3dxvShift.x - 3.f;
	ownBox.right = d3dxvShift.x + 3.f;
	ownBox.top = d3dxvShift.z - 3.f;
	ownBox.bottom = d3dxvShift.z + 3.f;
	/*SLog(L"own ColisionBox [left %.3f] [right %.3f] [top %.3f] [down %.3f] ", ownBox.left, ownBox.right, ownBox.top, ownBox.bottom);
	SLog(L"city ColisionBox [left %.3f] [right %.3f] [top %.3f] [down %.3f] ", colisionbox.left, colisionbox.right, colisionbox.top, colisionbox.bottom);*/
	if (InterSectRectF(&colisionbox, &ownBox))
		return true;
	else return false;
}

void PlayerObject::TimeRollBack(int LatencyTime)
{
	auto playerList = mInstanceWorld->GetplayerListPtr();
	for (int index = 0, maxcnt = (int32_t)(*playerList).size(); index < maxcnt; ++index)
	{
		if ((*playerList)[index] == nullptr)continue;
		if ((*playerList)[index]->mUid == mUid)continue;
		if ((*playerList)[index]->mRole == ROLE_KILLER)continue;
		auto colisionBox = (*playerList)[index]->TimeRollPos(LatencyTime);
		if (IsColision(colisionBox)) {
			if ((*playerList)[index]->mAnimation == TYPE_INTERACTION) {
				auto ptr = GeneratorManager::getInstance().GetGenPointer((*playerList)[index]->mKeepGenNumber);
				ptr->suspend();
				this->mKeepGenNumber = -1;
			}
			(*playerList)[index]->mAnimation = TYPE_HITTED;
			(*playerList)[index]->mHp -= 1;
			(*playerList)[index]->mUpdate |= PUPDATE::UpdateHP;
		}
	}
}

void PlayerObject::WoundTrap()
{
	if (mHp > 0) {
		mStatusBar = WOUNDTRAP_STATE;
		--mHp;
		mUpdate |= PUPDATE::UpdateHP | PUPDATE::UpdateStatus;
	}
}
void PlayerObject::MoveSimulation(Move go)
{
	int64_t roomNumber = mInstanceWorld->GetInstanceNO();
	// 속도에 대한 수정이 필요,,
	// 수정 전
	INT32 dwDirection = go.nowDirection();
	D3DXVECTOR3 m_d3dxvLook = go.LookVector();
	D3DXVECTOR3 m_d3dxvRight = go.RightVector();
	float Velocity = (float)go.Velocity();
	// 수정 후
	mLastDirection = go.nowDirection();
	mLastDirecionVector.mLookVector = go.LookVector();
	mLastDirecionVector.mRightVector = go.RightVector();
	//if (dwDirection & DIR_TRAPINSTALL) {
	//	if (mHp == 1) { mAnimation = TYPE_INJUREDIDLE; }
	//	else { mAnimation = TYPE_IDEL; }
	//	updatedTime_ = go.MoveTimeTampt();
	//	return;
	//}

	if (dwDirection & (DIR_FORWARD | DIR_BACKWARD | DIR_LEFT | DIR_RIGHT)) mLastDirectionKey = dwDirection;
	float fDistance = Velocity * go.DetaTime();

	D3DXVECTOR3 d3dxvShift = D3DXVECTOR3(0, 0, 0);
	D3DXVec3Normalize(&m_d3dxvLook, &m_d3dxvLook);
	D3DXVec3Normalize(&m_d3dxvRight, &m_d3dxvRight);
	if (mRole == ROLE_SURVIORS)
	{
		if (dwDirection & DIR_CROUCH)
		{
			if (0 == (mAnimation & TYPE_CROUSHALL))
			{
				//SLog(L"TYPE_CROUSHON");
				mAnimation = TYPE_CROUSHON;
			}
			else
			{
				if (dwDirection & DIR_FORWARD) {
					mAnimation = TYPE_CROUSHFORWORDWALK;
					d3dxvShift += m_d3dxvLook * fDistance;
				}
				if (dwDirection & DIR_BACKWARD) {
					mAnimation = TYPE_CROUSHBACKWALK;
					d3dxvShift -= m_d3dxvLook * fDistance;
				}
				//화살표 키 ‘→’를 누르면 로컬 x-축 방향으로 이동한다. ‘←’를 누르면 반대 방향으로 이동한다.
				if (dwDirection & DIR_RIGHT) {
					mAnimation = TYPE_CROUSHRIGHTWALK;
					d3dxvShift += m_d3dxvRight * fDistance;
				}
				if (dwDirection & DIR_LEFT) {
					mAnimation = TYPE_CROUSHLEFTWALK;
					d3dxvShift -= m_d3dxvRight * fDistance;
				}
			}
			// TODO .. 상황마다 속도를 다르게

		}
		else if (dwDirection & DIR_CROUCHUP)
		{
			mAnimation = TYPE_CROUSHOFF;
		}
		else if (dwDirection & DIR_WALK)
		{
			// TODO .. 상황마다 속도를 다르게

			if (dwDirection & DIR_FORWARD) {
				mAnimation = TYPE_FORWORDWALK;
				d3dxvShift += m_d3dxvLook * fDistance;
			}
			if (dwDirection & DIR_BACKWARD) {
				mAnimation = TYPE_BACKWALK;
				d3dxvShift -= m_d3dxvLook * fDistance;
			}
			//화살표 키 ‘→’를 누르면 로컬 x-축 방향으로 이동한다. ‘←’를 누르면 반대 방향으로 이동한다.
			if (dwDirection & DIR_RIGHT) {
				mAnimation = TYPE_RIGHTWALK;
				d3dxvShift += m_d3dxvRight * fDistance;
			}
			if (dwDirection & DIR_LEFT) {
				mAnimation = TYPE_LEFTWALK;
				d3dxvShift -= m_d3dxvRight * fDistance;
			}
		}
		else
		{
			// TODO .. 상황마다 속도를 다르게
			if (dwDirection & DIR_FORWARD) {
				if (mHp == 1) { mAnimation = TYPE_INJUREDFORWORDWALK; }
				else { mAnimation = TYPE_FORWARDRUN; }
				d3dxvShift += m_d3dxvLook * fDistance;
			}
			if (dwDirection & DIR_BACKWARD) {
				if (mHp == 1) { mAnimation = TYPE_INJUREDBACKWALK; }
				else { mAnimation = TYPE_BACKRUN; }
				d3dxvShift -= m_d3dxvLook * fDistance;
			}
			//화살표 키 ‘→’를 누르면 로컬 x-축 방향으로 이동한다. ‘←’를 누르면 반대 방향으로 이동한다.
			if (dwDirection & DIR_RIGHT) {
				if (mHp == 1) { mAnimation = TYPE_INJUREDRIGHTWALK; }
				else { mAnimation = TYPE_RIGHTRUN; }
				d3dxvShift += m_d3dxvRight * fDistance;
			}
			if (dwDirection & DIR_LEFT) {
				if (mHp == 1) { mAnimation = TYPE_INJUREDLEFTWALK; }
				else { mAnimation = TYPE_LEFTRUN; }
				d3dxvShift -= m_d3dxvRight * fDistance;
			}
			if ((dwDirection & (DIR_LEFT | DIR_FORWARD | DIR_BACKWARD | DIR_RIGHT)) == 0 && mAnimation != TYPE_HITTED && mAnimation!= TYPE_TRAPINSTALL)
			{
				if (mHp == 1) { mAnimation = TYPE_INJUREDIDLE; }
				else { mAnimation = TYPE_IDEL; }
			}
		}
	}
	//화살표 키 ‘↑’를 누르면 로컬 z-축 방향으로 이동(전진)한다. ‘↓’를 누르면 반대 방향으로 이동한다.
	//if (dwDirection & DIR_FORWARD) {
	//	d3dxvShift += m_d3dxvLook * fDistance;
	//}
	//if (dwDirection & DIR_BACKWARD) {
	//	d3dxvShift -= m_d3dxvLook * fDistance;
	//}
	////화살표 키 ‘→’를 누르면 로컬 x-축 방향으로 이동한다. ‘←’를 누르면 반대 방향으로 이동한다.
	//if (dwDirection & DIR_RIGHT) {
	//	d3dxvShift += m_d3dxvRight * fDistance;
	//}
	//if (dwDirection & DIR_LEFT) {
	//	d3dxvShift -= m_d3dxvRight * fDistance;
	//}
	if (mRole == ROLE_KILLER)
	{
		if (dwDirection & DIR_ATTACT)
		{
			this->mAnimation = TYPE_ATTACK;
			// TODO : Object1/2RTT 받을 필요가 있음
			int HalfRtt = 0;
			TimeRollBack(HalfRtt);
		}
		if (dwDirection & DIR_FORWARD) {
			mAnimation = TYPE_FORWARDRUN;
			d3dxvShift += m_d3dxvLook * fDistance;
		}
		if (dwDirection & DIR_BACKWARD) {
			mAnimation = TYPE_BACKRUN;
			d3dxvShift -= m_d3dxvLook * fDistance;
		}
		//화살표 키 ‘→’를 누르면 로컬 x-축 방향으로 이동한다. ‘←’를 누르면 반대 방향으로 이동한다.
		if (dwDirection & DIR_RIGHT) {
			mAnimation = TYPE_RIGHTRUN;
			d3dxvShift += m_d3dxvRight * fDistance;
		}
		if (dwDirection & DIR_LEFT) {
			mAnimation = TYPE_LEFTRUN;
			d3dxvShift -= m_d3dxvRight * fDistance;
		}
	}
	if (mRole == ROLE_SURVIORS)
	{
		if (dwDirection & DIR_INTERACTION)
		{
			if (this->mAnimation != TYPE_INTERACTION && go.GeneratorNumber() != -1) {
				this->mAnimation = TYPE_INTERACTION;
				this->mKeepGenNumber = go.GeneratorNumber();
				auto ptr = GeneratorManager::getInstance().GetGenPointer(go.GeneratorNumber());
				ptr->wakeup();
			}
			else
			{

			}
		}
		else if (this->mAnimation == TYPE_INTERACTION)
		{
			this->mAnimation = TYPE_IDEL;
			auto ptr = GeneratorManager::getInstance().GetGenPointer(mKeepGenNumber);
			ptr->suspend();
			this->mKeepGenNumber = -1;
		}
		// remark : 트랩이 눌렸을 때
		else if ((dwDirection & DIR_TRAP) && mTrapCount > 0)
		{
			this->mAnimation = TYPE_TRAPINSTALL;
			SLog(L"uid : %d Trap animation [%d] key [%d]", this->mUid, this->mAnimation, mLastDirectionKey);
			this->UseTrap();
			Trap * ptr = new Trap();
			//ptr->ToInstall(mCurrentLocation, Lookvector(), mUid);
			if (mLastDirectionKey &DIR_FORWARD) ptr->ToInstall(mCurrentLocation, mLastDirecionVector.mLookVector, mUid);
			else if (mLastDirectionKey &DIR_BACKWARD) ptr->ToInstall(mCurrentLocation, - mLastDirecionVector.mLookVector, mUid);
			else if (mLastDirectionKey &DIR_LEFT) ptr->ToInstall(mCurrentLocation, - mLastDirecionVector.mRightVector, mUid);
			else if (mLastDirectionKey &DIR_RIGHT) ptr->ToInstall(mCurrentLocation, mLastDirecionVector.mRightVector, mUid);
			TrapManager::getInstance().Add(ptr, roomNumber);
		}
		if (mHp == 1) {
			mStatusBar = EMERGENCY_STATE;
			mUpdate |= PUPDATE::UpdateStatus;
		}
		if (mHp <= 0) {
			mStatusBar = DEAD_STATE;
			mUpdate |= PUPDATE::UpdateStatus;
		}
		//SLog(L"32 ANI??    %d", mAnimation);
	}
	//d3dxvShift *= deltaTime;
	float fLength = sqrtf(d3dxvShift.x * d3dxvShift.x + d3dxvShift.z * d3dxvShift.z);
	float fMaxVelocityXZ = 300.f * (float)go.DetaTime();
	if (fLength > fMaxVelocityXZ)
	{
		d3dxvShift.x *= (fMaxVelocityXZ / fLength);
		d3dxvShift.z *= (fMaxVelocityXZ / fLength);
	}
	mCurrentLocation += d3dxvShift;
	RectF ColisionBox;
	// 임시
	//ColisionBox.left = Currentpos_.x - 15.32;
	//ColisionBox.right = Currentpos_.x + 15.32;
	//ColisionBox.bottom = Currentpos_.z + 9.25;
	//ColisionBox.top = Currentpos_.z - 9.25;
	ColisionBox.left = mCurrentLocation.x - 5.25;
	ColisionBox.right = mCurrentLocation.x + 5.25;
	ColisionBox.bottom = mCurrentLocation.z + 4.32;
	ColisionBox.top = mCurrentLocation.z - 4.32;
	auto Result = TrapManager::getInstance().isColliesion(ColisionBox, roomNumber);
	if (Result.first == true)
	{
		Result.second->Catch(this->mUid);
		this->WoundTrap();
	}
	if (mRecordPos.size() > 10)
	{
		auto renew = mRecordPos.front();
		mRecordPos.pop_front();
		renew->first = static_cast<int>(go.DetaTime() * 1000);
		renew->second = ColisionBox;
		mRecordPos.push_back(renew);
	}
	else
	{
		auto durationTime = static_cast<int>(go.DetaTime() * 1000);
		auto a = new pair<int, RectF>(durationTime, ColisionBox);
		mRecordPos.push_back(a);
	}
	mUpdatedTime = go.MoveTimeTampt();
	//SLog(L"uid : %d animation [%d]", this->mUid, this->mAnimation);
	//SLog(L"Animation num[%d]", this->mAnimation);
}
void PlayerObject::Tick()
{
	size_t movesize = mMoveData.size();
	if (movesize == 0)return;
	deque<Move> unsimulationlist;
	//SLog(L"uid [%d] zzz moveData_.befor size() [ %d ]", this->mUid, moveData_.size());
	auto Mlist = mMoveData.movelist();
	(*Mlist).copy_and_delete(unsimulationlist);
	//SLog(L"uid [%d] ooo  moveData_.after size() [ %d ]", this->mUid, moveData_.size());

	for (auto go : unsimulationlist)
	{
		MoveSimulation(go);
	}
}

void PlayerObject::Write(Packet * inPacket)
{
	auto packet = reinterpret_cast<PK_S_NTY_PLAYERPOS *>(inPacket);
	packet->uid.emplace_back(mUid);
	packet->px.emplace_back(mCurrentLocation.x);
	packet->py.emplace_back(mCurrentLocation.y);
	packet->pz.emplace_back(mCurrentLocation.z);
	packet->LastDirection.emplace_back(mLastDirection);
	packet->LastLookvect.emplace_back(mLastDirecionVector.mLookVector.x);
	packet->LastLookvect.emplace_back(mLastDirecionVector.mLookVector.y);
	packet->LastLookvect.emplace_back(mLastDirecionVector.mLookVector.z);
	packet->LastRightvect.emplace_back(mLastDirecionVector.mRightVector.x);
	packet->LastRightvect.emplace_back(mLastDirecionVector.mRightVector.y);
	packet->LastRightvect.emplace_back(mLastDirecionVector.mRightVector.z);
	packet->LastRecvTime.emplace_back(mMoveData.LastTimeStamp());
	packet->LastUpdateTime.emplace_back(mUpdatedTime);
	packet->AnimationType.push_back(mAnimation);

	if (mAnimation == TYPE_ATTACK) {
		mAnimation = TYPE_IDEL;
	}
	if (mAnimation == TYPE_TRAPINSTALL)
	{
		if (mHp == 1) {
			mAnimation = TYPE_INJUREDIDLE;
		}
		else { mAnimation = TYPE_IDEL; }
	}
	if (mAnimation == TYPE_HITTED) {
		if (mHp == 0) { mAnimation = TYPE_DIE; }
		if (mHp == 1) {
			mAnimation = TYPE_INJUREDIDLE;
		}
		else { mAnimation = TYPE_IDEL; }
		//SLog(L"Hitted");
	}
	if (mAnimation == TYPE_CROUSHON)
	{
		//SLog(L"croushIDLE");
		mAnimation = TYPE_CROUSHIDLE;
	}

	packet->Playerupdate.emplace_back(mUpdate);


	if (mUpdate & PUPDATE::UpdateHP)
	{
		packet->hp.push(mHp);
		SLog(L"mUpdate [%d]", mUpdate);
		SLog(L"mHp [%d]", mHp);
		SLog(L"mHp size [%d]", packet->hp.size());
	}

	if (mUpdate & PUPDATE::UpdateTrap)
	{
		packet->trapCount.push(mTrapCount);
		SLog(L"TrapCount [%d]", mTrapCount);
		SLog(L"TrapCount size [%d]", packet->trapCount.size());
	}
	if (mUpdate & PUPDATE::UpdateStatus)
	{
		packet->statusUI.push(mStatusBar);
		SLog(L"TrapCount [%d]", mStatusBar);
		SLog(L"TrapCount size [%d]", packet->statusUI.size());
	}

	mUpdate = PUPDATE::UpdateNot;
}

