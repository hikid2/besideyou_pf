#pragma once
typedef enum
{
	ROLE_NONE,
	ROLE_KILLER,
	ROLE_SURVIORS,
}USER_ROLE;
typedef enum
{
	STOP_STATE,				// 멈춰 있는 상태(아무 이동키도 누르지 않은 상태)
	RUN_STATE,				// 달리는 상태(일반적)
	WALK_STATE,				// 걷는 상태(특수 키 누르고 이동시)
	CROUCH_STATE,			// 숙여있는 상태(특수 키누르고 있을 시)
	EMPLACETRAP_STATE,		// 트랩을 설치하는 상태(이동하지못함)
}MOVE_STATE;

typedef enum
{
	TYPE_IDEL = 1 << 0,
	TYPE_FORWARDRUN = 1 << 1,
	TYPE_BACKRUN = 1 << 2,
	TYPE_LEFTRUN = 1 << 3,
	TYPE_RIGHTRUN = 1 << 4,
	TYPE_INJUREDIDLE = 1 << 5,
	TYPE_INJUREDFORWORDWALK = 1 << 6,
	TYPE_INJUREDBACKWALK = 1 << 7,
	TYPE_INJUREDLEFTWALK = 1 << 8,
	TYPE_INJUREDRIGHTWALK = 1 << 9,
	TYPE_INTERACTION = 1 << 10,
	TYPE_ATTACK = 1 << 11,
	TYPE_HITTED = 1 << 12,
	TYPE_TRAPINSTALL = 1 << 13,
	TYPE_STEPONTRAP = 1 << 14,
	TYPE_CROUSHON = 1 << 15,
	TYPE_CROUSHOFF = 1 << 16,
	TYPE_CROUSHIDLE = 1 << 17,
	TYPE_CROUSHFORWORDWALK = 1 << 18,
	TYPE_CROUSHBACKWALK = 1 << 19,
	TYPE_CROUSHLEFTWALK = 1 << 20,
	TYPE_CROUSHRIGHTWALK = 1 << 21,
	TYPE_FORWORDWALK = 1 << 22,
	TYPE_BACKWALK = 1 << 23,
	TYPE_LEFTWALK = 1 << 24,
	TYPE_RIGHTWALK = 1 << 25,
	TYPE_DIE = 1 << 26,
	TYPE_SPRINT = 1 << 27,
	TYPE_CROUSHALL = TYPE_CROUSHON | TYPE_CROUSHOFF | TYPE_CROUSHIDLE | TYPE_CROUSHFORWORDWALK | TYPE_CROUSHBACKWALK | TYPE_CROUSHLEFTWALK | TYPE_CROUSHRIGHTWALK,
	TYPE_WALKALL = TYPE_BACKWALK | TYPE_LEFTWALK | TYPE_FORWORDWALK | TYPE_RIGHTWALK,
}ANIMATION_TYPE;
typedef enum
{
	IDLE_STATE,				// 보통상태
	WOUNDTRAP_STATE,		// 트랩에 걸린 상태
	EMERGENCY_STATE,		// 체력이 얼마 안 남았을때 상태
	DEAD_STATE,				// 죽은 상태
	NOT_USEED,				// 미사용
}STATE_BAR;

typedef enum
{
	EVENT_NONE,
	EVENT_MOVESTART,
	EVENT_MOVESTOP,
	EVENT_DEADRECKNING,
	EVENT_INTERPOLATION,
	EVENT_ATT,
	EVENT_OPERATORON,
	EVENT_TRAPON,
}EVENT_TYPE;

typedef enum
{
	UpdateNot = 0,
	UpdateHP = 1 << 0,
	UpdateTrap = 1 << 1,
	UpdateStatus = 1 << 2,
	UpdateAll = UpdateHP | UpdateTrap | UpdateStatus,
}PUPDATE;
class DirVectorInfo
{
public:
	DirVectorInfo();
	DirVectorInfo(D3DXVECTOR3, D3DXVECTOR3);
	DirVectorInfo(const DirVectorInfo &);
	DirVectorInfo(DirVectorInfo && other);
	DirVectorInfo & operator=(DirVectorInfo && other);

	D3DXVECTOR3 mLookVector;
	D3DXVECTOR3 mRightVector;
};
class Move
{
public:
	Move() : mMoveTimeTampt(0), mDeltaTime(0) {}
	Move(INT32 nowDirection, INT32 lastDirection, D3DXVECTOR3 lookvector,
		D3DXVECTOR3 rightvector, UINT64 TimeStampt, float deltaTime, double inVelocity, INT32 GeneratorNumber)
		: mNowDirection(nowDirection), mLastDirection(lastDirection), mLookVector(lookvector),
		mRightVector(rightvector), mMoveTimeTampt(TimeStampt), mDeltaTime(deltaTime), mGeneratorNumber(GeneratorNumber), mVelocity(inVelocity)
	{}
	Move(const Move & t) {
		mDeltaTime = t.mDeltaTime;
		mLastDirection = t.mLastDirection;
		mLookVector = t.mLookVector;
		mMoveTimeTampt = t.mMoveTimeTampt;
		mNowDirection = t.mNowDirection;
		mRightVector = t.mRightVector;
		mGeneratorNumber = t.mGeneratorNumber;
		mVelocity = t.mVelocity;
	}

	~Move() {}

	Move(Move && other) {
		*this = std::move(other);
	}

	Move & operator=(Move && other)
	{
		if (this != &other)
		{
			mLookVector = std::move(other.mLookVector);
			mRightVector = std::move(other.mRightVector);
			mNowDirection = std::move(other.mNowDirection);
			mMoveTimeTampt = std::move(other.mMoveTimeTampt);
			mDeltaTime = std::move(other.mDeltaTime);
			mGeneratorNumber = std::move(other.mGeneratorNumber);
			mLastDirection = std::move(other.mLastDirection);
			mVelocity = std::move(other.mVelocity);
		}
		return *this;
	}

	Move & operator=(Move & other)
	{
		mLookVector = other.mLookVector;
		mRightVector = other.mRightVector;
		mNowDirection = other.mNowDirection;
		mMoveTimeTampt = other.mMoveTimeTampt;
		mDeltaTime = other.mDeltaTime;
		mGeneratorNumber = other.mGeneratorNumber;
		mLastDirection = other.mLastDirection;
		mVelocity = other.mVelocity;
		return *this;
	}
	D3DXVECTOR3 & LookVector() { return mLookVector; }
	D3DXVECTOR3 & RightVector() { return mRightVector; }
	INT32 & nowDirection() { return mNowDirection; }
	INT32 & lastDirection() { return mLastDirection; }
	UINT64 & MoveTimeTampt() { return mMoveTimeTampt; }
	float & DetaTime() { return mDeltaTime; }
	INT32 & GeneratorNumber() { return mGeneratorNumber; }
	double & Velocity() { return mVelocity; }
private:
	D3DXVECTOR3 mLookVector;
	D3DXVECTOR3 mRightVector;
	INT32		mNowDirection;
	UINT64		mMoveTimeTampt;
	float		mDeltaTime;
	INT32		mGeneratorNumber;
	INT32		mLastDirection;
	double		mVelocity;
};
class MoveList
{
public:
	MoveList() : mLastTempTime(0), mMoveList(L"MoveList") {}
	~MoveList() { this->clear(); }
	bool insertTimeTempt(INT32 nowDirection, INT32 LastDirection, D3DXVECTOR3 LookVector,
		D3DXVECTOR3 RightVector, UINT64 TimeStampt, float deltaTIme, double inVelocity, INT32 GeneratorNumber)
	{
		if (TimeStampt < mLastTempTime)
		{
			return false;
		}
		Move move{ nowDirection, LastDirection, LookVector, RightVector, TimeStampt, deltaTIme , inVelocity, GeneratorNumber };

		mMoveList.emplace_back(move);
		mLastTempTime = TimeStampt;
		return true;
	}
	UINT64 LastTimeStamp() { return mLastTempTime; }
	void removeLastTimeTempt(UINT64 LastTimeTampt)
	{
		size_t size = mMoveList.size();
		for (auto iter = 0; iter < size; ++iter)
		{
			Move temp;
			if (mMoveList.top(temp))
			{
				if (temp.MoveTimeTampt() < LastTimeTampt) {
					mMoveList.pop(temp);
				}
				else return;
			}

		}
	}
	void clear() { mMoveList.clear(); }
	bool HasMoves() { return mMoveList.size() != 0; }
	size_t size() { return mMoveList.size(); }
	SafeDeque<Move> * movelist() { return  &mMoveList; }
private:
	SafeDeque<Move> mMoveList;
	UINT64 mLastTempTime;
};

class InstanceGameWorld;

class PlayerObject : public GameObject
{
public:
	bool				mAbleGamePlayer;

	PlayerObject();
	void SetRoom(InstanceGameWorld * inInstanceWorld) { mInstanceWorld = inInstanceWorld; }

	ANIMATION_TYPE animation() { return mAnimation; }

	void Setanimation(ANIMATION_TYPE ani) { mAnimation = ani; }

	void Initialize();

	PlayerObject(wstr_t name, Int64 uid, USER_ROLE role,
		D3DXVECTOR3 position, FLOAT degree, InstanceGameWorld * inInstanceWorld);

	void Initialize(wstr_t name, Int64 uid, USER_ROLE role,
		D3DXVECTOR3 position, InstanceGameWorld * inInstanceWorld);

	PlayerObject(PlayerObject & instance);

	virtual ~PlayerObject();

	MoveList & GetMoveData() { return mMoveData; }

	void SetKeepGenNumber(int number) { mKeepGenNumber = number; }

	int KeepGenNumber() { return mKeepGenNumber; }

	void SetIslastMoveTimestamp(bool inIsDirty) { IsLastMoveTimestamp = inIsDirty; }

	bool LastMoveTimestamp() const { return IsLastMoveTimestamp; }

	void SetUpdatedTime(UINT64 time) { mUpdatedTime = time; }

	UINT64 UpdatedTime() { return mUpdatedTime; }

	virtual void SendOutgoingPackets();

	D3DXVECTOR3 & GetPos(); // 현재 좌표 

	void SetClear();

	void Setuid(Int64 uid);

	const Int64 & GetUid();

	void SetOid(oid_t oid);

	const oid_t & GetOid();

	void operator=(PlayerObject & ins);

	void SetMoveState(MOVE_STATE & state);

	UINT64 & Latency();

	void SetLatency(UINT64 time);		// 레이턴시 수정

	void UseTrap();						// 트렙 사용시

	void MoveSimulation(Move go);

	bool IsColision(RectF colisionbox);

	USER_ROLE & GetRole();

	STATE_BAR & GetStatebar();

	INT32 & TrapCount();

	INT32 & GetHp();

	UINT64 & SystemRecordTick();

	UINT64 & SystemTick();


	void SetLookvector(D3DXVECTOR3 vec) { mLastDirecionVector.mLookVector = vec; }
	void SetRightkvector(D3DXVECTOR3 vec) { mLastDirecionVector.mRightVector= vec; }
	void SetlastDirection_(INT32 tick) { mLastDirection = tick; }

	D3DXVECTOR3 & Lookvector() { return mLastDirecionVector.mLookVector; }
	D3DXVECTOR3 & Rightkvector() { return mLastDirecionVector.mRightVector; }
	INT32 LastDirection() { return mLastDirection; }

	const D3DXVECTOR3 Lookvector()const { return mLastDirecionVector.mLookVector; }
	const D3DXVECTOR3 Rightvector()const { return mLastDirecionVector.mRightVector; }


	void SetRole(USER_ROLE role);
	void SetStatebar(STATE_BAR state);
	void SetTrapcount(INT32 count);
	void SetHp(INT32 hp);


	BOOL SetCurrentPos(D3DXVECTOR3 v); // 좌표 변경
	void SetNextFramePos(D3DXVECTOR3 & v);

	void SetEvent(EVENT_TYPE type);
	EVENT_TYPE GetEventType();

	void SetFixPos(D3DXVECTOR3 & pos);
	D3DXVECTOR3 FixPos();
	void FixPosClear();


	void SetPrevPos(D3DXVECTOR3 & v);
	void SetVelocity(float velocity);
	D3DXVECTOR3 & Velocity_D();
	float & Velocity_F();
	/*void SetDirection(D3DXVECTOR3 v);
	D3DXVECTOR3 & Direction();*/

	RectF TimeRollPos(int halfRTT);

	void TimeRollBack(int LatencyTime);

	void WoundTrap();
	void Tick();
	void Wakeup() {}
	void Stop() {}
	void Start() {}

	void Write(Packet * inPacket);

	bool InterSectRectF(const RectF * r1, const RectF * r2)
	{
		if (r1->left  <  r2->right &&
			r1->top  <  r2->bottom &&
			r1->right  >  r2->left &&
			r1->bottom   >  r2->top)
		{
			return true;
		}
		return false;
	}
private:
	uint8_t				mUpdate;
	UINT64				mServerTime;
	INT64				mUid;					// 유저 넘버
	USER_ROLE			mRole;					// 유저의 역할
	oid_t				mOid;					// RoomSesion oid
private:
	D3DXVECTOR3			mCurrentLocation;			// 현재 위치
	D3DXVECTOR3			mNextFrameLocation;			// 다음 좌표
	D3DXVECTOR3			mPrevLocation;				// 이전 좌표
	D3DXVECTOR3			mFixLocation;				// 변화에 따른 보정 좌표

	DirVectorInfo		mLastDirecionVector;
	//D3DXVECTOR3			mLastLookVector;
	//D3DXVECTOR3			mLastRightVector;

	INT32				mLastDirection;
	int32_t				mLastDirectionKey;

	ANIMATION_TYPE		mAnimation;
	D3DXVECTOR3			mVelocity;				// 속도

	MOVE_STATE			mMoveState;				// 이동상태
	STATE_BAR			mStatusBar;				// 상태 창
	FLOAT				mSpeed;					// 이동 속도
	INT32				mHp;					// 체력 
	INT32				mTrapCount;				// 트랩 수
	UINT64				mLatency;				// 레이턴시 count;

	EVENT_TYPE			mEventType;

	InstanceGameWorld * mInstanceWorld;

	bool IsLastMoveTimestamp;

	std::list<pair<int, RectF> *> mRecordPos;


	MoveList			mMoveData;
	UINT64				mUpdatedTime;
	int					mKeepGenNumber;

private:
	static const float Killer_speed;	// 살인마 이동속도
	static const float Sur_run;			// 생존자 뛰는속도
	static const float Sur_walk;		// 생존자 걷는속도
	static const float Sur_CrouchWalk;		// 생존자 기는속도
};


const float PlayerObject::Killer_speed = 0.026f;
const float PlayerObject::Sur_run = 0.021f;
const float PlayerObject::Sur_walk = 0.014f;
const float PlayerObject::Sur_CrouchWalk = 0.014f;
