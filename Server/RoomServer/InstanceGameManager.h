#pragma once

class InstanceGameManager : public Singleton<InstanceGameManager>
{
public:
	typedef pair<bool, InstanceGameWorld *> RoomObject;
public:
	InstanceGameManager();

	virtual ~InstanceGameManager();

	void Initalize();

	void Clear(int64_t Roomindex);

	bool At(int64_t inIndex, InstanceGameWorld *& outObjectPtr);

	bool At(int64_t index, bool first, InstanceGameWorld *& outObjectPtr);

	bool OperatingGame(int64_t roomindex, PK_T_NTF_STARTUSERDATA * packet);

	bool CheckBuildObjectSucc(int64_t & index, int64_t & uid);

	void tick() {}

private:
	std::array<RoomObject, ROOMMAXCOUNT> mInstanceGameList;
	Lock mLock;

	bool IsValidIndex(const int64_t & inIndex);
};