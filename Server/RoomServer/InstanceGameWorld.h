#pragma once
#include"stdafx.h"

#define ROOMMAXCOUNT 200
class InstanceGameWorld
{
public:
	void SetInstanceNO(int64_t inNO);

	int64_t & GetInstanceNO() { return mInstanceNO; }

	InstanceGameWorld(int64_t inNO);

	~InstanceGameWorld();

	void Clear();

	void GameStart(PK_T_NTF_STARTUSERDATA * inPacket);

	void GameExit();

	void update();

	void BroadCastSend();

	std::array<PlayerObject*, 4> * GetplayerListPtr() { return &mPlayerList; }

	bool SuccessfulBuild();

	typedef array<int, 10 > GeneratorContainer;
	void Write(Packet * inPacket);
private:
	GeneratorContainer				mGeneratorList;
	Lock							mLock;
	int64_t							mInstanceNO;
	std::array<PlayerObject*, 4>	mPlayerList;
	volatile array<STATE_BAR, 3>	mUserStatebar;
	size_t							mUnSuccesfulBuild;
};
