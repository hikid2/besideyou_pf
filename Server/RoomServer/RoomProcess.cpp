#include"stdafx.h"
#include "RoomProcess.h"

IngameProcess::IngameProcess()
{
	_serverTime = GetTickCount64();
	this->RegistSubContents();
}

void IngameProcess::RegistSubContents()
{
	mContentsMethodTable.insert(make_pair(PE_C_REQ_FIRSTSIGNAL, &IngameProcess::CPacket_FIRSTSIGNAL));
	mContentsMethodTable.insert(make_pair(PE_C_NTY_LATENCYCHECK, &IngameProcess::CPacket_LATENCYCHECK));
	mContentsMethodTable.insert(make_pair(PE_T_NTF_STARTUSERDATA, &IngameProcess::TPacket_STARTUSERDATA));
	mContentsMethodTable.insert(make_pair(PE_C_REQ_OWNMOVE, &IngameProcess::CPacket_USERMOVE));
	mContentsMethodTable.insert(make_pair(PE_C_REQ_MOVESTART, &IngameProcess::CPacket_MOVESTART));
	mContentsMethodTable.insert(make_pair(PE_C_REQ_MOVESTOP, &IngameProcess::CPacket_MOVESTOP));
	mContentsMethodTable.insert(make_pair(PE_C_REQ_MOVEUPDATE, &IngameProcess::CPacket_MOVEUPDATE));
	mContentsMethodTable.insert(make_pair(PE_C_NTY_MOVESTAMPT, &IngameProcess::CPacket_MOVESTAMPT));
	mContentsMethodTable.insert(make_pair(PE_C_NTY_BUILDOBJECTSUCC, &IngameProcess::CPacket_BUILDOBJECTSUCC));
	mContentsMethodTable.insert(make_pair(PE_C_ANS_CHITTING, &IngameProcess::CPacket_CHITTING));


}

//터미널로 인게임에 접속할 유저들의 정보 데이터
void IngameProcess::TPacket_STARTUSERDATA(Session * session, Packet * rowPacket)
{

	PK_T_NTF_STARTUSERDATA * castPacketPtr = (PK_T_NTF_STARTUSERDATA *)rowPacket;

	InstanceGameManager::getInstance().OperatingGame(castPacketPtr->roomNumber, castPacketPtr);

	PK_T_NTY_USERBUILDSUCC terminalPacket;

	terminalPacket.roomNumber = castPacketPtr->roomNumber;

	SLog(L"UserData create Complete!");

	TerminalManager::getInstance().terminal(L"RobbyServer")->SendPacket(&terminalPacket);
}

void IngameProcess::CPacket_USERMOVE(Session * session, Packet * rowPacket)
{
	UINT64 currentTick = GetTickCount64();

	PK_C_REQ_OWNMOVE * castPacketPtr = (PK_C_REQ_OWNMOVE*)rowPacket;

	SLog(L"GetTickCount [%d], Client Tick[%d]", GetTickCount64(), castPacketPtr->time);

	InstanceGameWorld * roomPtr = nullptr;

	if (!InstanceGameManager::getInstance().At(castPacketPtr->roomnumber, roomPtr))
	{
		SLog(L"[CPacket_USERMOVE] : Not Found roomPtr! \n");
		return;
	}

	INT64 retUID = castPacketPtr->uid;

	auto iter = find_if(roomPtr->GetplayerListPtr()->begin(), roomPtr->GetplayerListPtr()->end(),
		[retUID](PlayerObject * user)->bool {return user->GetUid() == retUID; });

	if (iter != roomPtr->GetplayerListPtr()->end())
	{
		(*iter)->SetCurrentPos(D3DXVECTOR3(castPacketPtr->pos[0], castPacketPtr->pos[1], castPacketPtr->pos[2]));
	}
	else
	{
		SLog(L"Error UserMove find_if Function\n");
		return;
	}
	PK_S_ANS_USERMOVE writePacket;

	writePacket.uid = retUID;
	writePacket.pos.push_back(castPacketPtr->pos[0]);
	writePacket.pos.push_back(castPacketPtr->pos[1]);
	writePacket.pos.push_back(castPacketPtr->pos[2]);

	auto playerlist = roomPtr->GetplayerListPtr();
	for (int user = 0; user < 2; ++user)
	{
		if ((*playerlist)[user]->GetUid() == retUID)continue;
		auto sendSession = SessionManager::getInstance().GetSession((*playerlist)[user]->GetOid());
		sendSession->SendPacket(&writePacket);
	}
}

void IngameProcess::CPacket_MOVESTART(Session * session, Packet * rowPacket)
{
	PK_C_REQ_MOVESTART * castPacketPtr = (PK_C_REQ_MOVESTART*)rowPacket;
	PK_S_NTF_MOVESTART writePacket;
	writePacket.uid = castPacketPtr->uid;
	writePacket.MoveStartVecPos = castPacketPtr->MoveStartVecPos;
	writePacket.MoveStartPosTime = castPacketPtr->MoveStartPosTime;
	writePacket.NextFrameVecPos = castPacketPtr->NextFrameVecPos;
	writePacket.NextFramePosTime = castPacketPtr->NextFramePosTime;
	writePacket.MoveDirection = castPacketPtr->MoveDirection;
	InstanceGameWorld * roomPtr = nullptr;
	if (!InstanceGameManager::getInstance().At(castPacketPtr->RoomNumber, roomPtr))
	{
		SLog(L"[CPacket_USERMOVE] : Not Found roomPtr! \n");
		return;
	}
	auto playerList = roomPtr->GetplayerListPtr();
	for (auto iter : (*playerList))
	{
		if (iter->GetUid() == session->Uid())continue;
		if (iter->GetUid() == -1)continue;
		auto Csession = SessionManager::getInstance().GetSession(iter->GetOid());
		Csession->SendPacket(&writePacket);
	}
}

void IngameProcess::CPacket_MOVESTOP(Session * session, Packet * rowPacket)
{
	PK_C_REQ_MOVESTOP * castPacketPtr = (PK_C_REQ_MOVESTOP *)rowPacket;
	PK_S_NTF_MOVESTOP cPacket;

	cPacket.uid = castPacketPtr->uid;
	cPacket.StopDirection = castPacketPtr->StopDirection;
	cPacket.StopKeyTime = castPacketPtr->StopKeyTime;
	cPacket.StopVecPos = castPacketPtr->StopVecPos;

	InstanceGameWorld * roomPtr = nullptr;
	if (!InstanceGameManager::getInstance().At(castPacketPtr->RoomNumber, roomPtr))
	{
		SLog(L"[CPacket_USERMOVE] : Not Found roomPtr! \n");
		return;
	}

	auto playerList = roomPtr->GetplayerListPtr();
	for (auto iter : (*playerList))
	{
		if (iter->GetUid() == session->Uid())continue;
		if (iter->GetUid() == -1)continue;
		auto Csession = SessionManager::getInstance().GetSession(iter->GetOid());
		Csession->SendPacket(&cPacket);
	}
}

void IngameProcess::CPacket_MOVEUPDATE(Session * session, Packet * rowPacket)
{
	PK_C_REQ_MOVEUPDATE * castPacketPtr = (PK_C_REQ_MOVEUPDATE *)rowPacket;
	PK_S_NTF_MOVEUPDATE writePacket;

	writePacket.uid = castPacketPtr->uid;
	writePacket.ViewDirection = castPacketPtr->ViewDirection;
	writePacket.MoveDirection = castPacketPtr->MoveDirection;
	writePacket.MoveStartVecPos = castPacketPtr->MoveStartVecPos;
	writePacket.MoveStartPosTime = castPacketPtr->MoveStartPosTime;
	writePacket.NextFrameVecPos = castPacketPtr->NextFrameVecPos;
	writePacket.NextFramePosTime = castPacketPtr->NextFramePosTime;
	InstanceGameWorld * roomPtr = nullptr;
	if (!InstanceGameManager::getInstance().At(castPacketPtr->RoomNumber, roomPtr))
	{
		SLog(L"[CPacket_USERMOVE] : Not Found roomPtr! \n");
		return;
	}
	auto playerList = roomPtr->GetplayerListPtr();
	for (auto iter : (*playerList))
	{
		if (iter->GetUid() == session->Uid())continue;
		if (iter->GetUid() == -1)continue;
		auto Csession = SessionManager::getInstance().GetSession(iter->GetOid());
		Csession->SendPacket(&writePacket);
	}
}

void IngameProcess::CPacket_MOVESTAMPT(Session * session, Packet * rowPacket)
{
	PK_C_NTY_MOVESTAMPT * castPacketPtr = (PK_C_NTY_MOVESTAMPT *)rowPacket;
	// 패킷을 이용하여 방을 찾는다.
	InstanceGameWorld * roomPtr = nullptr;
	if (!InstanceGameManager::getInstance().At(castPacketPtr->roomNumber, roomPtr))
	{
		SLog(L"[CPacket_USERMOVE] : Not Found roomPtr! \n");
		return;
	}
	// 플레이어 리스트를 추출하여 스탬프를 보낸 유저를 찾는다.
	auto playerList = roomPtr->GetplayerListPtr();

	INT64 retUID = castPacketPtr->uid;

	auto iter = find_if((*playerList).begin(), (*playerList).end(),
		[retUID](PlayerObject * user)->bool {return user->GetUid() == retUID; });

	if (iter != (*playerList).end())
	{
		// 무브스템프의 시간을 추출하여 마지막 시간과 대조 하여 더 늦은 패킷일 경우 
		// 무브스탬프를 무브리스트에 추가한다.
		size_t retMoveCount = castPacketPtr->deltaTIme.size();

		for (auto index = 0; index < retMoveCount; ++index)
		{
			auto dirindex = 3 * index;

			auto keyindex = 2 * index;

			D3DXVECTOR3 rightvector = { castPacketPtr->rightvector[dirindex],
				castPacketPtr->rightvector[dirindex + 1], castPacketPtr->rightvector[dirindex + 2] };

			D3DXVECTOR3 lookvector = { castPacketPtr->lookvector[dirindex], castPacketPtr->lookvector[dirindex + 1],
				castPacketPtr->lookvector[dirindex + 2] };

			if ((*iter)->GetMoveData().insertTimeTempt(castPacketPtr->nowDirection[index], castPacketPtr->LastDirection[index],
				lookvector, rightvector, castPacketPtr->StamptStartTime[index], castPacketPtr->deltaTIme[index], castPacketPtr->Velocity[index], castPacketPtr->genNumber))
			{
				(*iter)->SetIslastMoveTimestamp(true);
			}
		}
	}

}




// 유저가 인게임으로 처음 들어왔을 때의 패킷(클라이언트가 서버에게 PK_C_REQ_FIRSTSIGNAL 을 전송해줌)
void IngameProcess::CPacket_FIRSTSIGNAL(Session * session, Packet * rowPacket)
{
	PK_C_REQ_FIRSTSIGNAL * castPacketPtr = (PK_C_REQ_FIRSTSIGNAL*)rowPacket;
	PK_S_ANS_HELLO writePacketFirst;
	writePacketFirst.ClientTime = castPacketPtr->ClientTime;
	session->SendPacket(&writePacketFirst);
	SLog(L" Send to Hello [uid : %d]", session->Uid());

	PK_S_NTY_FIRSTUSERDATA writePacketSeconds;
	atomic_thread_fence(std::memory_order_seq_cst);
	//////////////////////////////////////////////
	InstanceGameWorld * instanceWorld = nullptr;
	if (!InstanceGameManager::getInstance().At(castPacketPtr->roomNumber, true, instanceWorld))
	{
		SLog(L"[CPacket_USERMOVE] : Not Found roomPtr! \n");
		return;
	}
	auto Pvect = instanceWorld->GetplayerListPtr();

	SLog(L" playerList size : %d]", (*Pvect).size());

	PlayerObject * ownPlayer = nullptr;

	instanceWorld->Write(&writePacketSeconds);
#if SHOW
	for (auto iter = 0; iter < (*Pvect).size(); ++iter)
	{
		if ((*Pvect)[iter]->GetUid() == -1) continue;
		if ((*Pvect)[iter]->GetUid() == castPacketPtr->uid) {
			ownPlayer = (*Pvect)[iter];
			(*Pvect)[iter]->SetOid(session->GetId());
			session->Setuid(castPacketPtr->uid);
			writePacketSeconds.trapcount = (*Pvect)[iter]->TrapCount();
		}
		writePacketSeconds.id.emplace_back((*Pvect)[iter]->GetName());
		writePacketSeconds.uid.emplace_back((*Pvect)[iter]->GetUid());
		writePacketSeconds.px.emplace_back((*Pvect)[iter]->GetPos().x);
		writePacketSeconds.py.emplace_back((*Pvect)[iter]->GetPos().y);
		writePacketSeconds.pz.emplace_back((*Pvect)[iter]->GetPos().z);
		writePacketSeconds.role.emplace_back((*Pvect)[iter]->GetRole());
		writePacketSeconds.degree.emplace_back((*Pvect)[iter]->Lookvector().x);
		writePacketSeconds.degree.emplace_back((*Pvect)[iter]->Lookvector().y);
		writePacketSeconds.degree.emplace_back((*Pvect)[iter]->Lookvector().z);
		if ((*Pvect)[iter]->GetRole() == ROLE_KILLER) {
			writePacketSeconds.statusbar.emplace_back(-1);
			writePacketSeconds.hp.emplace_back(-1);
		}
		else {
			writePacketSeconds.statusbar.emplace_back((INT32)(*Pvect)[iter]->GetStatebar());
			writePacketSeconds.hp.emplace_back((*Pvect)[iter]->GetHp());
		}
	}
#else
	for (auto iter = 0; iter < (*Pvect).size(); ++iter)
	{
		if ((*Pvect)[iter].get_role() != ROLE_KILLER) {
			SPacket.statusbar[iter] = (INT32)(*Pvect)[iter].get_statebar();
			SPacket.uid[iter] = (INT32)(*Pvect)[iter].uid();
			SPacket.id[iter] = (*Pvect)[iter].name();
		}
		if ((*Pvect)[iter].uid() == packet->uid) {
			ownPlayer = &(*Pvect)[iter];
			auto pos = (*Pvect)[iter].get_position();
			SPacket.px = pos.x;
			SPacket.py = pos.y;
			SPacket.px = pos.z;
			SPacket.hp = (*Pvect)[iter].get_hp();
			SPacket.degree = (*Pvect)[iter].get_degree();
			SPacket.role = (INT32)(*Pvect)[iter].get_role();
			SPacket.trapcount = (*Pvect)[iter].get_trapcount();
		}
	}
#endif
	writePacketSeconds.STime = ownPlayer->SystemRecordTick();
	session->SendPacket(&writePacketSeconds);
	SLog(L" Send to FIRSTUSERDATA [uid : %d]", session->Uid());
}

// Latency Check용~~~!
void IngameProcess::CPacket_LATENCYCHECK(Session * session, Packet * rowPacket)
{
	PK_C_NTY_LATENCYCHECK * castPacketPtr = (PK_C_NTY_LATENCYCHECK*)rowPacket;
	UINT64 roundTripDelay = (GetTickCount64() - castPacketPtr->STime) / 2;
	auto uid = session->Uid();
	for (auto iter = 0; iter > ROOMMAXCOUNT; ++iter)
	{
		InstanceGameWorld * roomPtr = nullptr;
		if (!InstanceGameManager::getInstance().At(iter, roomPtr))
		{
			SLog(L"[CPacket_LATENCYCHECK] : Not Found roomPtr! \n");
			continue;
		}

		size_t maxcount = roomPtr->GetplayerListPtr()->size();
		auto playerlist = roomPtr->GetplayerListPtr();
		for (int index = 0; index < maxcount; ++index)
		{
			if ((*playerlist)[index]->GetUid() != uid) continue;
			// Latency 확인 완료
			(*playerlist)[index]->SetLatency(roundTripDelay / 2);
			PK_S_NTY_LATENCYCHECK cPacket;
			cPacket.STime = (*playerlist)[index]->Latency();
			session->SendPacket(&cPacket);
			return;
		}

	}
}

void IngameProcess::CPacket_BUILDOBJECTSUCC(Session * session, Packet * rowPacket)
{
	SLog(L" recv to BUILDOBJECTSUCC [uid : %d]", session->Uid());
	PK_C_NTY_BUILDOBJECTSUCC * castPacketPtr = (PK_C_NTY_BUILDOBJECTSUCC*)rowPacket;
	if (InstanceGameManager::getInstance().CheckBuildObjectSucc(castPacketPtr->roomNumber, castPacketPtr->uid))
	{
		InstanceGameWorld * roomPtr = nullptr;
		if (!InstanceGameManager::getInstance().At(castPacketPtr->roomNumber, roomPtr))
		{
			SLog(L"[CPacket_LATENCYCHECK] : Not Found roomPtr! \n");
			return;
		}
		auto playerList = roomPtr->GetplayerListPtr();
		PK_S_NTY_GOTOINGAME cPacket;
		for (auto iter : (*playerList))
		{
			if (iter->GetUid() == -1)continue;
			auto Csession = SessionManager::getInstance().GetSession(iter->GetOid());
			Csession->SendPacket(&cPacket);
			SLog(L" send to GOTOINGAME [uid : %d]", Csession->Uid());
		}
	}
}

void IngameProcess::CPacket_CHITTING(Session * session, Packet * rowPacket)
{
	for (int i = 0; i < 10; ++i)
	{
		auto ptr = GeneratorManager::getInstance().GetGenPointer(i);
		ptr->stop();
	}
}

AutoLatencyCheck::AutoLatencyCheck()
{
	const int AUTO_IO_SUPERVISE = 5000;
	TaskNode * node = new TaskNode(this, AUTO_IO_SUPERVISE, TICK_INFINTY);
	TaskManager::getInstance().Add(node);
}

AutoMoveSend::AutoMoveSend()
{
	const int UPDATE_SEND_MOVE = 100;
	TaskNode * node = new TaskNode(this, UPDATE_SEND_MOVE, TICK_INFINTY);
	TaskManager::getInstance().Add(node);
}
