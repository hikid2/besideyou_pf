#include"stdafx.h"
#include"GameObject\generator.h"
#include"InstanceGameWorld.h"

/**
	@return : void

	@param 1 : Source Data

	@brief : InstanceNO Set

	@warning : NO
*/
void InstanceGameWorld::SetInstanceNO(int64_t inNO) { mInstanceNO = inNO; }

InstanceGameWorld::InstanceGameWorld(int64_t inNO) : mLock(L"InRoomManager")
{
	mInstanceNO = inNO;
	mUnSuccesfulBuild = 4;
	for (auto iter = 0; iter < mPlayerList.size(); ++iter)
	{
		mPlayerList[iter] = new PlayerObject(L"notUser", -1, ROLE_NONE, D3DXVECTOR3(0.f, 0.f, 0.0f), 0.0f, this);
	}
}

InstanceGameWorld::~InstanceGameWorld()
{
	for (auto iter = 0; iter < mPlayerList.size(); ++iter)
	{
		auto pointer = mPlayerList[iter];
		SAFE_DELETE(pointer);
	}
}

void InstanceGameWorld::Clear()
{
	for (auto iter : mPlayerList) 
		iter->SetClear();
	// 유저 상태 초기화
	// 오브젝트 비 연결
}

/**
	@return : void

	@param 1 : PK_T_NTF_STARTUSERDATA Packet

	@brief : LobbyServer에서 InstanceGame의 시작을 위한 Initialize작업을 진행

	@warning : 최대인원을 2명으로 제한(클라이언트의 업데이트 중지) -> 기회가 되면 수정이 필요.
*/
void InstanceGameWorld::GameStart(PK_T_NTF_STARTUSERDATA * inPacket)
{
	// TEST
	int unIndex = 1;
	for (auto index = 0; index < 2; ++index) {
		if ((USER_ROLE)inPacket->role[index] == ROLE_KILLER) {
			mPlayerList[0]->Initialize(inPacket->id[index], inPacket->uid[index], ROLE_KILLER, D3DXVECTOR3(650.0f, 70.f, 610.0f), this);
			continue;
		}
		if (unIndex == 1) {
			mPlayerList[unIndex]->Initialize(inPacket->id[index], inPacket->uid[index], ROLE_SURVIORS, D3DXVECTOR3(1300.0, 70.f, 60.0), this);
			++unIndex;
			continue;
		}
	}
	// TODO : uid.size()로 변경해야함.
	mUnSuccesfulBuild = 2;
	for (int i = 0; i < 10; ++i)
	{
		int genIndex = (int)(mInstanceNO * 10) + i;
		GeneratorManager::getInstance().AddGenerator(Generator::instance(genIndex), genIndex);
		auto ptr = GeneratorManager::getInstance().GetGenPointer(genIndex);
		this->mGeneratorList[i] = genIndex;
		World::getInstance().AddGameObject(ptr);
	}
	return;
}

/**
	@return : void

	@brief : Instance Game이 종료되어 자원을 초기화 시킴

	@warning : 
*/
void InstanceGameWorld::GameExit()
{
	InstanceGameManager::getInstance().Clear(mInstanceNO);
	for (int i = 0; i < 10; ++i)
	{
		auto ptr = GeneratorManager::getInstance().GetGenPointer(this->mGeneratorList[i]);
		World::getInstance().RemoveGameObject(ptr);
		GeneratorManager::getInstance().RemoveGenerator(this->mGeneratorList[i]);
	}
	PK_T_NTY_GAMECLEAR packet;
	packet.roomNumber = mInstanceNO;
	TerminalManager::getInstance().terminal(L"RobbyServer")->SendPacket(&packet);
}

/**
	@return : void

	@brief : Instance안의 상태를 일정 프레임마다 Update 함

	@warning : 
*/
void InstanceGameWorld::update()
{
	SAFE_LOCK(mLock);
	for (auto iter = 0; iter < mPlayerList.size(); ++iter)
	{
		if (mPlayerList[iter] != nullptr)
		{
			mPlayerList[iter]->Tick();
		}
	}
}

/**
	@return : void

	@brief : PK_S_NTY_PLAYERPOS(발전기, 트랩 정보 포함), Game Result State Packet Wirte and Send.

	@warning : 
*/
void InstanceGameWorld::BroadCastSend()
{
	PK_S_NTY_PLAYERPOS packet;
	mLock.lock(__FILEW__, __LINE__);
	for (auto iter = 0; iter < mPlayerList.size(); ++iter)
	{
		if (mPlayerList[iter] == nullptr) continue;
		mPlayerList[iter]->Write(&packet);
	}
	int onGenCount = 0;
	for (int i = 0; i < 10; ++i)
	{
		auto ptr = GeneratorManager::getInstance().GetGenPointer(this->mGeneratorList[i]);
		packet.GeneratorNumber.emplace_back(mGeneratorList[i]);
		if (ptr->IsComplete())
			onGenCount += 1;
		auto persent = ptr->Percent();
		packet.GeneratorP.emplace_back(persent);
	}
	TrapManager::getInstance().Write(&packet, mInstanceNO);
	mLock.Unlock();
	for (auto iter = 0; iter < mPlayerList.size(); ++iter)
	{
		if (mPlayerList[iter] == nullptr) continue;
		size_t oid = mPlayerList[iter]->GetOid();
		auto session = SessionManager::getInstance().GetSession(oid);
		if (session == nullptr)continue;
		session->SendPacket(&packet);
		if (packet.hp.size() > 0)
		{
			printf("");
		}
	}
	
	bool isGameEnd = false;
	auto PlayerListCount = mPlayerList.size();
	
	auto zeroLiftUserCount = 0;
	
	for (auto iter = 0; iter < mPlayerList.size(); ++iter)
	{
		// 빈 사람일 경우 패스
		if (mPlayerList[iter]->GetRole() == ROLE_NONE)
		{
			PlayerListCount--;
			continue;
		}
		// 생존자 일 경우
		if (mPlayerList[iter]->GetRole() == ROLE_SURVIORS) {
			// 생존자의 체력이 0 일시
			if (mPlayerList[iter]->GetHp() == 0) {
				zeroLiftUserCount++;
				if (mPlayerList[iter]->mAbleGamePlayer == true)
				{
					zeroLiftUserCount++;
					auto oid = mPlayerList[iter]->GetOid();
					auto uid = mPlayerList[iter]->GetUid();
					PK_S_ANS_DEFEATSTATE packet;
					packet.role = ROLE_SURVIORS;
					packet.uid = uid;
					auto session = SessionManager::getInstance().GetSession(oid);
					if (session != nullptr)
						session->SendPacket(&packet);
					mPlayerList[iter]->mAbleGamePlayer = false;
				}
				// 패배를 알림.
				continue;
			}
			else if (onGenCount == 10)
			{
				SLog(L"all Generator count is [%d ]", onGenCount);
				auto currentPosition =  mPlayerList[iter]->GetPos();
				SLog(L"now Position is [%.2f][%.2f]", currentPosition.x, currentPosition.z);
				if (currentPosition.z > 2535.0  &&
					currentPosition.z < 2550.0 &&
					currentPosition.x > 1175.0  &&
					currentPosition.x < 1270.0)
				{
					//SLog(L"동쪽문 탈출");
					auto oid = mPlayerList[iter]->GetOid();
					auto uid = mPlayerList[iter]->GetUid();
					PK_S_ANS_WINSTATE packet;
					packet.role = ROLE_SURVIORS;
					packet.uid = uid;
					auto session = SessionManager::getInstance().GetSession(oid);
					if(session != nullptr)
						session->SendPacket(&packet);
					for (auto index = 0; index < mPlayerList.size(); ++index)
					{
						if (mPlayerList[index] == nullptr) continue;
						if (mPlayerList[index]->GetRole() == ROLE_KILLER)
						{
							auto oid = mPlayerList[index]->GetOid();
							auto uid = mPlayerList[index]->GetUid();
							PK_S_ANS_DEFEATSTATE packet;
							packet.role = ROLE_KILLER;
							packet.uid = uid;
							auto session = SessionManager::getInstance().GetSession(oid);
							if (session != nullptr)
								session->SendPacket(&packet);
							isGameEnd = true;
							break;
						}
					}
				}
				else if (currentPosition.z > 5.0  &&
					currentPosition.z < 20.0 &&
					currentPosition.x > 1175.0 &&
					currentPosition.x < 1270.0)
				{
					//SLog(L"서쪽문 탈출");
					auto oid = mPlayerList[iter]->GetOid();
					auto uid = mPlayerList[iter]->GetUid();
					PK_S_ANS_WINSTATE packet;
					packet.role = ROLE_SURVIORS;
					packet.uid = uid;
					auto session = SessionManager::getInstance().GetSession(oid);
					if(session != nullptr)
						session->SendPacket(&packet);
					for (auto index = 0; index < mPlayerList.size(); ++index)
					{
						if (mPlayerList[index] == nullptr) continue;
						if (mPlayerList[index]->GetRole() == ROLE_KILLER)
						{
							auto oid = mPlayerList[index]->GetOid();
							auto uid = mPlayerList[index]->GetUid();
							PK_S_ANS_DEFEATSTATE packet;
							packet.role = ROLE_KILLER;
							packet.uid = uid;
							auto session = SessionManager::getInstance().GetSession(oid);
							if (session != nullptr)
								session->SendPacket(&packet);
							isGameEnd = true;
							break;
						}
					}
				}				
				continue;
			}
			continue;
		}
	}
	if (PlayerListCount - 1 == zeroLiftUserCount)
	{
		for (auto index = 0; index < mPlayerList.size(); ++index)
		{
			if (mPlayerList[index] == nullptr) continue;
			if (mPlayerList[index]->GetRole() == ROLE_KILLER)
			{
				auto oid = mPlayerList[index]->GetOid();
				auto uid = mPlayerList[index]->GetUid();
				PK_S_ANS_WINSTATE packet;
				packet.role = ROLE_KILLER;
				packet.uid = uid;
				auto session = SessionManager::getInstance().GetSession(oid);
				if (session == nullptr)continue;
				session->SendPacket(&packet);
				isGameEnd = true;
				break;
			}
		}
	}
	if (isGameEnd)
	{
		GameExit();
	}
}

bool InstanceGameWorld::SuccessfulBuild()
{
	SAFE_LOCK(mLock);
	mUnSuccesfulBuild -= 1;
	if (mUnSuccesfulBuild == 0)
		return true;
	return false;
}

void InstanceGameWorld::Write(Packet * inPacket)
{
	switch (inPacket->type())
	{
	case PE_S_NTY_FIRSTUSERDATA:
	{
		auto castPacekt = reinterpret_cast<PK_S_NTY_FIRSTUSERDATA*>(inPacket);
		for (auto iter : mGeneratorList)
		{
			int retGenNo = iter;
			castPacekt->generatorNumber_.emplace_back(retGenNo);
		}
	}
		
		break;
	default:
		break;
	}
	
}
