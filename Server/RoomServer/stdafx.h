// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 또는 프로젝트 관련 포함 파일이
// 들어 있는 포함 파일입니다.
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#define PI 3.1415926535

//move
#define DIR_FORWARD	0x01
#define DIR_BACKWARD	0x02
#define DIR_LEFT	0x04
#define DIR_RIGHT	0x08

#define DIR_CROUCH		0x10
#define DIR_WALK	0x20
#define DIR_ATTACT		0x40
#define DIR_INTERACTION 0x40
#define DIR_TRAP 0x80
#define DIR_CROUCHUP 1 << 8
#define DIR_TRAPINSTALL 1 << 9

inline float DegreeToRadian(float deg) {
	return (float)(deg * (PI / 180.F));
}
inline float RadianToDegree(float rad) {
	return (float)(rad * (180.F / PI));
}
typedef struct RECTF
{
	double left;
	double right;
	double top;
	double bottom;
}RectF;





// TODO: 프로그램에 필요한 추가 헤더는 여기에서 참조합니다.
//#include<d3d11.h>
//#include<d3dx11.h>
#define SHOW 1


#include"serverHeader.h"
#include"deque.h"

class Trap;
typedef pair<bool, Trap *> TrapResult;


#include<d3dx10math.h>
#include<DirectXCollision.h>
#include"GameObject\generator.h"
#include"GameObject\Trap.h"
#include"GameObject\PlayerObject.h"
#include"InstanceGameWorld.h"
#include"InstanceGameManager.h"
#include"RoomProcess.h"



