#include"stdafx.h"
#include"InstanceGameManager.h"

InstanceGameManager::InstanceGameManager() : mLock(L"InRoomManager")
{
	this->Initalize();
}

InstanceGameManager::~InstanceGameManager()
{
	for (auto iter = 0; iter < ROOMMAXCOUNT; ++iter)
	{
		auto pointer = mInstanceGameList[iter].second;
		SAFE_DELETE(pointer);
	}
}

void InstanceGameManager::Initalize()
{
	for (auto i = 0; i < ROOMMAXCOUNT; ++i)
	{
		mInstanceGameList[i] = make_pair(false, new InstanceGameWorld(i + 1));
	}
}

void InstanceGameManager::Clear(int64_t Roomindex)
{
	// 방 시뮬레이션 안되게 
	mInstanceGameList[Roomindex].first = false;
	auto list = mInstanceGameList[Roomindex].second->GetplayerListPtr();
	for (auto iter : (*list)) {
		if (iter == nullptr) continue;
		World::getInstance().RemoveGameObject(iter);
	}
	mInstanceGameList[Roomindex].second->Clear();
}

bool InstanceGameManager::At(int64_t inIndex, InstanceGameWorld *& outObjectPtr)
{
	// 예외 처리 
	if (!IsValidIndex(inIndex)) {
		SLog(L"!!!!   [InRoomManager at() Error ] Invalid index.   !!!!");
		return false;
	}

	SAFE_LOCK(mLock);
	if (mInstanceGameList[inIndex].first == false) {
		return false;
	}

	outObjectPtr = mInstanceGameList[inIndex].second;
	return true;
}

bool InstanceGameManager::At(int64_t index, bool first, InstanceGameWorld *& outObjectPtr)
{
	// 예외 처리 
	if (!IsValidIndex(index)) {
		SLog(L"!!!!   [InRoomManager at() Error ] Invalid index.   !!!!");
		return false;
	}
	SAFE_LOCK(mLock);
	outObjectPtr = mInstanceGameList[index].second;
	return true;
}

bool InstanceGameManager::OperatingGame(int64_t roomindex, PK_T_NTF_STARTUSERDATA * packet)
{
	if (!IsValidIndex(roomindex)) {
		return false;
		SLog(L"!!!!   [InRoomManager operatingGame() Error ] Invalid index.   !!!!");
	}
	int64_t realroomindex = roomindex;
	SAFE_LOCK(mLock);
	if (mInstanceGameList[realroomindex].first == true) {
		SLog(L"!!!!   [InRoomManager operatingGame() Error ] Already GameStarted.   !!!!");
		return false;
	}
	mInstanceGameList[realroomindex].second->SetInstanceNO(realroomindex);
	mInstanceGameList[realroomindex].second->GameStart(packet);
	SLog(L"!!!!   [InRoomManager operatingGame()object making]  !!!!");
	return true;
}

bool InstanceGameManager::CheckBuildObjectSucc(int64_t & index, int64_t & uid)
{

	if (!IsValidIndex(index)) {
		return false;
		SLog(L"!!!!   [InRoomManager operatingGame() Error ] Invalid index.   !!!!");
	}
	int64_t realroomindex = index;

	if (mInstanceGameList[realroomindex].second->SuccessfulBuild()) {
		SLog(L"Change To Game Room start");
		auto list = mInstanceGameList[realroomindex].second->GetplayerListPtr();
		for (auto iter : (*list)) {
			if (iter == nullptr)continue;
			World::getInstance().AddGameObject(iter);
		}
		mInstanceGameList[realroomindex].first = true;

		return true;
	}
	return false;
}

bool InstanceGameManager::IsValidIndex(const int64_t & inIndex)
{
	// invalid index Check
	if (inIndex > ROOMMAXCOUNT) {
		SLog(L"!!!!   [InRoomManager] Invalid index.   !!!!");
		return false;
	}
	return true;
}
