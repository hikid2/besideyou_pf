#pragma once
#include"stdafx.h"
#include<deque>
#define CodeTest 0

#ifdef CodeTest
template<class T>
class SafeDeque
{
private:
	std::deque<T> deque_;
	Lock lock_;
public:
	SafeDeque(WCHAR * name) : lock_(name)
	{}
	~SafeDeque()
	{
		deque_.clear();
	}
	inline typename void copy_and_delete(deque<T> & deque)
	{
		SAFE_LOCK(lock_);
		deque.assign(deque_.begin(), deque_.end());
		deque_.clear();
	}

	inline void emplace_back(const T &t)
	{
		SAFE_LOCK(lock_);
		deque_.emplace_back(t);
	}
	inline void push_back(const T &t)
	{
		SAFE_LOCK(lock_);
		deque_.push_back(t);
	}
	inline bool pop(T & t)
	{
		SAFE_LOCK(lock_);
		if (deque_.empty())
			return false;
		t = deque_.front();
		deque_.pop_front();
		return true;
	}
	inline const bool top(T & t)
	{
		SAFE_LOCK(lock_);
		if (deque_.empty())
			return false;
		t = deque_.front();
		return true;
	}
	inline bool erase() {
		SAFE_LOCK(lock_);
		deque<T>::iterator begin = deque_.begin();
		if (begin == deque_.end())return false;
		deque_.erase(begin);
		return true;
	}
	inline bool erase(typename std::deque<T>::iterator & begin, typename std::deque<T>::iterator & end) {
		SAFE_LOCK(lock_)
		deque_.erase(begin, end);
		return true;
	}
	inline bool isEmpty() {
		return deque_.empty();
	}
	inline size_t size()
	{
		SAFE_LOCK(lock_);
		size_t size = deque_.size();
		return size;
	}
	inline typename deque<T>::iterator begin()
	{
		SAFE_LOCK(lock_);
		deque<T>::iterator begin = deque_.begin();
		return begin;
	}
	inline typename T Front()
	{
		SAFE_LOCK(lock_);
		T front = deque_.front();
		return front;
	}
	inline typename T at(int index)
	{
		SAFE_LOCK(lock_);
		deque<T>::iterator temp = deque_.begin() + index;
		return (*temp);
	}
	inline typename deque<T>::iterator end()
	{
		SAFE_LOCK(lock_);
		deque<T>::iterator end = deque_.end();
		return end;
	}
	void clear() { deque_.clear(); }
};

#else
template<class T>
//class SafeDeque
//{
//	enum
//	{
//		READ,
//		WRITE,
//		MAX
//	};
//private:
//	std::deque<T>	deque_[MAX];
//
//	std::deque<T> * readPtr_;
//	std::deque<T> * writePtr_;
//
//	Lock lock_;
//public:
//	SafeDeque(WCHAR * name) : lock_(name)
//	{
//		readPtr_ = &deque_[READ];
//		writePtr_ = &deque_[WRITE];
//	}
//	SafeDeque(const SafeDeque<T> & deq)
//	{
//		SAFE_LOCK(lock_);
//		deque_.insert(deque_.begin(), deq.deque_.begin(), deq.deque_.end());
//		lock_ = deq.lock_;
//	}
//
//	~SafeDeque()
//	{
//		this->clear();
//	}
//
//	inline void emplace_back(const T &t)
//	{
//		SAFE_LOCK(lock_);
//		writePtr_->emplace_back(t);
//	}
//	inline void push_back(const T &t)
//	{
//		SAFE_LOCK(lock_);
//		writePtr_->push_back(t);
//	}
//	// pop()
//	// 순서. 빼내기전에 전체의 사이즈 측정
//	// 전체 사이즈가 0이면 빼낼게 없으므로 return;
//	// 후 readptr_->emptr()를 확인
//	// 비어 있다면 swap()을 한다
//	// 비어있지 않으면 그대로 쓰는게 맞다
//	inline bool pop(T & t)
//	{
//		SAFE_LOCK(lock_);
//		if (this->size() == 0) return false;
//		if (readPtr_->empty()) this->swap();
//		t = readPtr_->front();
//		readPtr_->pop_front();
//		return true;
//	}
//	inline const bool top(T & t)
//	{
//		SAFE_LOCK(lock_);
//		if (this->size() == 0) {
//			return false;
//		}
//		if (readPtr_->empty()) this->swap();
//		t = readPtr_->front();
//		return true;
//	}
//	// 
//	inline void swap() {
//		SAFE_LOCK(lock_);
//		if (writePtr_ == &deque_[WRITE])
//		{
//			writePtr_ = &deque_[READ];
//			readPtr_ = &deque_[WRITE];
//		}
//		else 
//		{
//			readPtr_ = &deque_[READ];
//			writePtr_ = &deque_[WRITE];
//		}
//	}
//	/*inline bool erase(size_t index) {
//		SAFE_LOCK(*lock_);
//		for (auto i = 0; i < index; ++i)
//		{
//			if()
//			readPtr_->pop_front();
//		}
//		return true;
//	}*/
//	inline bool isEmpty() {
//		SAFE_LOCK(lock_)
//		return readPtr_->empty();
//	}
//	inline size_t size()
//	{
//		SAFE_LOCK(lock_);
//		size_t size = readPtr_->size() + writePtr_->size();
//		return size;
//	}
//	/*inline typename deque<T>::iterator begin()
//	{
//		SAFE_LOCK(*lock_);
//		deque<T>::iterator begin = deque_.begin();
//		return begin;
//	}*/
//	inline typename T Front()
//	{
//		SAFE_LOCK(lock_);
//		T front = deque_.front();
//		return front;
//	}
//	inline typename T at(int index)
//	{
//		SAFE_LOCK(lock_);
//		deque<T>::iterator temp = deque_.begin() + index;
//		return (*temp);
//	}
//	/*inline typename deque<T>::iterator end()
//	{
//		SAFE_LOCK(*lock_);
//		deque<T>::iterator end = deque_.end();
//		return end;
//	}*/
//	void clear() {
//		deque_[READ].clear();
//		deque_[WRITE].clear();
//	}
//};

#endif